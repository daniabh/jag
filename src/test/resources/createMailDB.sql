/**
 * The mail database contains emails. They can be received emails, sent emails or draft emails.
 * It has five tables that allow the retrieval of an email's content and information, 
 * an email's folder, an email's attachments and recipients.
 * Author:  Dania Bouhmidi
 */



DROP DATABASE IF EXISTS MAIL;
CREATE DATABASE MAIL;

USE MAIL;

DROP USER IF EXISTS mailer@localhost;
CREATE USER mailer@'localhost' IDENTIFIED BY 'dawson';
GRANT ALL ON MAIL.* TO mailer@'localhost';


FLUSH PRIVILEGES;


