USE MAIL;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS EMAIL;
DROP TABLE IF EXISTS ADDRESSES;
DROP TABLE IF EXISTS EMAILTOADDRESS;
DROP TABLE IF EXISTS ATTACHMENTS;
DROP TABLE IF EXISTS FOLDERS;
SET FOREIGN_KEY_CHECKS = 1;
-- This table contains all the folders that categorize emails.
-- Each folder name must be unique, there are no duplicates in this table.
CREATE TABLE FOLDERS(
FOLDERID INT NOT NULL AUTO_INCREMENT,
FOLDER VARCHAR(200) NOT NULL UNIQUE,
PRIMARY KEY (FOLDERID)
);
-- This table holds emails, t
-- The emails can be received emails,sent emails or draft emails.
-- The folder of an email categorizes it.
CREATE TABLE EMAIL (
 EMAILID INT NOT NULL AUTO_INCREMENT,
 SUBJECT VARCHAR(255) NULL DEFAULT '',
 TEXT VARCHAR(5000) NULL DEFAULT '',
 HTMLTEXT TEXT NULL,
 USERNAME VARCHAR(62) NOT NULL,
 FROMEMAILADDRESS VARCHAR(320) NOT NULL,
 DATE TIMESTAMP NOT NULL,
 FOLDERID INT NOT NULL,
 CONSTRAINT FKEFOLDERID FOREIGN KEY(FOLDERID)
 REFERENCES FOLDERS(FOLDERID),
 PRIMARY KEY  (EMAILID)
);

-- Every email address appears once in this table.
CREATE TABLE ADDRESSES (
ADDRESSID INT NOT NULL AUTO_INCREMENT,
EMAILADDRESS VARCHAR(320) NOT NULL UNIQUE,
PRIMARY KEY  (ADDRESSID)
);
-- Bridging table so that an email can have multiple recipients 
-- in multiple recipient fields.
CREATE TABLE EMAILTOADDRESS (
ID INT  NOT NULL AUTO_INCREMENT,
EMAILID INT NOT NULL,
ADDRESSID INT NOT NULL,
EMAILADDRESSTYPE VARCHAR(5) NOT NULL,
CONSTRAINT FKEMAILID FOREIGN KEY(EMAILID)
REFERENCES EMAIL(EMAILID), 
CONSTRAINT FKADDRESSID FOREIGN KEY(ADDRESSID)
REFERENCES ADDRESSES(ADDRESSID), 
PRIMARY KEY  (ID)
);

-- This table holds all the attachments
-- An attachment record must indicate if it embedded or not.
-- An attachment can be associated with multiple emails (with the emailid field)
-- An email can also have multiple attachments 
CREATE TABLE ATTACHMENTS (
ATTACHMENTID INT NOT NULL AUTO_INCREMENT,
FILENAME VARCHAR(400),
ATTACHMENT LONGBLOB NULL,
EMBEDDED BOOLEAN,
CONTENTID VARCHAR(400) NULL,
EMAILID INT NOT NULL,
CONSTRAINT FKATEMAILID FOREIGN KEY(EMAILID)
REFERENCES EMAIL(EMAILID),
PRIMARY KEY  (ATTACHMENTID)
);

INSERT INTO FOLDERS (FOLDERID,FOLDER) VALUES ('1','DRAFT');
INSERT INTO FOLDERS (FOLDERID,FOLDER) VALUES ('2','SENT');
INSERT INTO FOLDERS (FOLDERID,FOLDER) VALUES ('3','INBOX');

-- The last three inserted emails are received emails, because their assigned folder is the INBOX folder.
INSERT INTO EMAIL (EMAILID,SUBJECT,TEXT,HTMLTEXT,USERNAME,FROMEMAILADDRESS,DATE,FOLDERID) VALUES
('1','TEST SUBJECT','TEST EMAIL 1',NULL,'moonchild.zyx','moonchild.zyx@gmail.com',NOW(),'1'),
('2','TEST SUBJECT','TEST EMAIL 2',NULL,'moonchild.zyx','moonchild.zyx@gmail.com', NOW(),'1'),
('3','Dnnnnnnnnnnnn', 'Nunc rhoncus dui vel sem.','<html><body><h1>Hello</h1> <img src="cid:Cubilia.jpg"> </body></html>', 'blue.side.xyzy','blue.side.xyzy@gmail.com', '2020-07-26 07:25:59','1'),
('4','Nulla ac enim.', 'Proin leo odio porttitor i.',NULL,'haku.xyz' ,'haku.xyz@gmail.com', '2019-10-07 23:45:07','1'),
('5','Nulla justo.', 'Sed vel enim sit amet nunc viverra dapibus.', '<html><body><h1>Hey!</h1><img src="cid:Cubilia.jpg"></body></html>','blue.side.xyzy', 'blue.side.xyzy@gmail.com', '2020-02-14 09:05:18','1'),
('6','Duis ac nibh.', 'Vivamus in felis eu sapien.', NULL,'moonchild.zyx', 'moonchild.zyx@gmail.com', '2020-03-26 19:55:57','2'),
('7','Morbi vel lectus in.', 'Duis ac nibh.', NULL, 'haku.xyz','haku.xyz@gmail.com', '2020-07-26 07:25:59','2'),
('8','Vestibulum ante ipsum primis.', 'Phasellus in felis.', NULL,'blue.side.xyzy', 'blue.side.xyzy@gmail.com', '2020-06-06 18:23:10','2'),
('9','Nam nulla.', 'Quisque id justo sit amet sapien.', NULL,'blue.side.xyzy','blue.side.xyzy@gmail.com','2020-05-27 15:45:25','2' ),
('10','Aliquam erat volutpat.', 'Praesent id massa id.', NULL, 'moonchild.zyx','moonchild.zyx@gmail.com', '2019-11-26 14:52:55','2'),
('11','Nullam varius.', 'Donec quis orci eget orci.', NULL,'swan.zyxz', 'swan.zyxz@gmail.com', '2020-07-26 07:25:59','1'),
('12','Pellentesque eget nunc.', 'Curabitur in libero.', NULL,'blue.side.xyzy', 'blue.side.xyzy@gmail.com', '2020-07-26 07:25:59','1'),
('13','Maecenas ut massa.', 'Praesent blandit lacinia erat.', NULL,'swan.zyxz', 'swan.zyxz@gmail.com', '2020-07-26 07:25:59','2'),
('14','Morbi ut odio.', 'Vivamus vel nulla eget.', NULL,'blue.side.xyzy','blue.side.xyzy@gmail.com', '2020-07-26 07:25:59','2'),
('15','Proin leo odio, porttitor id.', 'Aliquam sit amet diam i.',NULL, 'swan.zyxy','swan.zyxz@gmail.com', '2020-06-06 18:23:10','2'),
('16','Quisque id justo sit amet.', 'Suspendisse ornare.', NULL,'haku.xyz', 'haku.xyz@gmail.com', '2020-07-26 07:25:59','1'),
('17','Proin risus.', 'Nam tristique tortor eu pede.', NULL,'blue.side.xyzy','blue.side.xyzy@gmail.com', '2019-11-26 14:52:55','1'),
('18','In congue.', 'Duis bibendum, felis.', NULL,'swan.zyxz','swan.zyxz@gmail.com', '2020-07-26 07:25:59','3'),
('19','Cras pellentesque volutpi.', 'In hac habitasse.', NULL, 'moonchild.zyx','moonchild.zyx@gmail.com', '2020-07-26 07:25:59','3'),
('20','Morbi vestibulum, velit', 'In hac habitasse.', '<html><body><h1>Hello</h1></body></html>','blue.side.xyzy', 'blue.side.xyzy@gmail.com', '2020-06-06 18:23:10','3');


INSERT INTO ADDRESSES (ADDRESSID,EMAILADDRESS) VALUES
('1','blue.side.xyzy@gmail.com'),
('2','swan.zyxz@gmail.com'),
('3','haku.xyz@gmail.com'),
('4','moonchild.zyx@gmail.com');

-- BLOB attachments are null, filename field is filled for testing purposes.
-- If the embedded boolean value is true, a contentid must be specified.
INSERT INTO ATTACHMENTS (ATTACHMENTID,FILENAME, ATTACHMENT,EMBEDDED,CONTENTID,EMAILID) VALUES
 ('1','Cubilia.jpg', NULL,true,'Cubilia.jpg','5'),
 ('2','Gravida.png', NULL,false,NULL,'2'),
 ('3','UtUltrices.png', NULL,true,'UtUltrices.png','3'),
 ('4','VulputateJustoIn.jpg', NULL,false,NULL,'4'),
 ('5','a.jpg', NULL,false,NULL,'5'),
 ('6','b.png', NULL,false,NULL,'2'),
 ('7','c.png', NULL,false,NULL,'3'),
 ('8','d.jpg', NULL,false,NULL,'4'),
 ('9','e.jpg', NULL,false,NULL,'1'),
 ('10','f.jpg', NULL,false,NULL,'9'),
 ('11','1.jpg', NULL,false,NULL,'5'),
 ('12','2.png', NULL,false,NULL,'2'),
 ('13','3.png', NULL,false,NULL,'3'),
 ('14','4.jpg', NULL,false,NULL,'4'),
 ('15','5.jpg', NULL,false,NULL,'1'),
 ('16','6.jpg', NULL,false,NULL,'9');



INSERT INTO EMAILTOADDRESS (EMAILID,ADDRESSID,EMAILADDRESSTYPE) VALUES
('1','1','TO'),
('2','2','CC'),
('3','3','BCC'),
('4','4','CC'),
('5','1','TO'),
('6','2','BCC'),
('7','3','TO'),
('8','4','CC'),
('9','1','BCC'),
('10','2','TO'),
('11','3','CC'),
('12','4','TO'),
('13','1','CC'),
('14','2','BCC'),
('15','3','TO'),
('16','4','TO'),
('17','1','BCC'),
('18','2','CC'),
('19','3','CC'),
('20','4','CC');
