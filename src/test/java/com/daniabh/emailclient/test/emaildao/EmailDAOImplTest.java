package com.daniabh.emailclient.test.emaildao;

import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.exception.EmailLimitExceededException;
import com.daniabh.emailclient.persistence.EmailDAOImpl;
import com.daniabh.emailclient.properties.MailConfigBean;
import com.daniabh.emailclient.test.MethodLogger;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import jodd.mail.CommonEmail;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class for EmailDAOImpl. Tests that all the CRUD operations work
 * properly.
 *
 * @author Dania Bouhmidi
 */
public class EmailDAOImplTest {

    @Rule
    public MethodLogger methodLogger = new MethodLogger();
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private MailConfigBean configBean;
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOImplTest.class);
    private EmailDAOImpl emailDAO;

    /**
     * The code of the following five methods :
     * seedAfterTestCompleted,seedDatabase,loadAsString,splitStatements and
     * isComment was taken from :
     * https://gitlab.com/omniprof/jdbc_demo/-/blob/master/src/test/java/com/kenfogel/jdbcdemo/unittests/DemoTestCase.java
     */
    /**
     * The database is recreated before each test.If the last test is
     * destructive then the database is in an unstable state. @AfterClass is
     * called just once when the test class is finished with by the JUnit
     * framework. It is instantiating the test class anonymously so that it can
     * execute its non-static seedDatabase routine.
     *
     * @throws java.sql.SQLException
     */
    @AfterClass
    public static void seedAfterTestCompleted() throws SQLException {
        LOG.info("@AfterClass seeding");
        new EmailDAOImplTest().seedDatabase();
    }

    /**
     * This routine recreates the database before every test.It makes sure that
     * a destructive test will not interfere with any other test. It does not
     * support stored procedures. This routine is courtesy of Bartosz Majsak, an
     * Arquillian developer at JBoss.
     *
     * @throws java.sql.SQLException
     */
    @Before
    public void seedDatabase() throws SQLException {
        configBean = new MailConfigBean();
        configBean.setDatabaseName("MAIL");
        configBean.setDatabaseURL("localhost");
        configBean.setDatabasePortNumber(3306);
        configBean.setDatabaseUserName("mailer");
        configBean.setDatabasePassword("dawson");
        emailDAO = new EmailDAOImpl(configBean);
        LOG.info("@Before seeding");
        final String seedDataScript = loadAsString("createEmailDB.sql");
        try ( Connection connection = emailDAO.getConnection();) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * Helper method for the seedDatabse method. Loads the sql statements inside
     * a SQL file as a string.
     */
    private String loadAsString(final String path) {
        try ( InputStream inputStream = Thread.currentThread().
                getContextClassLoader().
                getResourceAsStream(path);  Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    /**
     * Helper method for the seedDatabase method. Splits sql statements so that
     * they can be executed.
     *
     * @param reader
     * @param statementDelimiter
     * @return list of Statements
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                sqlStatement.append(' ');
                if (line.endsWith(statementDelimiter)) {
                    String sql = sqlStatement.toString().substring(0, sqlStatement.toString().length() - 1);
                    statements.add(sql);
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * Helper method to check if a line from a SQL file is a comment.
     *
     * @param line
     * @return boolean indicating whether the input line is a comment.
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

    @Test
    /**
     * Test for the create() method. Creates an EmailBean with a basic email
     * object. Checks that the email that was set is the same as the one found.
     */
    public void createBasicEmailBeanTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test createEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        int generatedId = emailDAO.create(emailBean);
        EmailBean foundBean = emailDAO.findID(generatedId);
        assertEquals(emailBean, foundBean);
    }

    @Test
    /**
     * Test for the create method. Checks that we can create an EmailBean that
     * contains a ReceivedEmail object in it. Checks that it gets assigned the
     * appropriate folder : INBOX
     */
    public void createReceivedEmailTest() throws SQLException, IOException, EmailLimitExceededException {
        ReceivedEmail received = ReceivedEmail.create().
                to("moonchild.zyx@gmail.com").
                from("blue.side.xyzy@gmail.com").
                subject("Test Received Email.");
        EmailBean emailBean = new EmailBean(received);
        int key = emailDAO.create(emailBean);
        emailDAO.findID(key);
        assertEquals(emailDAO.getFolder(emailBean), "INBOX");
    }

    @Test
    /**
     * Test for the create() method. Checks that if we pass null as an argument,
     * the method execution fails.
     */
    public void createNullTest() throws SQLException, IOException, EmailLimitExceededException {
        thrown.expect(NullPointerException.class);
        emailDAO.create(null);
    }

    @Test
    /**
     * Test for the create() method. Checks that the date of the email is set
     * properly.
     */
    public void createDateTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test createEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        int generatedId = emailDAO.create(emailBean);
        LocalDateTime dateNow = LocalDateTime.now();
        EmailBean foundBean = emailDAO.findID(generatedId);
        //By truncating a date to minutes, it can be compared.
        dateNow = dateNow.truncatedTo(ChronoUnit.MINUTES);
        LocalDateTime foundDate = foundBean.getDate();
        LOG.debug("Date found : " + foundDate);
        LOG.debug("Date now : " + dateNow);
        assertEquals(foundDate, dateNow);
    }

    @Test
    /**
     * Test for the create method. Checks that we cannot create an email with
     * null values in all of its fields.
     */
    public void createEmailWithNullValuesTest() throws SQLException, IOException, EmailLimitExceededException {
        thrown.expect(NullPointerException.class);
        Email email = Email.create();
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
    }

    @Test
    /**
     * Test for the create() method. Checks that the html text is set properly
     */
    public void createWithHtmlTest() throws SQLException, IOException, EmailLimitExceededException {
        String htmlSet = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Hello !</h1>"
                + "</body></html>";
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("swan.zyxz@gmail.com")
                .htmlMessage(htmlSet)
                .from("blue.side.xyzy@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        int generatedID = emailDAO.create(emailBean);
        EmailBean foundBean = emailDAO.findID(generatedID);
        foundBean.email.messages();
        String htmlFound = "";
        for (Object emessage : foundBean.email.messages()) {
            EmailMessage message = ((EmailMessage) (emessage));
            if (message.getMimeType().equals(MimeTypes.MIME_TEXT_HTML)) {
                htmlFound = message.getContent();
            }
        }
        assertEquals(htmlFound, htmlSet);
    }

    @Test
    /**
     * Test for the create() method. Checks that the html text and that the
     * embedded attachment is set properly
     */
    public void createWithHtmlAndEmbeddedFileTest() throws SQLException, IOException, EmailLimitExceededException {
        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is a cat picture embedded in "
                + "this email.</h1><img src='cid:cat.jpg'>"
                + "</body></html>";
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("swan.zyxz@gmail.com")
                .htmlMessage(htmlText)
                .from("blue.side.xyzy@gmail.com")
                .embeddedAttachment(EmailAttachment.with().content("testFiles//cat.jpg"));
        EmailBean emailBean = new EmailBean(email);
        int generatedID = emailDAO.create(emailBean);
        EmailBean foundBean = emailDAO.findID(generatedID);
        EmailAttachment testFile = null;
        Email foundEmail = (Email) foundBean.email;
        for (EmailAttachment file : foundEmail.attachments()) {
            if (file.isEmbedded()) {
                testFile = file;
            }
        }
        assertEquals(testFile.getContentId(), "cat.jpg");
    }

    @Test
    /**
     * Test for create email with a subject field that exceeds the limit set in
     * the database.
     */
    public void createEmailSubjectExceedsLimitTest() throws SQLException, IOException, EmailLimitExceededException {
        thrown.expect(EmailLimitExceededException.class);
        //A subject field's limit is 255 characters.
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 500; i++) {
            sb.append("a");
        }
        String subject = sb.toString();
        Email email = Email.create().subject(subject).from("moonchild.zyx@gmail.com").
                to("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
    }

    @Test
    /**
     * Test for create Email with a text field that exceeds the limit set in the
     * database.
     */
    public void createEmailTextExceedsLimitTest() throws SQLException, IOException, EmailLimitExceededException {
        thrown.expect(EmailLimitExceededException.class);
        //A text field's limit is 50000 characters in the database.
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 5555; i++) {
            sb.append("1");
        }
        String text = sb.toString();
        Email email = Email.create().
                subject("Test").
                from("moonchild.zyx@gmail.com")
                .textMessage(text)
                .to("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
    }

    @Test
    /**
     * Test for the update() method. Test the update operation with a modified
     * from field and 2 added attachments.
     */
    public void updateFieldAndAttachmentsTest() throws SQLException, IOException, EmailLimitExceededException {

        String[] toRecipients = {"moonchild.zyx@gmail.com",
            "swan.zyxz@gmail.com",
            "haku.xyz@gmail.com"};
        Email email = Email.create()
                .subject("Test updateEmail")
                .to(toRecipients)
                .from("moonchild.zyx@gmail.com");

        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is a cat picture embedded in "
                + "this email.</h1><img src='cid:cat.jpg'>"
                + "</body></html>";
        email.cc("swan.zyxz@gmail.com")
                .bcc("haku.xyz@gmail.com")
                .from("blue.side.xyzy@gmail.com")
                .htmlMessage(htmlText)
                .attachment(EmailAttachment.with().content("testFiles//moon.jpg"))
                .embeddedAttachment(EmailAttachment.with().content("testFiles//cat.jpg"));
        EmailBean emailBean = new EmailBean(email);
        int generatedId = emailDAO.create(emailBean);
        emailDAO.update(emailBean);
        EmailBean found = emailDAO.findID(generatedId);
        assertEquals(found, emailBean);
    }

    @Test
    /**
     * Test for the update() method. Checks that if we pass null as a parameter
     * to the update method, the method's execution fails.
     */
    public void updateNullTest() throws SQLException, IOException, EmailLimitExceededException {
        thrown.expect(NullPointerException.class);
        emailDAO.update(null);
    }

    @Test
    /**
     * Test for the updateFolder method. Checks that an EmailBean's folder is
     * correctly updated.
     */
    public void updateFolderTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        // When an email is created, its assigned folder is "DRAFT"
        emailDAO.updateFolder("SENT", emailBean);
        assertEquals(emailDAO.getFolder(emailBean), "SENT");
    }

    @Test
    /**
     * Test for the updateFolder method. Checks that an EmailBean's folder
     * cannot be updated from the DRAFT folder to the INBOX folder.
     */
    public void updateFolderFromDraftToInboxTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        // The folder assigned to a created email is DRAFT by default.
        // Attempt an update on the folder.
        emailDAO.updateFolder("INBOX", emailBean);
        // Check that the folder was not updated.
        assertEquals(emailDAO.getFolder(emailBean), "DRAFT");
    }

    @Test
    /**
     * Test for the updateFolder method. Checks that an EmailBean's folder does
     * not get updated if its current folder is SENT and its new folder is
     * DRAFT.
     */
    public void updateFolderFromSentToDraftTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        //Update emailBean's folder to SENT
        emailDAO.updateFolder("SENT", emailBean);
        // Attempt updating the emailBean's folder from SENT to DRAFT.
        emailDAO.updateFolder("DRAFT", emailBean);
        assertEquals(emailDAO.getFolder(emailBean), "SENT");
    }

    @Test
    /**
     * Test for the updateFolder method. Checks that an EmailBean's folder does
     * not get updated if its current folder is INBOX and its new folder is
     * DRAFT.
     */
    public void updateFolderFromInboxToDraftTest() throws SQLException, IOException, EmailLimitExceededException {
        ReceivedEmail email = ReceivedEmail.create()
                .subject("Test updateEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        //A ReceivedEmail object gets assigned the INBOX folder by default.
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        // Attempt to update the emailBean's folder to  DRAFT
        emailDAO.updateFolder("DRAFT", emailBean);

        assertEquals(emailDAO.getFolder(emailBean), "INBOX");
    }

    @Test
    /**
     * Test for the update method. Verifies that an email not in the DRAFT
     * folder cannot have its content updated.
     */
    public void updateEmailNotInDraftFolderTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        emailDAO.updateFolder("SENT", emailBean);
        int updated = emailDAO.update(emailBean);
        //The update method returns 1 if the emailBean has been updated and 0 if it was not.
        assertEquals(updated, 0);
    }

    @Test
    /**
     * Test for the update method. Checks that we cannot update an email that
     * does not exist in the database.
     */
    public void updateEmailThatDoesNotExistTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test deleteEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBeanWithNoId = new EmailBean(email);
        email.cc("blue.side.xyzy@gmail.com");
        int updated = emailDAO.update(emailBeanWithNoId);
        assertEquals(updated, 0);
    }

    @Test
    /**
     * Test for the update method. Checks that attachments are properly updated.
     */
    public void updateAddedANewAttachmentTest() throws SQLException, IOException, EmailLimitExceededException {
        String[] toRecipients = {
            "swan.zyxz@gmail.com",
            "haku.xyz@gmail.com"};
        List<File> attachments = new ArrayList<>();
        attachments.add(new File("testFiles//moon.jpg"));
        attachments.add(new File("testFiles//cat.jpg"));
        Email email = Email.create()
                .subject("Test updateEmail")
                .to("moonchild.zyx@gmail.com")
                .from("moonchild.zyx@gmail.com")
                .attachment(EmailAttachment.with().content("testFiles//moon.jpg"));
        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is a cat picture embedded in "
                + "this email.</h1><img src='cid:cat.jpg'>"
                + "</body></html>";
        email.to(toRecipients)
                .htmlMessage(htmlText)
                .attachment(EmailAttachment.with().content(attachments.get(0)))
                .attachment(EmailAttachment.with().content(attachments.get(1)))
                .embeddedAttachment(EmailAttachment.with().content("testFiles//cat.jpg"));
        EmailBean emailBean = new EmailBean(email);
        int generatedId = emailDAO.create(emailBean);
        emailDAO.update(emailBean);
        EmailBean beanFound = emailDAO.findID(generatedId);
        // Two emailBean objects are only equal if their content and all of their email's content is the same.
        assertEquals(beanFound, emailBean);
    }

    @Test
    /**
     * Test for the findID method. Checks that the email that was found is the
     * same that was created.
     */
    public void findIDEmailMatchTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test deleteEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        //Create an emailBean
        int generatedId = emailDAO.create(emailBean);
        //Find the emailBean
        EmailBean foundBean = emailDAO.findID(generatedId);
        assertEquals(emailBean, foundBean);
    }

    @Test
    /**
     * Test for the findID method. Checks that creating an emailBean object with
     * a specific id and comparing it to the emailBean returned by the findID
     * method with the same id returns an emailBean that is equal to it.
     */
    public void findIDFromTestDataTest() throws SQLException, IOException {
        //Setting EmailBean with ID : 1 and filling it with the same information it has in the database.
        Email email = Email.create().subject("TEST SUBJECT")
                .textMessage("TEST EMAIL 1")
                .from("moonchild.zyx@gmail.com")
                .to("blue.side.xyzy@gmail.com");
        EmailBean testBean = new EmailBean(1, 1, LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES), "moonchild.zyx", email);

        EmailBean foundBean = emailDAO.findID(1);
        //Compare the beans.
        assertEquals(testBean, foundBean);
    }

    @Test
    /**
     * Test for the delete() method. Checks that a deleted email does not exist
     * in the database.
     */
    public void deleteTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test deleteEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        int createdId = emailDAO.create(emailBean);
        emailDAO.delete(createdId);
        assertNull(emailDAO.findID(createdId));
    }

    @Test
    /**
     * Test for the delete method. Checks that we cannot delete an email that
     * does not exist in the database.
     */
    public void deleteEmailThatDoesNotExistTest() throws SQLException, IOException {
        int idDoesNotExist = 90;
        int deleted = emailDAO.delete(idDoesNotExist);
        assertEquals(deleted, 0);
    }

    @Test
    /**
     * Test for the findSubject method based on existing test data in the
     * database.
     */
    public void findSubjectTest() throws SQLException, IOException {
        //The mail database has two emails in the EMAIL table with the following subject : "TEST SUBJECT"
        String testSubject = "TEST SUBJECT";
        int expectedCount = 0;
        String selectQuery = "SELECT COUNT(*) FROM EMAIL WHERE SUBJECT = ?";
        try ( Connection connection = emailDAO.getConnection();  PreparedStatement selectStatement
                = connection.prepareStatement(selectQuery, Statement.RETURN_GENERATED_KEYS)) {
            selectStatement.setString(1, testSubject);
            ResultSet rs = selectStatement.executeQuery();
            if (rs.next()) {
                expectedCount = rs.getInt(1);
            }
        }
        List<EmailBean> foundEmails = emailDAO.findSubject(testSubject);
        assertEquals("Two emails should be found with the subject : " + testSubject, expectedCount, foundEmails.size());
    }

    @Test
    /**
     * Test for the create method. Sends an email from an email address to the
     * same email address. Checks that the received email gets assigned the
     * "INBOX" folder and that the sent email gets assigned the "SENT" folder
     * and that the types of each email are correctly set (ReceivedEmail or
     * Email).
     */
    public void sendEmailAndReceiveEmailFromSameAddressTest() throws SQLException, IOException, EmailLimitExceededException {
        Email sentEmail = Email.create()
                .subject("Test send and receive email from the same address.")
                .to("moonchild.zyx@gmail.com")
                .from("moonchild.zyx@gmail.com");
        ReceivedEmail receivedEmail = ReceivedEmail.create()
                .subject("Test send and receive email from the same address.")
                .to("moonchild.zyx@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean sentBean = new EmailBean(sentEmail);
        EmailBean receivedBean = new EmailBean(receivedEmail);
        emailDAO.create(sentBean);
        emailDAO.updateFolder("SENT", sentBean);
        emailDAO.create(receivedBean);
        boolean test = emailDAO.getFolder(sentBean).equals("SENT")
                && (sentBean.email instanceof Email)
                && emailDAO.getFolder(receivedBean).equals("INBOX")
                && (receivedBean.email instanceof ReceivedEmail);
        assertTrue(test);
    }

    @Test
    /**
     * Test for the update method. Checks that we cannot update an email that is
     * already sent.
     */
    public void updateOnSentEmailTest() throws SQLException, IOException, EmailLimitExceededException {
        Email sentEmail = Email.create()
                .subject("Test sentEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        sentEmail.subject("Updated sentEmail")
                .to("blue.side.xyzy@gmail.com")
                .cc("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(sentEmail);
        int key = emailDAO.create(emailBean);
        //Update folder to "SENT".
        emailDAO.updateFolder("SENT", emailBean);
        emailDAO.update(emailBean);
        EmailBean checkEmailBean = emailDAO.findID(key);
        assertEquals("Email should not be updated", checkEmailBean, emailBean);
    }

    @Test
    /**
     * Test for the update method. Checks that we cannot uopdate an email that
     * is already sent.
     */
    public void updateOnReceivedEmailTest() throws SQLException, IOException, EmailLimitExceededException {
        ReceivedEmail receivedEmail = ReceivedEmail.create()
                .subject("Test sentEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        receivedEmail.subject("Updated sentEmail")
                .to("blue.side.xyzy@gmail.com")
                .cc("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(receivedEmail);
        int key = emailDAO.create(emailBean);
        emailDAO.update(emailBean);
        EmailBean checkEmailBean = emailDAO.findID(key);
        assertEquals("Email should not be updated", checkEmailBean, emailBean);
    }

    @Test
    /**
     * Test for the findAll() method. Checks the current amount of emails in the
     * database. The amount should be 20.
     */
    public void findAllTest() throws SQLException, IOException {
        List<EmailBean> found = emailDAO.findAll();
        assertEquals("testFindAll: ", 20, found.size());
    }

    @Test
    /**
     * Test for the findAll method. Checks that Emails in the inbox folder are
     * retrieved as ReceivedEmail objects.
     */
    public void findAllReceivedTest() throws SQLException, IOException {
        List<EmailBean> foundEmails = emailDAO.findAll();
        // Emails with id 18,19,20 in the database are received emails.
        List<EmailBean> receivedEmails = new ArrayList<>();
        receivedEmails.add(foundEmails.get(17));
        receivedEmails.add(foundEmails.get(18));
        receivedEmails.add(foundEmails.get(19));
        for (EmailBean received : receivedEmails) {
            assertEquals((received.email instanceof ReceivedEmail), true);
        }
    }

    @Test
    /**
     * Test for the findAll method. Checks that Emails in the draft folder or
     * the sent folder are retrieved as Email objects.
     */
    public void findAllSentTest() throws SQLException, IOException {
        List<EmailBean> foundEmails = emailDAO.findAll();
        // All emails except those with id 18,19,20 
        // in the database are either draft emails or sent emails, 
        // Therefore, their type is Email.
        int size = foundEmails.size() - 3;
        for (int i = 0; i < size; i++) {
            CommonEmail foundEmail = foundEmails.get(i).email;
            assertEquals((foundEmail instanceof Email), true);
        }
    }

    @Test
    /**
     * Test for the findFrom method. Verifies that the list of EmailBean objects
     * found all have an email with the same address in the from field.
     */
    public void findFromAddressExistsTest() throws SQLException, IOException {
        List<EmailBean> found = emailDAO.findFrom("moonchild.zyx@gmail.com");
        found.forEach(emailBean -> {
            assertEquals("Each email should have the same from address. ", "moonchild.zyx@gmail.com", emailBean.email.from().getEmail());
        });

    }

    @Test
    /**
     * Test for the findFrom method with an email address that does not exist in
     * the database.
     */
    public void findFromAddressThatDoesNotExistTest() throws SQLException, IOException {
        List<EmailBean> found = emailDAO.findFrom("DoesNotExist@gmail.com");
        assertEquals(found.isEmpty(), true);
    }

    @Test
    /**
     * Test for the findFrom method with after insert a new Email record.
     */
    public void findFromAfterInsert() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test createEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("hellohelloDoesNotExist@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        //Add record
        emailDAO.create(emailBean);
        List<EmailBean> found = emailDAO.findFrom("hellohelloDoesNotExist@gmail.com");
        assertEquals(found.size(), 1);
    }

    @Test
    /**
     * Test for the findFrom method with after deleting an Email record.
     */
    public void findFromAfterDelete() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test createEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("hellohelloDoesNotExist@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        //Add record
        int generatedID = emailDAO.create(emailBean);
        //Delete the record
        emailDAO.delete(generatedID);
        List<EmailBean> found = emailDAO.findFrom("hellohelloDoesNotExist@gmail.com");
        assertEquals(found.isEmpty(), true);
    }

    @Test
    /**
     * Test for findAll after adding a new record.
     */
    public void findAllAfterAddingRecordTest() throws SQLException, IOException, EmailLimitExceededException {
        Email email = Email.create()
                .subject("Test createEmail")
                .to("blue.side.xyzy@gmail.com")
                .from("moonchild.zyx@gmail.com");
        EmailBean emailBean = new EmailBean(email);
        //Add record
        emailDAO.create(emailBean);
        //Check
        //The database currently has 20 records in the email table, 
        //With the newly inserted record, it should have 21 records.
        List<EmailBean> foundEmailBeans = emailDAO.findAll();
        assertEquals("The database should have 21 records now.", foundEmailBeans.size(), 21);
    }

    @Test
    /**
     * Test for findAll after deleting a new record.
     */
    public void findAllAfterDeletingRecordTest() throws SQLException, IOException {
        //Delete record with id 1.
        emailDAO.delete(1);
        //Check
        //The database currently has 20 records in the email table, 
        //If we delete a record it should have 19 records.
        List<EmailBean> foundEmailBeans = emailDAO.findAll();
        assertEquals("The database should have 19 records now.", foundEmailBeans.size(), 19);
    }

    @Test
    /**
     * Test for the findWith method. Makes sure that all the emailBeans found
     * contain the same address that was given as input to the findWith method)
     * in the to field.
     */
    public void findWithTOTest() throws SQLException, IOException {
        String address = "moonchild.zyx@gmail.com";
        String recipientType = "TO";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        for (EmailBean emailBean : foundEmails) {
            List<String> listEmails = new ArrayList<>();
            for (EmailAddress to : emailBean.email.to()) {
                listEmails.add(to.getEmail());
            }
            assertEquals(listEmails.contains(address), true);
        }
    }

    @Test
    /**
     * Test for the findWith method. Makes sure that all the emailBeans found
     * contain the same address that was given as input to the findWith method)
     * in the to field.
     */
    public void findWithCCTest() throws SQLException, IOException {
        String address = "moonchild.zyx@gmail.com";
        String recipientType = "CC";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        for (EmailBean emailBean : foundEmails) {
            List<String> listEmails = new ArrayList<>();
            for (EmailAddress cc : emailBean.email.cc()) {
                listEmails.add(cc.getEmail());
            }
            assertEquals(listEmails.contains(address), true);
        }
    }

    @Test
    /**
     * Test for the findWith method. Makes sure that all the emailBeans found
     * contain the same address that was given as input to the findWith method)
     * in the to field.
     */
    public void findWithBCCTest() throws SQLException, IOException {
        String address = "moonchild.zyx@gmail.com";
        String recipientType = "BCC";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        for (EmailBean emailBean : foundEmails) {
            List<String> listEmails = new ArrayList<>();
            for (EmailAddress bcc : ((Email) emailBean.email).bcc()) {
                listEmails.add(bcc.getEmail());
            }
            assertEquals(listEmails.contains(address), true);
        }
    }

    @Test
    /**
     * Test for the findWith method. Makes sure that all the emailBeans found
     * contain the same address that was given as input to the findWith method)
     * in the to field.
     */
    public void findWithNoneFoundTest() throws SQLException, IOException {
        String address = "emailAddressNotInTheDatabase@gmail.com";
        String recipientType = "CC";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        assertEquals(foundEmails.isEmpty(), true);
    }

    @Test
    /**
     * Test for the findWith method. Checks that the method includes newly
     * inserted addresses in the beans it finds.
     */
    public void findWithAfterInsertTest() throws SQLException, IOException, EmailLimitExceededException {
        String address = "emailAddressNotInTheDatabase@gmail.com";
        Email email = Email.create()
                .subject("Test")
                .from("moonchild.zyx@gmail.com").
                to(address);
        EmailBean emailBean = new EmailBean(email);
        emailDAO.create(emailBean);
        String recipientType = "TO";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        for (EmailBean foundBean : foundEmails) {
            List<String> listEmails = new ArrayList<>();
            for (EmailAddress to : foundBean.email.to()) {
                listEmails.add(to.getEmail());
            }
            assertEquals(listEmails.contains(address), true);
        }
    }

    @Test
    /**
     * Test for the findWith method. Checks that the method does not include
     * deleted records in the beans it finds.
     */
    public void findWithAfterDeleteTest() throws SQLException, IOException, EmailLimitExceededException {
        String address = "emailAddressNotInTheDatabase@gmail.com";
        Email email = Email.create()
                .subject("Test")
                .from("moonchild.zyx@gmail.com").
                cc(address);
        EmailBean emailBean = new EmailBean(email);
        int generatedID = emailDAO.create(emailBean);
        LOG.info("gen id : " + generatedID);
        emailDAO.delete(generatedID);
        String recipientType = "CC";
        List<EmailBean> foundEmails = emailDAO.findWith(address, recipientType);
        assertEquals(foundEmails.isEmpty(), true);
    }

    @Test
    /**
     * Test for the removeFolder method. Removes the SENT folder and checks that
     * the remaining emails in the database are not in the SENT folder.
     */
    public void removeFolderTest() throws SQLException, IOException {
        List<EmailBean> emailBeans = emailDAO.findAll();
        List<EmailBean> emailsInSentFolder = new ArrayList<>();
        for (EmailBean emailBean : emailBeans) {
            if (emailDAO.getFolder(emailBean).equals("SENT")) {
                emailsInSentFolder.add(emailBean);
            }
        }
        emailDAO.removeFolder("SENT");
        int remainingEmails = emailBeans.size() - emailsInSentFolder.size();
        // Refresh
        emailBeans = emailDAO.findAll();
        assertEquals(emailBeans.size(), remainingEmails);
    }

}
