package com.daniabh.emailclient.test.mailaction;

import com.daniabh.emailclient.business.Mailer;
import com.daniabh.emailclient.exception.InvalidBCCException;
import com.daniabh.emailclient.exception.InvalidCCException;
import com.daniabh.emailclient.exception.InvalidConfigAddressException;
import com.daniabh.emailclient.exception.InvalidTOException;
import com.daniabh.emailclient.properties.MailConfigBean;
import com.daniabh.emailclient.test.MethodLogger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailException;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 * Test class for the Mailer class. Tests whether the sendEmail method handles
 * messages, attachments,embedded attachments, html text and recipients
 * properly. Tests whether the receiveEmail method receives emails correctly,
 * with multiple receivers, CC,BCC and TO recipients.
 *
 * @author Dania Bouhmidi
 */
public class MailerTest {

    //A Rule is implemented as a class with methods that are associated
    // with the lifecycle of a unit test. These methods run when required.
    // Avoids the need to cut and paste code into every test method.
    @Rule
    public MethodLogger methodLogger = new MethodLogger();
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MailerTest.class);

    private MailConfigBean sendConfigBean;
    private Mailer mailer;
    //List containing the email host and credentials of 4 gmail accounts for testing purposes.
    private List<MailConfigBean> receivers = Arrays.asList(
            new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "haku.xyz@gmail.com", "9098abcd"),
            new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "moonchild.zyx@gmail.com", "912qwerty"),
            new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "swan.zyxz@gmail.com", "M3frW7jynGjXDmd"),
            new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "blue.side.xyzy@gmail.com", "9087abcd"));

    @Before
    /**
     * Initializes the sendConfigBean object. Deletes emails from each of the
     * MailConfigBean objects in the receivers ArrayList, so that the first
     * email in the array returned by the receiveEmail method is the appropriate
     * email.
     */
    public void init() {
        sendConfigBean = new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "moonchild.zyx@gmail.com", "912qwerty");
        mailer = new Mailer(sendConfigBean);
        for (int i = 0; i < receivers.size(); i++) {
            ImapServer imapServer = MailServer.create()
                    .host("imap.gmail.com")
                    .ssl(true)
                    .auth(receivers.get(i).getUserEmailAddress(), receivers.get(i).getEmailPassword())
                    .buildImapMailServer();

            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                //delete all emails from the current receiver's account.
                session.receiveEmailAndDelete();
            }
        }

    }

    @Test
    /**
     * Test for sendEmail with null values for all parameters except the toField
     * List.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailNullValuesExceptToFieldTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidTOException, InvalidBCCException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                null, null, null, toField, null, null);

        pause();
        //Assert
        assertNotNull(sentEmail);
    }

    @Test
    /**
     * Test for sendEmail with a null send MailConfigBean.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailNullSendBeanTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        mailer = new Mailer(null);
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                null, null, null, toField, null, null);

        pause();
        //Assert
        assertNull(sentEmail);
    }

    @Test
    /**
     * Test for an email's subject text. Verifies that the subject's text that
     * was sent as a parameter to the sendEmail method is the same as the one
     * retrieved from the sent Email object.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailSubjectTextTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : To Recipients", "Hello this is a test message", null, toField, null, null);

        pause();
        //Assert
        Assert.assertEquals("Test Message : To Recipients", sentEmail.subject());
    }

    @Test
    /**
     * Test for an email sent with plain text. Verifies that the text sent as a
     * parameter is the same as the text message retrieved from the email.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailMessageTextTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message", "Hello this is a test message", null, toField, null, null);

        pause();
        //Assert
        EmailMessage textMessage = null;
        for (EmailMessage message : sentEmail.messages()) {
            if (message.getMimeType().equals(MimeTypes.MIME_TEXT_PLAIN)) {
                textMessage = message;
                LOG.info(message.getContent());
            }
        }
        Assert.assertEquals("Hello this is a test message", textMessage.getContent());
    }

    @Test
    /**
     * Test for sendEmail with the to (recipients) field. Verifies that the to
     * addresses were properly set.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailToRecipientsTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidTOException, InvalidBCCException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : To Recipients", "Hello this is a test message", null, toField, null, null);

        pause();
        String[] actualAddresses = convertEmailAddressesToStrings(sentEmail.to());
        String[] expectedAddresses = new String[toField.size()];
        toField.toArray(expectedAddresses);
        //Assert
        Assert.assertArrayEquals(actualAddresses, expectedAddresses);

    }

    @Test
    /**
     * Test for sendEmail with CC recipients. Verifies that the cc addresses
     * were properly set.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailCCRecipientsTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> ccField = new ArrayList<>();
        ccField.add("blue.side.xyzy@gmail.com");
        ccField.add("swan.zyxz@gmail.com");
        ccField.add("haku.xyz@gmail.com");
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : CC Recipients", "Hello this is a test message", null, toField, ccField, null);

        pause();
        String[] actualAddresses = convertEmailAddressesToStrings(sentEmail.cc());
        String[] expectedAddresses = new String[ccField.size()];
        ccField.toArray(expectedAddresses);
        //Assert
        Assert.assertArrayEquals(actualAddresses, expectedAddresses);

    }

    @Test
    /**
     * Test for sendEmail with BCC recipients. Verifies that the bcc addresses
     * were properly set.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailBCCRecipientsTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> bccField = new ArrayList<>();
        bccField.add("blue.side.xyzy@gmail.com");
        bccField.add("swan.zyxz@gmail.com");
        bccField.add("haku.xyz@gmail.com");
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : BCC Recipients", "Hello this is a test message", null, toField, null, bccField);
        pause();
        String[] actualAddresses = convertEmailAddressesToStrings(sentEmail.bcc());
        String[] expectedAddresses = new String[bccField.size()];
        bccField.toArray(expectedAddresses);
        //Assert
        Assert.assertArrayEquals(actualAddresses, expectedAddresses);
    }

    @Test(expected = IllegalArgumentException.class)
    /**
     * Test for sendEmail without recipients. Verifies that an
     * IllegalArgumentException is thrown.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailNoRecipientsTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidTOException, InvalidBCCException {
        //Act
        mailer.sendEmail(null, null,
                "Test Message : No Recipients", "Hello, this is a test message", null, null, null, null);
        fail("Did not throw IllegalArgumentException");
    }

    @Test
    /**
     * Test for sendEmail with htmlText.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailWithHtmlTextTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        String htmlTextToSet = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Testing sendEmail. "
                + "</h1>"
                + "<h2>Html test text.</h2></body></html>";

        String textMessage = "This is a test for the setContent method."
                + "(testing html text.)";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : Html text.", textMessage, htmlTextToSet, toField, null, null);
        pause();
        String actualHtmlMessage = "";
        for (EmailMessage message : sentEmail.messages()) {
            if (message.getMimeType().equals(MimeTypes.MIME_TEXT_HTML)) {
                actualHtmlMessage = message.getContent();
                LOG.info(message.getContent());
            }
        }
        //Assert
        Assert.assertEquals(htmlTextToSet, actualHtmlMessage);

    }

    @Test
    /**
     * Test for sendEmail with an embedded attachment.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailWithEmbeddedAttachmentTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        List<File> embeddedAttachments = new ArrayList<>();
        embeddedAttachments.add(new File("testFiles//cat.jpg"));
        String textMessage = "This is a test for the setContent method.";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is a cat picture embedded in "
                + "this email.</h1><img src='cid:cat.jpg'>"
                + "</body></html>";
        //Act
        Email sentEmail = mailer.sendEmail(null, embeddedAttachments, "Test Message : Embedded Attachment.",
                textMessage, htmlText, toField, null, null);

        pause();
        boolean test = sentEmail.attachments().get(0).isEmbedded()
                && sentEmail.attachments().get(0).getContentId().equals("cat.jpg")
                && sentEmail.attachments().get(0).getName().equals("cat.jpg");
        //Assert
        assertTrue("The file attached to the email should be embedded in the html text.", test);
    }

    @Test
    /**
     * Test for sendEmail with a regular attachment.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailWithNormalFileAttachmentTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        List<File> emailAttachments = new ArrayList<>();
        emailAttachments.add(new File("testFiles//moon.jpg"));

        String textMessage = "This is a test for the setContent method.";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(emailAttachments, null, "Test Message :  Attachment.",
                textMessage, null, toField, null, null);

        pause();
        boolean test = (!sentEmail.attachments().get(0).isEmbedded())
                && sentEmail.attachments().get(0).getName().equals("moon.jpg");
        //Assert
        assertTrue("The file should be attached to the email and it should not be embedded.", test);
    }

    @Test
    /**
     * Test for sendEmail with an embedded attachment and a regular attachment.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidTOException
     * @throws InvalidBCCException
     */
    public void sendEmailWith2TypesOfAttachmentsTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidTOException, InvalidBCCException {
        //Arrange
        List<File> embeddedAttachments = new ArrayList<>();
        embeddedAttachments.add(new File("testFiles//cat.jpg"));
        List<File> emailAttachments = new ArrayList<>();
        emailAttachments.add(new File("testFiles//moon.jpg"));
        String textMessage = "This is a test for the setContent method.";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is a cat picture embedded in "
                + "this email.</h1><img src='cid:cat.jpg'>"
                + "</body></html>";
        //Act
        Email sentEmail = mailer.sendEmail(emailAttachments, embeddedAttachments, "Test Message : 2 Types of Attachments.",
                textMessage, htmlText, toField, null, null);

        pause();
        boolean test = sentEmail.attachments().get(0).isEmbedded() == false && sentEmail.attachments().get(1).isEmbedded();
        assertTrue("The two files attached were not handled properly.", test);
    }

    @Test(expected = FileNotFoundException.class)
    /**
     * Test for sendEmail with an attachment that contains a non existent file.
     * Verifies that a FileNotFoundException is thrown.
     *
     * @throws IOException
     * @throws InvalidConfigAddressException
     * @throws InvalidCCException
     * @throws InvalidBCCException
     * @throws InvalidTOException
     */
    public void sendEmailWithNonExistentFileAttachmentTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        List<File> emailAttachments = new ArrayList<>();
        emailAttachments.add(new File("doesNotExist.jpg"));

        String textMessage = "This is a test for the setContent method.";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        mailer.sendEmail(emailAttachments, null, "Test Message : Non Existent Attachment.",
                textMessage, null, toField, null, null);

        pause();
        fail("Did not throw FileNotFoundException");

    }

    /**
     * Test for sendEmail with an invalid sending email address.Verifies that an
     * InvalidEmailAddressException is thrown.
     *
     * @throws IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test(expected = InvalidConfigAddressException.class)
    public void sendEmailFromInvalidSendEmailAdressTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean invalidSendConfigBean
                = new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "   xyzy12345.this.email.does.not.exist.either.@gmail.com", "908123454yhfgvbfd");

        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        mailer = new Mailer(invalidSendConfigBean);
        //Act
        mailer.sendEmail(null, null, "Test for the sendEmail method",
                "Email addresses do not exist.", null, toField, null, null);
        fail("Did not throw InvalidEmailException");

    }

    /**
     * Test for sendEmail with a sending email address with an invalid
     * password.Verifies that a MailException is thrown.
     *
     * @throws IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test(expected = MailException.class)
    public void sendEmailWithInvalidFromEmailPasswordTest() throws IOException, InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean invalidSendConfigBean
                = new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "moonchild.zyx@gmail.com", "908123454yhfgvbfdinvalid");

        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        mailer = new Mailer(invalidSendConfigBean);
        //Act
        mailer.sendEmail(null, null, "Test for the sendEmail method",
                "Email password is invalid.", null, toField, null, null);
        fail("Did not throw MailException");
    }

    /**
     * Test for sendEmail with one invalid to field email address.Verifies that
     * an exception is thrown and that its message contains the invalid emaill
     * address.
     *
     * @throws IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     */
    @Test
    public void sendEmailWithOneInvalidToFieldEmailAdressTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        toField.add("this.email.does.not.exist");
        //Act
        String actual = null;
        String expected = "this.email.does.not.exist";
        try {
            mailer.sendEmail(null, null, "Test for the sendEmail method",
                    "One of the toField email addresses does not exist.", null, toField, null, null);
        } catch (InvalidTOException e) {
            actual = e.getInvalidRecipient();
        }
        assertEquals(actual, expected);

    }

    /**
     * Test for sendEmail with one invalid cc field email address.Verifies that
     * an exception is thrown and that its message contains the invalid emaill
     * address.
     *
     * @throws IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test
    public void sendEmailWithOneInvalidCCFieldEmailAdressTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> ccField = new ArrayList<>();
        ccField.add("haku.xyz@gmail.com");
        ccField.add("this.email.does.not.exist");
        //Act
        String actual = null;
        String expected = "this.email.does.not.exist";
        try {
            mailer.sendEmail(null, null, "Test for the sendEmail method",
                    "One of the ccField email addresses does not exist.", null, null, ccField, null);
        } catch (InvalidCCException e) {
            actual = e.getInvalidRecipient();
        }
        assertEquals(actual, expected);

    }

    /**
     * Test for sendEmail with one invalid cc field email address. Verifies that
     * an exception is thrown and that its message contains the invalid emaill
     * address.
     *
     * @throws IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test
    public void sendEmailWithOneInvalidBCCFieldEmailAdressTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> bccField = new ArrayList<>();
        bccField.add("haku.xyz@gmail.com");
        bccField.add("this.email.does.not.exist");
        //Act
        String actual = null;
        String expected = "this.email.does.not.exist";
        try {
            mailer.sendEmail(null, null, "Test for the sendEmail method",
                    "One of the ccField email addresses does not exist.", null, null, null, bccField);
        } catch (InvalidBCCException e) {
            actual = e.getInvalidRecipient();
        }
        assertEquals(actual, expected);

    }

    /**
     * Test for receiveEmail with multiple emails received.Verifies that after
     * sending multiple emails to one address, the receiveEmail method with a
     * MailConfigBean with that address returns an array of 5 ReceivedEmails.
     *
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     */
    @Test
    public void receiveEmailMultipleEmailsTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean receiveConfigBean = receivers.get(0);
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        for (int i = 0; i < 5; i++) {
            mailer.sendEmail(null, null, "Test Message : receiveEmailMultipleEmailsTest",
                    "Hello this is a test message #" + i, null, toField, null, null);
        }
        pause();
        //Assert
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receiveConfigBean);
        assertEquals(receivedEmails.length, 5);

    }

    /**
     * Test for receiveEmail with zero mails received.Verifies that the array of
     * ReceivedEmails is empty, but not null.
     *
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test
    public void receiveEmailZeroEmailsTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean receiveConfigBean = receivers.get(0);
        //Act
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receiveConfigBean);
        //Assert
        assertEquals(receivedEmails.length, 0);

    }

    /**
     * Test for receiveEmail with multiple receivers in the to field with a
     * basic email without cc,bcc and attachments.
     *
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test
    public void receiveEmailMultipleToReceiversTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        toField.add("blue.side.xyzy@gmail.com");
        toField.add("swan.zyxz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null, "Test Message : receiveEmailMultipleReceiversTest",
                "Hello this is a test message", null, toField, null, null);
        pause();
        //Assert
        for (MailConfigBean receiveConfigBean : receivers) {
            // Check if it is not the sender's email address.
            if (!receiveConfigBean.getUserEmailAddress().equals("moonchild.zyx@gmail.com")) {
                ReceivedEmail[] receivedEmails = mailer.receiveEmail(receiveConfigBean);
                assertTrue(compareEmails(sentEmail, receivedEmails[0]));
            }
        }

    }

    @Test
    /**
     * Test for receiveEmail that compares the attachment that was sent and the
     * attachment received.
     */
    public void receiveEmailContentWithAttachmentTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        List<File> emailAttachments = new ArrayList<>();
        emailAttachments.add(new File("testFiles//moon.jpg"));

        String textMessage = "This is a test for the setContent method.";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(emailAttachments, null, "Test Message : Attachment.",
                textMessage, null, toField, null, null);

        pause();
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receivers.get(0));
        ReceivedEmail receivedEmail = receivedEmails[0];
        File attachedFile = emailAttachments.get(0);

        EmailAttachment<? extends DataSource> attachment = receivedEmail.attachments().get(0);
        File receivedFile = new File("temp", attachment.getName());
        receivedFile.createNewFile();
        attachment.writeToFile(receivedFile);

        byte[] attachedFileContent = Files.readAllBytes(attachedFile.toPath());
        byte[] receivedFileContent = Files.readAllBytes(receivedFile.toPath());
        //Assert
        Assert.assertArrayEquals(attachedFileContent, receivedFileContent);

    }

    @Test
    /**
     * Test for receiveEmail from a CC email address. Compares the sent Email's
     * CC field with the ReceivedEmail's CC field.
     */
    public void receiveEmailFromCCAddressTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean receiveConfigBean = receivers.get(3);
        ArrayList<String> ccField = new ArrayList<>();
        ccField.add(receiveConfigBean.getUserEmailAddress());
        ccField.add("haku.xyz@gmail.com");
        Email sentEmail = mailer.sendEmail(null, null, "Test Message : Receive Email Sent With CC From a CC Account.",
                "Test message", null, null, ccField, null);
        pause();
        //Act
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receiveConfigBean);
        ReceivedEmail receivedEmail = receivedEmails[0];
        String[] actualAddresses = convertEmailAddressesToStrings(receivedEmail.cc());
        String[] expectedAddresses = convertEmailAddressesToStrings(sentEmail.cc());
        //Assert
        Assert.assertArrayEquals(expectedAddresses, actualAddresses);
    }

    @Test
    /**
     * Test for receiveEmail with CC, BCC, TO Compares the sent Email's fields
     * with the ReceivedEmail's fields.
     */
    public void receiveEmailFromBCCAddressTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean receiveConfigBean = receivers.get(2);
        ArrayList<String> ccField = new ArrayList<>();
        ccField.add("blue.side.xyzy@gmail.com");
        ccField.add("haku.xyz@gmail.com");
        ArrayList<String> bccField = new ArrayList<>();
        bccField.add("blue.side.xyzy@gmail.com");
        bccField.add("swan.zyxz@gmail.com");
        bccField.add("haku.xyz@gmail.com");
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        Email sentEmail = mailer.sendEmail(null, null, "Test Message : Receive Email Sent With CC,BCC and TO From a CC Account.",
                "Test message", null, toField, ccField, bccField);
        pause();
        //Act
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receiveConfigBean);
        ReceivedEmail receivedEmail = receivedEmails[0];
        String[] actualAddresses = convertEmailAddressesToStrings(receivedEmail.cc());
        String[] expectedAddresses = convertEmailAddressesToStrings(sentEmail.cc());
        //Assert
        Assert.assertArrayEquals(expectedAddresses, actualAddresses);
    }

    /**
     * Test for receiveEmail with invalid receive address.An
     * InvalidEmailException is expected.
     *
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     */
    @Test(expected = InvalidConfigAddressException.class)
    public void receiveEmailFromInvalidAddressTest() throws InvalidConfigAddressException {
        //Arrange
        MailConfigBean receiveConfigBean = new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "invalidReceiveEmailAddress", "passwordABCDEF");
        //Act
        mailer.receiveEmail(receiveConfigBean);

        fail("Did not throw InvalidEmailException");
    }

    /**
     * Test for receiveEmail with invalid password for the receiving email
     * address.
     *
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    @Test(expected = MailException.class)
    public void receiveEmailWithInvalidPasswordTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        MailConfigBean receiveConfigBean = new MailConfigBean("imap.gmail.com", "smtp.gmail.com", "haku.xyz@gmail.com", "thisIsAnInvalidPassword");
        String textMessage = "This is a test for the receiveEmail method."
                + "(testing receiveEmail with a a valid email, and a wrong password.)";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        mailer.sendEmail(null, null,
                "Test Message : Html text.", textMessage, null, toField, null, null);
        pause();
        //Act
        mailer.receiveEmail(receiveConfigBean);
        fail("Did not throw MailException");
    }

    @Test
    /**
     * Test for receiveEmail with html text. Verifies that the html text of the
     * sent Email object is the same as the html text of the received
     * ReceivedEmail object.
     */
    public void receiveEmailWithHtmlTextTest() throws IOException, InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {
        //Arrange
        String htmlText = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Testing receiveEmail. "
                + "</h1>"
                + "<h2>Html test text.</h2></body></html>";

        String textMessage = "This is a test for the receiveEmail method."
                + "(testing html text.)";
        ArrayList<String> toField = new ArrayList<>();
        toField.add("haku.xyz@gmail.com");
        //Act
        Email sentEmail = mailer.sendEmail(null, null,
                "Test Message : Html text.", textMessage, htmlText, toField, null, null);
        pause();
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(receivers.get(0));
        ReceivedEmail receivedEmail = receivedEmails[0];
        String receivedHtmlMessage = "receivedHtml";
        String sentHtmlMessage = "sentHtml";
        for (int i = 0; i < sentEmail.messages().size(); i++) {
            if (sentEmail.messages().get(i).getMimeType().equals(MimeTypes.MIME_TEXT_HTML)) {
                sentHtmlMessage = sentEmail.messages().get(i).getContent();
            }
            if (receivedEmail.messages().get(i).getMimeType().equals("TEXT/HTML")) {
                receivedHtmlMessage = receivedEmail.messages().get(i).getContent();
            }
        }

        //Assert   
        Assert.assertEquals(sentHtmlMessage, receivedHtmlMessage);

    }

    /**
     * Helper method to add a (five) second pause to allow the Gmail server to
     * receive what has been sent.
     */
    private void pause() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
        }
    }

    /**
     * Helper method to convert an array of EmailAddress objects to a String
     * array for comparison purposes.
     *
     * @param emails
     * @return
     */
    private String[] convertEmailAddressesToStrings(EmailAddress[] emails) {
        String[] stringEmails = new String[emails.length];
        for (int i = 0; i < emails.length; i++) {
            stringEmails[i] = emails[i].getEmail();
        }
        return stringEmails;
    }

    /**
     * Helper method to compare the cc,bcc, from, subject and htmlText fields of
     * an Email object and a ReceivedEmail object.
     *
     * @param sentEmail
     * @param receivedEmail
     * @return a boolean representing whether the 2 emails matched.
     */
    private boolean compareEmails(Email sentEmail, ReceivedEmail receivedEmail) throws IOException {
        boolean match = true;
        if (receivedEmail.to().length == sentEmail.to().length) {

            for (int i = 0; i < receivedEmail.to().length; i++) {
                LOG.info("Received Email to :" + receivedEmail.to()[i]);
                LOG.info("Sent Email to :" + sentEmail.to()[i]);
                if (!receivedEmail.to()[i].getEmail().equals(sentEmail.to()[i].getEmail())) {
                    match = false;
                }
            }
        }

        if (sentEmail.cc() != null) {
            for (int i = 0; i < receivedEmail.cc().length; i++) {
                LOG.info("Received Email cc :" + receivedEmail.cc()[i]);
                LOG.info("Sent Email cc :" + sentEmail.cc()[i]);
                if (!receivedEmail.cc()[i].getEmail().equals(sentEmail.cc()[i].getEmail())) {
                    match = false;
                }
            }
        }
        LOG.info("Received Email to :" + receivedEmail.subject());
        LOG.info("Sent Email to :" + sentEmail.subject());
        LOG.info("Received Email first message content :" + receivedEmail.messages().get(0).getContent());
        LOG.info("Sent Email first message content :" + sentEmail.messages().get(0).getContent());
        if (sentEmail.from().equals(receivedEmail.from())) {
            if (sentEmail.subject().equals(receivedEmail.subject())) {
                if (compareEmailMessages(sentEmail, receivedEmail)) {
                    if (!compareFileAttachments(sentEmail, receivedEmail)) {
                        match = false;
                    }
                }
            }
        }

        return match;
    }

    /**
     * Helper method to compare the EmailMessage objects contained inside an
     * Email object and a ReceivedEmail object.
     *
     * @param sentEmail
     * @param receivedEmail
     * @return
     */
    private boolean compareEmailMessages(Email sentEmail, ReceivedEmail receivedEmail) {
        if (sentEmail.messages().size() == receivedEmail.messages().size()) {
            for (int i = 0; i < sentEmail.messages().size(); i++) {
                if (!sentEmail.messages().get(i).getMimeType().equals(receivedEmail.messages().get(i).getMimeType())) {
                    return false;
                }
                if (!sentEmail.messages().get(i).getContent().equals(receivedEmail.messages().get(i).getContent())) {
                    return false;
                }
            }

        } else {
            return false;
        }
        return true;
    }

    /**
     * Helper method to compare email the attachments of an Email object and a
     * ReceivedEmail object.
     *
     * @param sentEmail
     * @param receivedEmail
     * @return
     * @throws IOException
     */
    private boolean compareFileAttachments(Email sentEmail, ReceivedEmail receivedEmail) throws IOException {
        File receivedFile;
        File sentFile;
        for (EmailAttachment attachment : receivedEmail.attachments()) {
            receivedFile = new File("temp", attachment.getName());
            receivedFile.createNewFile();
            attachment.writeToFile(receivedFile);
            for (EmailAttachment sentAttachment : sentEmail.attachments()) {
                sentFile = new File("temp", attachment.getName());
                sentFile.createNewFile();
                sentAttachment.writeToFile(sentFile);

                byte[] attachedFileContent = Files.readAllBytes(sentFile.toPath());
                byte[] receivedFileContent = Files.readAllBytes(receivedFile.toPath());
                if (!Arrays.equals(attachedFileContent, receivedFileContent)) {
                    return false;
                }
            }
        }
        return true;
    }
}
