package com.daniabh.emailclient.emailbean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import jodd.mail.CommonEmail;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;

/**
 * Bean to hold an Email object and important information about an email such as
 * its folder, the username of the sender, and its received/sent date. This bean
 * also has methods that allow the comparison of its Email component.
 *
 * @author Dania Bouhmidi
 */
public class EmailBean implements Serializable {
    
    private int id;
    private int folderKey;
    private LocalDateTime date;
    private String username;
    
    public CommonEmail email;

    /**
     * Non-default constructor : sets the CommonEmail object. Sets the username
     * field to the first part of the from email address.
     *
     * @param email
     */
    public EmailBean(final CommonEmail email) {
        this.email = email;
        this.username = email.from().getEmail().substring(0, email.from().getEmail().indexOf("@"));
    }

    /**
     * Non-default constructor Sets all the parameters of the EmailBean.
     *
     * @param id
     * @param folderKey
     * @param date
     * @param username
     * @param email
     */
    public EmailBean(final int id, final int folderKey, final LocalDateTime date, final String username, final CommonEmail email) {
        this.id = id;
        this.folderKey = folderKey;
        this.date = date;
        this.username = username;
        this.email = email;
    }

    /**
     * Default Constructor Initializes object fields to null. folderKey and id
     * to -1.
     */
    public EmailBean() {
        this.email = null;
        this.date = null;
        this.date = null;
        this.folderKey = -1;
        this.id = -1;
    }

    /**
     * Gets the id of the EmailBean.
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the EmailBean.
     *
     * @param id
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Gets the folderid of the EmailBean.
     *
     * @return folderKey
     */
    public int getFolderKey() {
        return folderKey;
    }

    /**
     * Sets the folderid of the EmailBean.
     *
     * @param folderKey
     */
    public void setFolderKey(final int folderKey) {
        this.folderKey = folderKey;
    }

    /**
     * Gets the date of the EmailBean Based on the email type it is either, a
     * received date or a sent date.
     *
     * @return date retrieved.
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets the EmailBean date.
     *
     * @param date to set.
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Returns the emailBean's username.
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the emailBean's username.
     *
     * @param username
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Sets the email.
     *
     * @param email
     */
    public void setEmail(final CommonEmail email) {
        this.email = email;
    }
    
    @Override
    /**
     * Generates a hashcode for an emailBean object.
     */
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.id;
        hash = 29 * hash + this.folderKey;
        hash = 29 * hash + this.username.hashCode();
        hash = 29 * hash + this.date.hashCode();
        hash = 29 * hash + Objects.hashCode(this.email);
        return hash;
    }
    
    @Override
    /**
     * Compares two objects to check if they are equal. Two EmailBean objects
     * are only equal if they have the same id,the same folder, the same
     * username and their CommonEmail fields have the same content. This method
     * uses helper methods to compare the emails EmailBeans contain.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EmailBean other = (EmailBean) obj;
        try {
            return id == other.id && this.folderKey == other.folderKey
                    && username.equals(other.username)
                    && this.date.equals(other.date)
                    && (compareEmails(this.email, other.email));
        } catch (IOException ex) {
            Logger.getLogger(EmailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     *
     * Helper method to compare the EmailMessage objects contained inside two
     * Email objects.
     *
     * @param sentEmail
     * @param receivedEmail
     * @return boolean indicating whether the two email messages match.
     */
    private boolean compareEmailMessages(CommonEmail email, CommonEmail otherEmail) {
        if (email.messages().size() == otherEmail.messages().size()) {
            for (int i = 0; i < email.messages().size(); i++) {
                EmailMessage message = (EmailMessage) email.messages().get(i);
                EmailMessage otherMessage = (EmailMessage) otherEmail.messages().get(i);
                if (message != null && otherMessage != null) {
                    if (!message.getMimeType().
                            equals(otherMessage.getMimeType())) {
                        return false;
                    }
                    if (message.getContent() != null && otherMessage.getContent() != null) {
                        if (!message.getContent().
                                equals(otherMessage.getContent())) {
                            return false;
                        }
                    } else if (message.getContent() == null && otherMessage.getContent() != null) {
                        return false;
                    } else if (message.getContent() != null && otherMessage.getContent() == null) {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Helper method to compare the attachments of two CommonEmail objects.
     *
     * @param email
     * @param otherEmail
     * @return boolean indicating whether the email attachments match.
     * @throws IOException
     */
    private boolean compareFileAttachments(CommonEmail email, CommonEmail otherEmail) throws IOException {
        File receivedFile;
        File sentFile;
        for (int i = 0; i < otherEmail.attachments().size(); i++) {
            Object attachment = email.attachments().get(i);
            receivedFile = new File("temp", ((EmailAttachment) attachment).getName());
            receivedFile.createNewFile();
            ((EmailAttachment) attachment).writeToFile(receivedFile);
            Object otherAttachment = otherEmail.attachments().get(i);
            sentFile = new File("temp", ((EmailAttachment) otherAttachment).getName());
            sentFile.createNewFile();
            ((EmailAttachment) otherAttachment).writeToFile(sentFile);
            byte[] attachedFileContent = Files.readAllBytes(sentFile.toPath());
            byte[] receivedFileContent = Files.readAllBytes(receivedFile.toPath());
            if (!Arrays.equals(attachedFileContent, receivedFileContent)) {
                return false;
            }
        }
        return true;
        
    }

    /**
     * Helper method to compare the cc,bcc,to, from, subject and htmlText fields
     * of two Email objects. The two CommonEmail objects must be of the same
     * type (ReceivedEmail or Email)
     *
     * @param email
     * @param otherEmail
     * @return a boolean representing whether the 2 emails matched.
     */
    private boolean compareEmails(CommonEmail email, CommonEmail otherEmail) throws IOException {
        if (email.getClass() != otherEmail.getClass()) {
            return false;
        }
        return compareRecipients(email, otherEmail)
                && email.from().getEmail().equals(otherEmail.from().getEmail())
                && email.subject().equals(otherEmail.subject())
                && compareEmailMessages(email, otherEmail)
                && compareFileAttachments(email, otherEmail);
    }

    /**
     * Helper method to compare the recipients of two emails.
     *
     * @param email
     * @param otherEmail
     * @return boolean indicating whether all the recipients of both emails
     * match.
     */
    private boolean compareRecipients(CommonEmail email, CommonEmail otherEmail) {
        
        if (otherEmail instanceof ReceivedEmail && email instanceof ReceivedEmail) {
            otherEmail = ((ReceivedEmail) (otherEmail));
            email = ((ReceivedEmail) (email));
            return compareRecipient(email.to(), otherEmail.to()) && compareRecipient(email.cc(), otherEmail.cc());
        }
        if (otherEmail instanceof Email && email instanceof Email) {
            otherEmail = ((Email) (otherEmail));
            email = ((Email) (email));
            return compareRecipient(((Email) email).bcc(), (((Email) otherEmail).bcc()))
                    && compareRecipient(email.to(), otherEmail.to()) && compareRecipient(email.cc(), otherEmail.cc());
        }
        return false;
    }

    /**
     * Compares two arrays of emailaddresses for a specific recipient field.
     *
     * @param addresses
     * @param otherAddresses
     * @return boolean indicating whether the two array of recipient addresses
     * match.
     */
    private boolean compareRecipient(EmailAddress[] addresses, EmailAddress[] otherAddresses) {
        if (otherAddresses != null && addresses != null) {
            if (otherAddresses.length == addresses.length) {
                for (int i = 0; i < addresses.length; i++) {
                    if (!addresses[i].getEmail().equals(otherAddresses[i].getEmail())) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else if (addresses == null && otherAddresses != null) {
            return false;
        } else if (addresses != null && otherAddresses == null) {
            return false;
        }
        return true;
    }
    
}
