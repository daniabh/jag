package com.daniabh.emailclient.business;

import com.daniabh.emailclient.exception.InvalidBCCException;
import com.daniabh.emailclient.exception.InvalidCCException;
import com.daniabh.emailclient.exception.InvalidConfigAddressException;
import com.daniabh.emailclient.exception.InvalidTOException;
import com.daniabh.emailclient.properties.MailConfigBean;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.mail.Flags;
import jodd.mail.EmailFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * Mailer is a class to send and receive Emails. It has an Email object that is
 * set with the sendEmail method. It requires a MailConfigBean object to hold
 * the credentials of the sending email.
 *
 * @author Dania Bouhmidi
 */
public class Mailer {

    private final static Logger LOG = LoggerFactory.getLogger(Mailer.class);
    private Email email;
    private final MailConfigBean sendBean;

    /**
     * Constructor : sets the MailConfigBean required to hold the sending
     * account's credentials.
     *
     * @param sendBean
     */
    public Mailer(final MailConfigBean sendBean) {
        this.sendBean = sendBean;

    }

    /**
     * Sets the email object's recipients for the cc, bcc, and to fields. If all
     * of the recipient lists given as parameters are null, this method throws
     * an IllegalArgumentException.
     *
     * @param toField
     * @param ccField
     * @param bccField
     */
    private void setRecipients(final List<String> toField, final List<String> ccField, final List<String> bccField)
            throws InvalidConfigAddressException, InvalidCCException, InvalidBCCException, InvalidTOException {
        boolean toFieldIsValid = toField != null && !toField.isEmpty();
        boolean ccFieldIsValid = ccField != null && !ccField.isEmpty();
        boolean bccFieldIsValid = bccField != null && !bccField.isEmpty();
        //Check if at least one of the recipients field is not empty or null.
        if (toFieldIsValid || ccFieldIsValid || bccFieldIsValid) {
            if (toFieldIsValid) {
                setField(toField, "to");
            }
            if (ccFieldIsValid) {
                setField(ccField, "cc");
            }
            if (bccFieldIsValid) {
                setField(bccField, "bcc");
            }
        } else {
            throw new IllegalArgumentException("Please specify at least one recipient");
        }
    }

    /**
     * Sets the email object's recipients fields. The field type must be given
     * as an input. If one address in the recipients list is invalid, it throws
     * an Exception according to the fieldType.
     *
     * @param recipients (List containing the String email addresses to set)
     * @param fieldType (recipients field to set)
     */
    private void setField(List<String> recipients, String fieldType) throws InvalidConfigAddressException,
            InvalidCCException, InvalidBCCException, InvalidTOException {

        for (String emailAddress : recipients) {
            if (checkEmail(emailAddress)) {
                if (fieldType.equals("cc")) {
                    this.email.cc(emailAddress);
                }
                if (fieldType.equals("bcc")) {
                    this.email.bcc(emailAddress);
                }
                if (fieldType.equals("to")) {
                    this.email.to(emailAddress);
                }
            } else {
                if (fieldType.equals("cc")) {
                    throw new InvalidCCException(emailAddress);
                }
                if (fieldType.equals("bcc")) {
                    throw new InvalidBCCException(emailAddress);
                }
                if (fieldType.equals("to")) {
                    throw new InvalidTOException(emailAddress);
                }
            }
        }

    }

    /**
     * Sets the email's content. The email can contain a subject, attachments,
     * embedded attachments, html text and plain text messages. If any of the
     * parameters of this method are null, the email's content for that specific
     * parameter will not be set.
     *
     * @param subject
     * @param attachments (can be null)
     * @param embeddedAttachments (can be null)
     * @param htmlText (can be null)
     * @param textMessage
     * @throws IOException
     */
    private void setContent(String subject, List<File> attachments, List<File> embeddedAttachments,
            String htmlText, String textMessage) throws IOException {
        if (subject == null) {
            subject = "";
        }
        this.email.subject(subject);
        if (textMessage == null) {
            textMessage = "";
        }
        this.email.textMessage(textMessage);
        if (htmlText != null) {
            this.email.htmlMessage(htmlText);
        }
        setAttachments(attachments, embeddedAttachments);

    }

    /**
     * Checks that the files to be attached exist. If they do not exist, it
     * throws a FileNotFound exception. If they exist, it sets the email's
     * attachments.
     *
     * @param attachments
     * @param embeddedAttachments
     * @throws FileNotFoundException
     */
    private void setAttachments(List<File> attachments, List<File> embeddedAttachments) throws FileNotFoundException {
        if (attachments != null && !attachments.isEmpty()) {
            for (File emailAttachment : attachments) {
                if (validateAttachment(emailAttachment)) {
                    this.email.attachment(EmailAttachment.with().content(emailAttachment));
                } else {
                    throw new FileNotFoundException("File : " + emailAttachment.getName() + "to attach to the email was not found.");
                }
            }
        }

        if (embeddedAttachments != null && !embeddedAttachments.isEmpty()) {
            for (File embeddedAttachment : embeddedAttachments) {
                if (validateAttachment(embeddedAttachment)) {
                    this.email.embeddedAttachment(EmailAttachment.with().content(embeddedAttachment));
                } else {
                    throw new FileNotFoundException("File : " + embeddedAttachment.getName() + "to embed in the email was not found.");
                }
            }
        }
    }

    /**
     * Validates that the input File object exists.
     *
     * @param attachments
     * @return boolean indicating whether the file exists or not.
     */
    private boolean validateAttachment(File file) {
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    /**
     * Sends an email, the email can have regular attachments, embedded
     * attachments, a subject, a textMessage, html text , to,cc and bcc
     * recipients.If at least one list of recipients (CC,BCC,TO) is not empty or
     * null, the other lists can be null/empty. If sending address is invalid,
     * it throws an InvalidEmailAddressException.
     *
     * @param attachments (can be null)
     * @param ccField (can be null)
     * @param embeddedAttachments (can be null)
     * @param toField (can be null)
     * @param subject (can be null)
     * @param bccField (can be null)
     * @param textMessage (can be null)
     * @param htmlText (can be null)
     * @return Email object that is sent.
     * @throws java.io.IOException (if a file attachment from the attachments's
     * List or the embeddedAttachments' List was not found.)
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    public Email sendEmail(List<File> attachments, List<File> embeddedAttachments,
            final String subject, final String textMessage, final String htmlText, List<String> toField,
            List<String> ccField, List<String> bccField) throws IOException,
            InvalidConfigAddressException, InvalidCCException,
            InvalidBCCException, InvalidBCCException,
            InvalidBCCException, InvalidBCCException, InvalidTOException {
        if (sendBean != null) {
            if (checkEmail(sendBean.getUserEmailAddress())) {
                // Create am SMTP server object
                SmtpServer smtpServer = MailServer.create()
                        .ssl(true)
                        .host(sendBean.getSmtpUrl())
                        .auth(sendBean.getUserEmailAddress(), sendBean.getEmailPassword())
                        .buildSmtpMailServer();

                this.email = Email.create().from(sendBean.getUserEmailAddress());
                this.setRecipients(toField, ccField, bccField);

                this.setContent(subject, attachments, embeddedAttachments, htmlText, textMessage);
                try ( // A session is the object responsible for communicating with the server
                         SendMailSession session = smtpServer.createSession()) {
                    // Like a file we open the session, send the message and close the
                    // session
                    session.open();
                    session.sendMail(email);
                    LOG.info("Email sent");
                }
            } else {
                throw new InvalidConfigAddressException(
                        "Unable to send email because the send address : " + sendBean.getUserEmailAddress() + "is invalid");
            }
        }

        return this.email;
    }

    /**
     * Takes as input a MailConfigBean representing the email receiver.Uses an
     * ImapServer object to receive emails. Returns an array of emails
     * representing the unread emails of the receiver. If the emailReceiver
     * object contains an invalid email address, it throws an
     * InvalidEmailAddressException.
     *
     * @param emailReceiver
     * @return array of ReceivedEmail objects.
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     */
    public ReceivedEmail[] receiveEmail(MailConfigBean emailReceiver) throws InvalidConfigAddressException {
        ReceivedEmail[] emails = new ReceivedEmail[0];
        LOG.info("Current email address : " + emailReceiver.getUserEmailAddress());
        if (checkEmail(emailReceiver.getUserEmailAddress())) {
            ImapServer imapServer = MailServer.create()
                    .host(emailReceiver.getImapUrl())
                    .ssl(true)
                    .auth(emailReceiver.getUserEmailAddress(), emailReceiver.getEmailPassword())
                    .buildImapMailServer();

            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                LOG.info("Message count: " + session.getMessageCount());
                emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                if (emails == null) {
                    emails = new ReceivedEmail[0];
                }
            }
        } else {
            throw new InvalidConfigAddressException(
                    "Unable to receive email, because the receive address : " + emailReceiver.getUserEmailAddress() + "is invalid");
        }
        return emails;
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

}
