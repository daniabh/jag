package com.daniabh.emailclient.fakedata;

import com.daniabh.emailclient.javafxbean.EmailData;
import com.daniabh.emailclient.javafxbean.FolderFXBean;
import com.daniabh.emailclient.javafxbean.FormData;
import com.daniabh.emailclient.javafxbean.MessageBean;
import java.time.LocalDateTime;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * Fake Data generates JavaFx Beans to be displayed on all the parts of the user
 * interface. Generates fake folder data, fake email data, fake message data and
 * fake form data.
 *
 * @author Dania Bouhmidi
 */
public class FakeData {

    /**
     * Generates an ObservableList of FolderFXBeans to display on the UI.
     *
     * @return folders
     */
    public ObservableList<FolderFXBean> findFolders() {
        ObservableList<FolderFXBean> folders = FXCollections.observableArrayList();
        String[] folderNames = {"SENT", "INBOX", "DRAFT"};
        for (int i = 0; i < folderNames.length; i++) {
            FolderFXBean folder = new FolderFXBean(i, folderNames[i]);
            folders.add(folder);
        }
        return folders;
    }

    /**
     * Generates an ObservableList of EmailFXBeans to display on the UI.
     *
     * @return ObservableList of EmailFXBeans
     */
    public ObservableList<EmailData> findEmails() {
        ObservableList<EmailData> emails = FXCollections.observableArrayList();
        EmailData emailBean = new EmailData(1, "moonchild.zyx@gmail.com", "Test Subject", LocalDateTime.now());
        EmailData secondBean = new EmailData(2, "haku.xyz@gmail.com", "Test Subject 2", LocalDateTime.now());
        EmailData thirdBean = new EmailData(2, "blue.side.xyzy@gmail.com", "Everything goes", LocalDateTime.now());
        EmailData fourthBean = new EmailData(2, "haku.xyz@gmail.com", "Fourth email", LocalDateTime.now());
        EmailData fifthBean = new EmailData(2, "moonchild.zyx@gmail.com", "Fifth email", LocalDateTime.now());
        emails.addAll(emailBean, secondBean, thirdBean, fourthBean, fifthBean);
        return emails;
    }

    /**
     * Generates an ObservableList of FormData to display on the UI.
     *
     * @return forms
     */
    public ObservableList<FormData> findForms() {
        ObservableList<FormData> forms = FXCollections.observableArrayList();
        FormData firstForm = new FormData();
        FormData secondForm = new FormData();
        firstForm.setCc("haku.xyz@gmail.com");
        firstForm.setBcc("moonchild.zyx@gmail.com");
        firstForm.setTo("blue.side.xyzy@gmail.com");
        firstForm.setSubject("Test subject");

        secondForm.setCc("moonchild.zyx@gmail.com");
        secondForm.setBcc("blue@gmail.com");
        secondForm.setTo("swan@gmail.com");
        secondForm.setSubject("Another test subject");
        forms.addAll(firstForm, secondForm);
        return forms;
    }

    /**
     * Generates an ObservableList of MessageBean to display on the UI.
     *
     * @return messages
     */
    public ObservableList<MessageBean> findMessages() {
        ObservableList<MessageBean> messages = FXCollections.observableArrayList();
        MessageBean firstMessage = new MessageBean("First message");
        MessageBean secondMessage = new MessageBean("Second message");
        MessageBean thirdMessage = new MessageBean("Third message");
        MessageBean fourthMessage = new MessageBean("Fourth message");
        MessageBean fifthMessage = new MessageBean("Fifth message");
        messages.addAll(firstMessage, secondMessage, thirdMessage, fourthMessage, fifthMessage);
        return messages;
    }

}
