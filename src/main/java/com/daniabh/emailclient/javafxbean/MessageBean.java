package com.daniabh.emailclient.javafxbean;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX Bean that will be binded to the HTMLEditor. Represents an email's
 * mesage.
 *
 * @author Dania Bouhmidi
 */
public class MessageBean implements Serializable {

    private StringProperty textMessage;
    private StringProperty htmlMessage;

    /**
     * Default constructor
     */
    public MessageBean() {
        this.textMessage = new SimpleStringProperty();
        this.htmlMessage = new SimpleStringProperty();
    }

    /**
     * Non-default constructor
     *
     * @param textMessage
     * @param htmlMessage
     */
    public MessageBean(final String textMessage, final String htmlMessage) {
        this();
        this.textMessage.set(textMessage);
        this.htmlMessage.set(htmlMessage);

    }

    /**
     * Non-default constructor
     *
     * @param textMessage
     */
    public MessageBean(final String textMessage) {
        this();
        this.textMessage.set(textMessage);

    }

    /**
     * Gets the message String
     *
     * @return message String
     */
    public String getTextMessage() {
        return textMessage.get();
    }

    /**
     * Gets the message StringProperty
     *
     * @return message StringProperty
     */
    public StringProperty getMessageProperty() {
        return textMessage;
    }

    /**
     * Sets the textMessage string
     *
     * @param message
     */
    public void setTextMessage(final String message) {
        this.textMessage.set(message);
    }

    /**
     * Returns the html message property
     *
     * @return htmlMessage
     */
    public StringProperty getHtmlMessageProperty() {
        return htmlMessage;
    }

    /**
     * Returns the html message String
     *
     * @return htmlMessage string
     */
    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    /**
     * Sets the html message
     *
     * @param htmlMessage
     */
    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }

    @Override
    /**
     * Generates a hashCode.
     *
     * @return hashCode
     */
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.textMessage.get());
        hash = 97 * hash + Objects.hashCode(this.htmlMessage.get());
        return hash;
    }

    @Override
    /**
     * Returns a boolean indicating whether this object is equal to the input
     * object.
     *
     * @param Object obj
     * @return boolean indicating whether this object is equal to the input
     * object.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageBean other = (MessageBean) obj;
        if (!Objects.equals(this.textMessage.get(), other.textMessage.get())) {
            return false;
        }
        return Objects.equals(this.htmlMessage.get(), other.htmlMessage.get());
    }
}
