package com.daniabh.emailclient.javafxbean;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX Bean for an email's form. Contains the cc,bcc,to lists and the subject
 * for an email.
 *
 * @author Dania Bouhmidi
 */
public class FormData implements Serializable {

    private StringProperty to;
    private StringProperty cc;
    private StringProperty bcc;
    private StringProperty subject;

    /**
     * Default constructor : initializes the fields.
     */
    public FormData() {
        this.to = new SimpleStringProperty();
        this.cc = new SimpleStringProperty();
        this.bcc = new SimpleStringProperty();
        this.subject = new SimpleStringProperty();
    }

    /**
     * Non- default constructor : sets all the fields.
     *
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     */
    public FormData(final String to, final String cc, final String bcc, final String subject) {
        this();
        this.to.set(to);
        this.cc.set(cc);
        this.bcc.set(bcc);
        this.subject.set(subject);
    }

    /**
     * Returns the to field.
     *
     * @return to field
     */
    public String getTo() {
        return to.get();
    }

    /**
     * Sets the to property
     *
     * @param to
     */
    public void setTo(final String to) {
        this.to.set(to);
    }

    /**
     * Returns the to property
     *
     * @return to property
     */
    public StringProperty getToProperty() {
        return to;
    }

    /**
     * Returns the cc field.
     *
     * @return cc field
     */
    public String getCc() {
        return cc.get();
    }

    /**
     * Sets the cc property
     *
     * @param cc
     */
    public void setCc(final String cc) {
        this.cc.set(cc);
    }

    /**
     * Returns the cc property
     *
     * @return cc property
     */
    public StringProperty getCcProperty() {
        return cc;
    }

    /**
     * Returns the bcc field.
     *
     * @return bcc field
     */
    public String getBcc() {
        return bcc.get();
    }

    /**
     * Sets the cc property
     *
     * @param bcc
     */
    public void setBcc(final String bcc) {
        this.bcc.set(bcc);
    }

    /**
     * Returns the bcc property
     *
     * @return bcc property
     */
    public StringProperty getBccProperty() {
        return bcc;
    }

    /**
     * Returns the subject
     *
     * @return subject
     */
    public String getSubject() {
        return subject.get();
    }

    /**
     * Returns the subject
     *
     * @return subject property
     */
    public StringProperty getSubjectProperty() {
        return subject;
    }

    /**
     * Sets the subject
     *
     * @param subject
     */
    public void setSubject(final String subject) {
        this.subject.set(subject);
    }

    @Override
    /**
     * Generates a hashcode for the object.
     *
     * @return int hashCode
     */
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.to.get());
        hash = 53 * hash + Objects.hashCode(this.cc.get());
        hash = 53 * hash + Objects.hashCode(this.bcc.get());
        hash = 53 * hash + Objects.hashCode(this.subject.get());
        return hash;
    }

    @Override
    /**
     * Returns a boolean indicating whether the input object is equal to this
     * object.
     *
     * @param Object
     * @return boolean indicating whether the input object is equal to this
     * object.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormData other = (FormData) obj;
        if (!Objects.equals(this.to.get(), other.to.get())) {
            return false;
        }
        if (!Objects.equals(this.cc.get(), other.cc.get())) {
            return false;
        }
        if (!Objects.equals(this.bcc.get(), other.bcc.get())) {
            return false;
        }
        if (!Objects.equals(this.subject.get(), other.subject.get())) {
            return false;
        }
        return true;
    }

}
