/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniabh.emailclient.javafxbean;

import java.io.Serializable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX Bean that contains data for a folder, such as its id and folder name.
 *
 * @author Dania Bouhmidi
 */
public class FolderFXBean implements Serializable {

    private final IntegerProperty id;
    private final StringProperty folder;

    /**
     * Default constructor Initializes the properties.
     */
    public FolderFXBean() {
        this.id = new SimpleIntegerProperty();
        this.folder = new SimpleStringProperty();
    }

    /**
     * Constructor : Sets the id and folder of the JavaFX Bean.
     *
     * @param id
     * @param folder
     */
    public FolderFXBean(final int id, final String folder) {
        this();
        this.id.set(id);
        this.folder.set(folder);
    }

    /**
     * Returns the IntegerProperty id.
     *
     * @return id
     */
    public IntegerProperty getIdProperty() {
        return id;
    }

    /**
     * Returns the id.
     *
     * @return int id
     */
    public int getId() {
        return id.get();
    }

    /**
     * Sets the id in the IntegerProperty
     *
     * @param id
     */
    public void setId(final int id) {
        this.id.set(id);
    }

    /**
     * Returns the folder StringProperty.
     *
     * @return folder
     */
    public StringProperty getFolderProperty() {
        return folder;
    }

    /**
     * Returns the folder String.
     *
     * @return folder String
     */
    public String getFolder() {
        return folder.get();
    }

    /**
     * Sets the folder of the StringProperty.
     *
     * @param folder
     */
    public void setFolder(final String folder) {
        this.folder.set(folder);
    }

    @Override
    /**
     * Generates a hashCode for a FolderFXBean object.
     *
     * @return hashCode
     */
    public int hashCode() {
        return this.id.get();
    }

    @Override
    /**
     * Returns a boolean indicating whether two FolderFXBean objects are equal.
     *
     * @return boolean indicating whether two FolderFXBean objects are equal.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FolderFXBean other = (FolderFXBean) obj;
        if (this.id.get() != other.id.get()) {
            return false;
        }

        return true;
    }

    @Override
    /**
     * Returns the FolderFXBean's information in a string.
     *
     * @return String
     */
    public String toString() {
        return "FolderFXBean{" + "id=" + id.get() + ", folder=" + folder.get() + '}';
    }
}
