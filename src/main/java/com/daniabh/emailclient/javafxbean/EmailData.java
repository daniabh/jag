package com.daniabh.emailclient.javafxbean;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX Bean for an EmailBean Contains an email's id, from field, subject
 * field and date.
 *
 * @author Dania Bouhmidi
 */
public class EmailData implements Serializable {

    private final IntegerProperty id;
    private final StringProperty from;
    private final StringProperty subject;

    private ObjectProperty<LocalDateTime> date = new SimpleObjectProperty<>();

    /**
     * Default constructor
     */
    public EmailData() {
        this.id = new SimpleIntegerProperty();
        this.from = new SimpleStringProperty();
        this.subject = new SimpleStringProperty();
        this.date = new SimpleObjectProperty<>();
    }

    /**
     * Constructor : Sets the id, from subject and date properties of the JavaFX
     * bean.
     *
     * @param id
     * @param from
     * @param subject
     * @param date
     */
    public EmailData(final int id, final String from, final String subject, final LocalDateTime date) {
        this.id = new SimpleIntegerProperty(id);
        this.from = new SimpleStringProperty(from);
        this.subject = new SimpleStringProperty(subject);
        this.date = new SimpleObjectProperty<>(date);
    }

    /**
     * Returns the id IntegerProperty
     *
     * @return id
     */
    public IntegerProperty getIdProperty() {
        return id;
    }

    /**
     * Returns the id int
     *
     * @return id
     */
    public int getId() {
        return id.get();
    }

    /**
     * Sets the id in the IntegerProperty
     *
     * @param id
     */
    public void setId(final int id) {
        this.id.set(id);
    }

    /**
     * Sets the from StringProperty
     *
     * @return from
     */
    public StringProperty getFromProperty() {
        return from;
    }

    /**
     * Returns the from String.
     *
     * @return from
     */
    public String getFrom() {
        return from.get();
    }

    /**
     * Sets the from string.
     *
     * @param from
     */
    public void setFrom(final String from) {
        this.from.set(from);
    }

    /**
     * Returns the subject StringProperty
     *
     * @return subject
     */
    public StringProperty getSubjectProperty() {
        return subject;
    }

    /**
     * Returns the subject String
     *
     * @return subject
     */
    public String getSubject() {
        return subject.get();
    }

    /**
     * Sets the subject in the StringProperty
     *
     * @param subject
     */
    public void setSubject(final String subject) {
        this.subject.set(subject);
    }

    /**
     * Returnds the date ObjectProperty.
     *
     * @return date
     */
    public ObjectProperty<LocalDateTime> getDateProperty() {
        return date;
    }

    /**
     * Sets the date in the ObjectProperty date.
     *
     * @param date
     */
    public void setDate(final LocalDateTime date) {
        this.date.set(date);
    }

    @Override
    /**
     * Returns the information of an EmailData object in the form of a string.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmaiData{id=").append(id.get());
        sb.append(", from=").append(from.get());
        sb.append(", subject=").append(subject.get());
        sb.append(", date=").append(date.get());
        sb.append('}');
        return sb.toString();
    }

    @Override
    /**
     * Generates a hashCode for an EmailData object
     */
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id.get());
        hash = 83 * hash + Objects.hashCode(this.from.get());
        hash = 83 * hash + Objects.hashCode(this.subject.get());
        hash = 83 * hash + Objects.hashCode(this.date.get());
        return hash;
    }

    @Override
    /**
     * Returns a boolean indicating whether an EmailData object is equal to
     * another EmailData object.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailData other = (EmailData) obj;
        if (!Objects.equals(this.id.get(), other.id.get())) {
            return false;
        }
        if (!Objects.equals(this.from.get(), other.from.get())) {
            return false;
        }
        if (!Objects.equals(this.subject.get(), other.subject.get())) {
            return false;
        }
        if (!Objects.equals(this.date.get(), other.date.get())) {
            return false;
        }
        return true;
    }

}
