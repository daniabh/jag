package com.daniabh.emailclient.javafxbean;

import java.time.LocalDateTime;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import jodd.mail.CommonEmail;

/**
 * JavaFX Bean that acts as a translation for the EmailBean.
 *
 * @author Dania Bouhmidi
 */
public class EmailFXBean {

    private IntegerProperty id;
    private IntegerProperty folderKey;
    private ObjectProperty<LocalDateTime> date;
    private StringProperty username;

    private ObjectProperty<CommonEmail> email;

    /**
     * Default constructor
     */
    public EmailFXBean() {
        this.id = new SimpleIntegerProperty();
        this.folderKey = new SimpleIntegerProperty();
        this.date = new SimpleObjectProperty<>();
        this.username = new SimpleStringProperty();
        this.email = new SimpleObjectProperty<>();
    }

    /**
     * Non-default constructor
     *
     * @param id
     * @param folderKey
     * @param date
     * @param username
     * @param email
     */
    public EmailFXBean(int id, int folderKey, LocalDateTime date, String username, CommonEmail email) {
        this();
        this.id.set(id);
        this.folderKey.set(folderKey);
        this.date.set(date);
        this.username.set(username);
        this.email.set(email);
    }

    /**
     * Returns the id property
     *
     * @return id property
     */
    public IntegerProperty getIdProperty() {
        return id;
    }

    /**
     * Returns the int id
     *
     * @return int id
     */
    public int getId() {
        return id.get();
    }

    /**
     * Sets the id property
     *
     * @param id
     */
    public void setId(final int id) {
        this.id.set(id);
    }

    /**
     * Gets the folderKey property
     *
     * @return folderKey
     */
    public IntegerProperty getFolderKeyProperty() {
        return folderKey;
    }

    /**
     * Gets the folderKey int
     *
     * @return int folderKey
     */
    public int getFolderKey() {
        return folderKey.get();
    }

    /**
     * Sets the folderKey
     *
     * @param folderKey
     */
    public void setFolderKey(final int folderKey) {
        this.folderKey.set(folderKey);
    }

    /**
     * Returns the date property
     *
     * @return date property
     */
    public ObjectProperty<LocalDateTime> getDateProperty() {
        return date;
    }

    /**
     * Returns the LocalDateTime date
     *
     * @return LocalDateTime date
     */
    public LocalDateTime getDate() {
        return date.get();
    }

    /**
     * Sets the date property
     *
     * @param date
     */
    public void setDate(final LocalDateTime date) {
        this.date.set(date);
    }

    /**
     * Gets the username property
     *
     * @return username property
     */
    public StringProperty getUsernameProperty() {
        return username;
    }

    /**
     * Gets the username string
     *
     * @return username String
     */
    public String getUsername() {
        return username.get();
    }

    /**
     * Sets the username String
     *
     * @param username
     */
    public void setUsername(final String username) {
        this.username.set(username);
    }

    /**
     * Gets the CommonEmail Object Property
     *
     * @return ObjectProperty<CommonEmail>
     */
    public ObjectProperty<CommonEmail> getEmailProperty() {
        return email;
    }

    /**
     * Gets the CommonEmail Object
     *
     * @return
     */
    public CommonEmail getEmail() {
        return email.get();
    }

    /**
     * Sets the CommonEmail Object Property
     *
     * @param email
     */
    public void setEmail(final CommonEmail email) {
        this.email.set(email);
    }

    @Override
    /**
     * Generates a hashCode
     */
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id.get());
        hash = 17 * hash + Objects.hashCode(this.folderKey.get());
        hash = 17 * hash + Objects.hashCode(this.date.get());
        hash = 17 * hash + Objects.hashCode(this.username.get());
        hash = 17 * hash + Objects.hashCode(this.email.get());
        return hash;
    }

    @Override
    /**
     * Compares two EmailFXBean objects and returns a boolean indicating whether
     * the two objects are equal
     *
     * @param obj
     * @return boolean
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailFXBean other = (EmailFXBean) obj;
        if (!Objects.equals(this.id.get(), other.id.get())) {
            return false;
        }
        if (!Objects.equals(this.folderKey.get(), other.folderKey.get())) {
            return false;
        }
        if (!Objects.equals(this.date.get(), other.date.get())) {
            return false;
        }
        if (!Objects.equals(this.username.get(), other.username.get())) {
            return false;
        }
        return Objects.equals(this.email.get(), other.email.get());
    }

}
