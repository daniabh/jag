package com.daniabh.emailclient.javafxbean;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * JavaFX Bean to hold the information of mail configuration properties.
 *
 * @author Dania Bouhmidi
 */
public class PropertyBean implements Serializable {

    private StringProperty userName;
    private StringProperty emailAddress;
    private StringProperty mailPassword;
    private StringProperty imapURL;
    private StringProperty smtpURL;
    private StringProperty imapPort;
    private StringProperty smtpPort;
    private StringProperty mysqlURL;
    private StringProperty mysqlDatabase;
    private StringProperty mysqlPort;
    private StringProperty mysqlUser;
    private StringProperty mysqlPassword;

    /**
     * Non-default constructor : initializes all the fields.
     *
     * @param userName
     * @param emailAddress
     * @param mailPassword
     * @param imapURL
     * @param smtpURL
     * @param imapPort
     * @param smtpPort
     * @param mysqlURL
     * @param mysqlDatabase
     * @param mysqlPort
     * @param mysqlUser
     * @param mysqlPassword
     */
    public PropertyBean(final String userName, final String emailAddress, final String mailPassword,
            final String imapURL, final String smtpURL, final String imapPort,
            final String smtpPort, final String mysqlURL, final String mysqlDatabase,
            final String mysqlPort, final String mysqlUser, final String mysqlPassword) {
        this.userName = new SimpleStringProperty(userName);
        this.emailAddress = new SimpleStringProperty(emailAddress);
        this.mailPassword = new SimpleStringProperty(mailPassword);
        this.imapURL = new SimpleStringProperty(imapURL);
        this.smtpURL = new SimpleStringProperty(smtpURL);
        this.imapPort = new SimpleStringProperty(imapPort);
        this.smtpPort = new SimpleStringProperty(smtpPort);
        this.mysqlURL = new SimpleStringProperty(mysqlURL);
        this.mysqlDatabase = new SimpleStringProperty(mysqlDatabase);
        this.mysqlPort = new SimpleStringProperty(mysqlPort);
        this.mysqlUser = new SimpleStringProperty(mysqlUser);
        this.mysqlPassword = new SimpleStringProperty(mysqlPassword);
    }

    /**
     * Default constructor : initializes all fields to empty strings.
     */
    public PropertyBean() {
        this("", "", "", "", "", "", "", "", "", "", "", "");
    }

    /**
     * Gets the username String
     *
     * @return username String
     */
    public String getUserName() {
        return userName.get();
    }

    /**
     * Sets the username property
     *
     * @param userName
     */
    public void setUserName(final String userName) {
        this.userName.set(userName);
    }

    /**
     * Gets the username property
     *
     * @return username property
     */
    public StringProperty userNameProperty() {
        return userName;
    }

    /**
     * Gets the emailAddress String
     *
     * @return emailAddress String
     */
    public String getEmailAddress() {
        return emailAddress.get();
    }

    /**
     * Sets the emailAddress property
     *
     * @param emailAddress
     */
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress.set(emailAddress);
    }

    /**
     * Gets the emailAddress Property
     *
     * @return emailAddress Property
     */
    public StringProperty emailAddressProperty() {
        return emailAddress;
    }

    /**
     * Gets the mail password String
     *
     * @return mailPassword String
     */
    public String getMailPassword() {
        return mailPassword.get();
    }

    /**
     * Sets the mailPassword property
     *
     * @param mailPassword
     */
    public void setMailPassword(final String mailPassword) {
        this.mailPassword.set(mailPassword);
    }

    /**
     * Gets the mailPasswordProperty
     *
     * @return mailPasswordProperty
     */
    public StringProperty mailPasswordProperty() {
        return mailPassword;
    }

    /**
     * Gets the imap url String
     *
     * @return imap url String
     */
    public String getImapURL() {
        return imapURL.get();
    }

    /**
     * Sets the imapUrl
     *
     * @param imapURL
     */
    public void setImapURL(final String imapURL) {
        this.imapURL.set(imapURL);
    }

    /**
     * Gets the imapURLProperty
     *
     * @return imapURLProperty
     */
    public StringProperty imapURLProperty() {
        return imapURL;
    }

    /**
     * Gets the smtp url.
     *
     * @return SmtpURL String
     */
    public String getSmtpURL() {
        return smtpURL.get();
    }

    /**
     * Sets the smtpUrl
     *
     * @param smtpURL
     */
    public void setSmtpURL(final String smtpURL) {
        this.smtpURL.set(smtpURL);
    }

    /**
     * Gets the smtpURLProperty
     *
     * @return smtpURLProperty
     */
    public StringProperty smtpURLProperty() {
        return smtpURL;
    }

    /**
     * Gets the imap Port String
     *
     * @return imap Port String
     */
    public String getImapPort() {
        return imapPort.get();
    }

    /**
     * Sets the imapPort
     *
     * @param imapPort
     */
    public void setImapPort(final String imapPort) {
        this.imapPort.set(imapPort);
    }

    /**
     * Gets the imapPortProperty
     *
     * @return imapPortProperty
     */
    public StringProperty imapPortProperty() {
        return imapPort;
    }

    /**
     * Gets the smtp port String
     *
     * @return smpt port String
     */
    public String getSmtpPort() {
        return smtpPort.get();
    }

    /**
     * Set the smtpPort
     *
     * @param smtpPort
     */
    public void setSmtpPort(final String smtpPort) {
        this.smtpPort.set(smtpPort);
    }

    /**
     * Gets the smtpPort property
     *
     * @return smtpPort property
     */
    public StringProperty smtpPortProperty() {
        return smtpPort;
    }

    /**
     * Gets the mysqlUrl String
     *
     * @return mysqlUrl String
     */
    public String getMysqlURL() {
        return mysqlURL.get();
    }

    /**
     * Sets mysqlUrl
     *
     * @param mysqlURL
     */
    public void setMysqlURL(final String mysqlURL) {
        this.mysqlURL.set(mysqlURL);
    }

    /**
     * Gets the mysqlURLProperty
     *
     * @return mysqlURLProperty
     */
    public StringProperty mysqlURLProperty() {
        return mysqlURL;
    }

    /**
     * Gets mysqlDatabase
     *
     * @return mysqlDatabase
     */
    public String getMysqlDatabase() {
        return mysqlDatabase.get();
    }

    /**
     * Sets the MysqlDatabase field
     *
     * @param mysqlDatabase
     */
    public void setMysqlDatabase(final String mysqlDatabase) {
        this.mysqlDatabase.set(mysqlDatabase);
    }

    /**
     *
     * @return mysqlDatabaseProperty
     */
    public StringProperty mysqlDatabaseProperty() {
        return mysqlDatabase;
    }

    /**
     * Gets the MysqlPort field
     *
     * @return MysqlPort
     */
    public String getMysqlPort() {
        return mysqlPort.get();
    }

    /**
     * Sets the mysqlPort property
     *
     * @param mysqlPort
     */
    public void setMysqlPort(final String mysqlPort) {
        this.mysqlPort.set(mysqlPort);
    }

    /**
     * Gets the mysqlPort Property
     *
     * @return mysqlPortProperty
     */
    public StringProperty mysqlPortProperty() {
        return mysqlPort;
    }

    /**
     * Gets the mysql user String
     *
     * @return mysql user String
     */
    public String getMysqlUser() {
        return mysqlUser.get();
    }

    /**
     * Sets the mysqlUser property
     *
     * @param mysqlUser
     */
    public void setMysqlUser(final String mysqlUser) {
        this.mysqlUser.set(mysqlUser);
    }

    /**
     * Gets the mysqlUser property
     *
     * @return mysqlUser property
     */
    public StringProperty mysqlUserProperty() {
        return mysqlUser;
    }

    /**
     * Gets the mysql password String
     *
     * @return mysql password String
     */
    public String getMysqlPassword() {
        return mysqlPassword.get();
    }

    /**
     * Sets the myslPassword property
     *
     * @param mysqlPassword
     */
    public void setMysqlPassword(final String mysqlPassword) {
        this.mysqlPassword.set(mysqlPassword);
    }

    /**
     * Gets the mysqlPassword Property
     *
     * @return mysqlPassword Property
     */
    public StringProperty mysqlPasswordProperty() {
        return mysqlPassword;
    }

    @Override
    /**
     * Generates a hashCode.
     *
     * @return hashCode
     */
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.userName.get());
        hash = 29 * hash + Objects.hashCode(this.emailAddress.get());
        hash = 29 * hash + Objects.hashCode(this.mailPassword.get());
        hash = 29 * hash + Objects.hashCode(this.imapURL.get());
        hash = 29 * hash + Objects.hashCode(this.smtpURL.get());
        hash = 29 * hash + Objects.hashCode(this.imapPort.get());
        hash = 29 * hash + Objects.hashCode(this.smtpPort.get());
        hash = 29 * hash + Objects.hashCode(this.mysqlURL.get());
        hash = 29 * hash + Objects.hashCode(this.mysqlDatabase.get());
        hash = 29 * hash + Objects.hashCode(this.mysqlPort.get());
        hash = 29 * hash + Objects.hashCode(this.mysqlUser.get());
        hash = 29 * hash + Objects.hashCode(this.mysqlPassword.get());
        return hash;
    }

    @Override
    /**
     * Returns a boolean indicating whether this object is equal to the input
     * object.
     *
     * @param Object obj
     * @return boolean indicating whether this object is equal to the input
     * object.
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyBean other = (PropertyBean) obj;
        if (!Objects.equals(this.userName.get(), other.userName.get())) {
            return false;
        }
        if (!Objects.equals(this.emailAddress.get(), other.emailAddress.get())) {
            return false;
        }
        if (!Objects.equals(this.mailPassword.get(), other.mailPassword.get())) {
            return false;
        }
        if (!Objects.equals(this.imapURL.get(), other.imapURL.get())) {
            return false;
        }
        if (!Objects.equals(this.smtpURL.get(), other.smtpURL.get())) {
            return false;
        }
        if (!Objects.equals(this.imapPort.get(), other.imapPort.get())) {
            return false;
        }
        if (!Objects.equals(this.smtpPort.get(), other.smtpPort.get())) {
            return false;
        }
        if (!Objects.equals(this.mysqlURL.get(), other.mysqlURL.get())) {
            return false;
        }
        if (!Objects.equals(this.mysqlDatabase.get(), other.mysqlDatabase.get())) {
            return false;
        }
        if (!Objects.equals(this.mysqlPort.get(), other.mysqlPort.get())) {
            return false;
        }
        if (!Objects.equals(this.mysqlUser.get(), other.mysqlUser.get())) {
            return false;
        }
        return Objects.equals(this.mysqlPassword.get(), other.mysqlPassword.get());
    }

    @Override
    /**
     * Returns the propertyBean's information as a String.
     *
     * @return String information
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PropertyBean{userName=").append(userName.get());
        sb.append(", emailAddress=").append(emailAddress.get());
        sb.append(", mailPassword=").append(mailPassword.get());
        sb.append(", imapURL=").append(imapURL.get());
        sb.append(", smtpURL=").append(smtpURL.get());
        sb.append(", imapPort=").append(imapPort.get());
        sb.append(", smtpPort=").append(smtpPort.get());
        sb.append(", mysqlURL=").append(mysqlURL.get());
        sb.append(", mysqlDatabase=").append(mysqlDatabase.get());
        sb.append(", mysqlPort=").append(mysqlPort.get());
        sb.append(", mysqlUser=").append(mysqlUser.get());
        sb.append(", mysqlPassword=").append(mysqlPassword.get());
        sb.append('}');
        return sb.toString();
    }
}
