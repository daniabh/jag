package com.daniabh.emailclient.persistence;

import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.exception.EmailLimitExceededException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for the CRUD operations related to Emails.
 *
 * @author Dania Bouhmidi
 */
public interface EmailDAO {

    //Create
    //Creates records for an email in the mail database.
    //Handles it appropriately according to its type(ReceivedEmail or Email).
    public int create(EmailBean emailBean) throws SQLException, IOException,EmailLimitExceededException;

    //Read
    //Find all emails in the mail database and returns them as a list of EmailBeans.
    public List<EmailBean> findAll() throws SQLException, IOException;

    //Finds email with a specific id and returns it as an EmailBean.
    public EmailBean findID(int id) throws SQLException, IOException;

    // Find emails with a specific subject field and returns them as a list of EmailBeans.
    public List<EmailBean> findSubject(String subject) throws SQLException, IOException;

    // Finds emails from a specific email address and returns them as a list of EmailBeans.
    public List<EmailBean> findFrom(String from) throws SQLException, IOException;

    // Find emails with a specific email address in a specific recipientField and returns them as a list of EmailBeans.
    public List<EmailBean> findWith(String address, String recipientField) throws SQLException, IOException;

    // Update
    // Updates an email in the database with the given EmailBean's email content
    // The emailBean must have an id that exists in the mail database.
    // Returns 1 if emailBean was updated, otherwise, 0.
    public int update(EmailBean emailBean) throws SQLException, IOException,EmailLimitExceededException;
    
    //Updates an email's folder with the newFolder input.
    public void updateFolder (String newFolder, EmailBean emailBean) throws SQLException, IOException;
    //Delete
    
    //Deletes an email in the mail database with the given id.
    // Returns the number of records deleted.
    public int delete(int id) throws SQLException, IOException;
    
    //Deletes a folder and all of its content.
    public void removeFolder(String folder) throws SQLException, IOException;

}
