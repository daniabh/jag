package com.daniabh.emailclient.persistence;

import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.exception.EmailLimitExceededException;
import com.daniabh.emailclient.properties.MailConfigBean;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataSource;
import jodd.mail.CommonEmail;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the EmailDAO interface. It represents a data access
 * object for an Email. It has methods to retrieve an email from the mail
 * database based on different criterias such as a from email address, a cc,to,
 * or bcc email address(if we are retrieveing a sent email). It has methods to
 * create emails, those emails can be of any type, ReceivedEmail or Email.
 * EmailDAOImpl also handles updates on emails, such as updates on an email's
 * folder or content.
 *
 * @author Dania Bouhmidi
 */
public class EmailDAOImpl implements EmailDAO {

    private MailConfigBean configBean;
    private String sqlUrl;
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOImpl.class);

    /**
     * Constructor : sets the configBean and the sql url.
     *
     * @param configBean
     */
    public EmailDAOImpl(MailConfigBean configBean) {
        this.configBean = configBean;

        sqlUrl = "jdbc:mysql://"
                + configBean.getDatabaseURL()
                + ":"
                + configBean.getDatabasePortNumber()
                + "/"
                + configBean.getDatabaseName()
                + "?characterEncoding=UTF-8&autoReconnect=true&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&allowPublicKeyRetrieval=true";
    }

    /**
     * Helper method to get a connection.
     *
     * @return a Connection object
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        String username = this.configBean.getDatabaseUserName();
        String password = this.configBean.getDatabasePassword();
        Connection connection = DriverManager.getConnection(sqlUrl, username, password);
        return connection;
    }

    /**
     * Retrieves all the records in the mail database and returns the data as an
     * arraylist of EmailBean objects
     *
     * @return The arraylist of EmailBean objects found.
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    @Override
    public List<EmailBean> findAll() throws SQLException, IOException {
        List<EmailBean> rows = new ArrayList<>();
        String selectQuery = "SELECT EMAILID,SUBJECT,TEXT,HTMLTEXT,"
                + "USERNAME,FROMEMAILADDRESS,DATE,FOLDERID FROM EMAIL";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery);) {
            rows.addAll(find(selectStatement));
        }
        LOG.info("# of records found : " + rows.size());
        return rows;
    }

    /**
     * Deletes all the records associated with a specific email id. That email
     * id is the input parameter to the method.
     *
     * @param id The primary key to use to identify the email that must be
     * deleted
     * @return the number of records deleted.
     * @throws SQLException
     * @throws java.io.IOException
     */
    @Override
    public int delete(int id) throws SQLException, IOException {
        int result = 0;
        if (findID(id) != null) {
            String[] deleteQueries = {"DELETE FROM ATTACHMENTS WHERE EMAILID = ?",
                "DELETE FROM EMAILTOADDRESS WHERE EMAILID = ?",
                "DELETE FROM EMAIL WHERE EMAILID = ?"};
            for (String deleteQuery : deleteQueries) {
                try ( Connection connection = getConnection();  PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery);) {
                    deleteStatement.setInt(1, id);
                    result += deleteStatement.executeUpdate();
                }
            }
            LOG.info("# of records deleted for email with id : " + id + "is : " + result);
        } else {
            LOG.warn("Email with id : " + id + " to delete not found.");
        }
        return result;
    }

    /**
     * Updates an email's folder. An email in the sent folder cannot be moved to
     * the draft folder, nor can an email in the inbox folder be moved to the
     * draft folder. This method is separate from the update method to allow
     * folder updates on emails whether they are in the draft folder or not. An
     * email in the draft can be only be updated to the SENT folder when it gets
     * sent.
     *
     * @param newFolder folder to drag the email to.
     * @param emailBean whose folder needs to be updated.
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public void updateFolder(String newFolder, EmailBean emailBean) throws SQLException, IOException {
        if (findID(emailBean.getId()) != null) {
            String currentFolder = getFolder(emailBean);
            // An email in the sent folder cannot be moved to the draft folder.
            if (currentFolder.equals("INBOX") && newFolder.equals("DRAFT")) {
                LOG.warn("Cannot move the email with id : " + emailBean.getId() + "from the INBOX folder to the DRAFT folder.");
            } else if (currentFolder.equals("SENT") && newFolder.equals("DRAFT")) {
                LOG.warn("Cannot move the email with id : " + emailBean.getId() + "from the SENT folder to the DRAFT folder.");
            } else if (currentFolder.equals("DRAFT") && !(newFolder.equals("SENT") || newFolder.equals("OUTBOX"))) {
                // Cannot change email's folder from draft to a folder other than SENT.
                LOG.warn("Cannot move the email with id : " + emailBean.getId() + "from the DRAFT folder to any other folder than SENT.");
            } else if (!currentFolder.equals(newFolder)) {
                handleFolder(newFolder, emailBean);
            }
        } else {
            LOG.warn("Cannot update the email's folder, because the email with id : " + emailBean.getId() + "does not exist.");
        }
    }

    /**
     * Handles a folder update on the input emailBean. If the folder does not
     * exist, it creates it in the database and it updates the database
     * accordingly.
     *
     * @param folder (folder to move an email to.)
     * @param emailBean (emailBean that represents the email being dragged to
     * another folder.)
     * @throws SQLException
     */
    private void handleFolder(String folder, EmailBean emailBean) throws SQLException {
        String addFolder = "INSERT INTO FOLDERS (FOLDER) VALUES (?)";
        String update = "UPDATE EMAIL SET FOLDERID = ? WHERE EMAILID = ?";
        int folderId = -1;
        String selectQuery = "SELECT FOLDERID FROM FOLDERS WHERE FOLDER = ?";
        if (checkExists(selectQuery, folder) != -1) { // If the folder exists
            folderId = checkExists(selectQuery, folder);
        } else { // Add the folder to the database if it does not exists yet.
            try ( Connection connection = getConnection();  PreparedStatement insertStatement
                    = connection.prepareStatement(addFolder, Statement.RETURN_GENERATED_KEYS);) {
                insertStatement.setString(1, folder);
                insertStatement.executeUpdate();
                ResultSet insertResult = insertStatement.getGeneratedKeys();
                if (insertResult.next()) {
                    folderId = insertResult.getInt(1);
                }
            }
        }
        try ( Connection connection = getConnection();  PreparedStatement updateStatement = connection.prepareStatement(update);) {
            updateStatement.setInt(1, folderId);
            updateStatement.setInt(2, emailBean.getId());
            updateStatement.executeUpdate();
        }
        //Assign folderid to the input emailBean.
        emailBean.setFolderKey(folderId);
    }

    /**
     * Checks whether a record in the database exists according to the input
     * select query and the toCheck string. If it exists, it returns the
     * selected field(which is an id/primary key).
     *
     * @param selectQuery
     * @param toCheck
     * @return -1 if the record does not exist, otherwise, the record's primary
     * key is returned.
     * @throws SQLException
     */
    private int checkExists(String selectQuery, String toCheck) throws SQLException {
        try ( Connection connection = getConnection();  PreparedStatement check = connection.prepareStatement(selectQuery);) {
            check.setString(1, toCheck);
            ResultSet checkResult = check.executeQuery();
            if (checkResult.next()) {
                return checkResult.getInt(1);
            }
            return -1;
        }
    }

    /**
     * Updates an email record and its corresponding content, attachments and
     * recipients.The emailBean object must have an id that exists in the mail
     * database.It will update the mail database for the record with the
     * emailBean's id To be updated, the emailBean input must have a folderKey
     * that refers to the draft folder in the database, Otherwise, it cannot be
     * updated.This method does not update an email's folder.
     *
     * @param emailBean
     * @return 1 if emailBean was updated, otherwise, 0.
     * @throws SQLException
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.EmailLimitExceededException
     */
    @Override
    public int update(EmailBean emailBean) throws SQLException, IOException, EmailLimitExceededException {
        int result = 0;
        String folder = "";
        if (findID(emailBean.getId()) != null) {
            String updateQuery = "UPDATE EMAIL SET SUBJECT = ?, TEXT = ?,HTMLTEXT = ?, USERNAME = ?,"
                    + "FROMEMAILADDRESS = ?, DATE = ? WHERE EMAILID = ? ";
            folder = getFolder(emailBean);
            // Only update if the email's folder is DRAFT.
            if (folder.equals("DRAFT")) {
                try ( Connection connection = getConnection();  PreparedStatement updateStatement = connection.prepareStatement(updateQuery);) {
                    fillPreparedStatementForEmail(updateStatement, emailBean, "UPDATE");
                    updateStatement.setInt(7, emailBean.getId());
                    updateStatement.executeUpdate();
                    updateAttachments(emailBean);
                    updateRecipients(emailBean.getId(), (Email) emailBean.email);
                    result = 1;
                    LOG.info("Email was updated : " + (result > 0));
                }
            } else {
                LOG.warn("Email cannot be updated, because it is not in the draft folder, it is in the " + folder + "folder.");
            }
        } else {
            LOG.warn("The email with id :  " + emailBean.getId() + "you attempted to update does not exist.");
        }
        return result;
    }

    /**
     * Updates the attachments of an email with the given id. The input
     * EmailBean object parameter must contain an email that has contains the
     * updated attachments to set.
     *
     * @param emailBean that has an email with attachments that need to be
     * updated.
     * @throws SQLException
     * @throws IOException
     */
    private void updateAttachments(EmailBean emailBean) throws SQLException, IOException {
        // Remove the email's current attachments.
        removeAttachments(emailBean.getId());
        // Insert the updated attachments.
        setAttachments(emailBean);
    }

    /**
     * Removes an email's attachments from the database.
     *
     * @param id (an email's id)
     * @throws SQLException
     */
    private void removeAttachments(int id) throws SQLException {
        int result = 0;
        String deleteSql = "DELETE FROM ATTACHMENTS WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement deleteStatement = connection.prepareStatement(deleteSql);) {
            deleteStatement.setInt(1, id);
            result += deleteStatement.executeUpdate();
        }
        LOG.info("# of attachment records deleted for email with email id : " + id + " is : " + result);
    }

    /**
     * Updates an email object's recipients. Only an Email object can have its
     * recipients updated, since it can be in the draft folder.
     *
     * @param id (an email's record id)
     * @param email (an Email object)
     * @throws SQLException
     */
    private void updateRecipients(int id, Email email) throws SQLException {
        List<String[]> recipients = new ArrayList<>();
        recipients.add(convertEmailAddressesToStrings(email.to()));
        recipients.add(convertEmailAddressesToStrings(email.cc()));
        recipients.add(convertEmailAddressesToStrings(email.bcc()));
        //Remove all the recipients with the email's id.
        removeRecipients(id);
        String[] recipientTypes = {"TO", "CC", "BCC"};
        for (int i = 0; i < recipients.size(); i++) {
            if (recipients.get(i) != null && recipients.get(i).length != 0) {
                //Insert the new recipients.
                setRecipient(recipients.get(i), recipientTypes[i], id);
            }
        }
    }

    /**
     * Removes the recipients of the email with the input id.
     *
     * @param id (an email record's id).
     * @throws SQLException
     */
    private void removeRecipients(int id) throws SQLException {
        int result = 0;
        String deleteSql = "DELETE FROM EMAILTOADDRESS WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement delete = connection.prepareStatement(deleteSql);) {
            delete.setInt(1, id);
            result += delete.executeUpdate();
        }
        LOG.info("# of recipient records deleted for email with email id : " + id + " is : " + result);
    }

    /**
     * Retrieves all the records from the mail database associated with an email
     * with the input id. Returns the retrieved data as an emailBean object.
     *
     * @param id (primary key)
     * @return The Email object
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    @Override
    public EmailBean findID(int id) throws SQLException, IOException {
        EmailBean emailBean = null;
        String selectQuery = "SELECT EMAILID,SUBJECT,TEXT,HTMLTEXT,"
                + "USERNAME,FROMEMAILADDRESS,DATE,FOLDERID FROM EMAIL WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery);) {
            selectStatement.setInt(1, id);
            if (!find(selectStatement).isEmpty()) {
                // There should only be one EmailBean found
                // Since we are using its id to retrieve it.
                emailBean = find(selectStatement).get(0);
            }
        }
        LOG.info("Found " + id + "?: " + (emailBean != null));
        return emailBean;
    }

    /**
     * Retrieves all the records from the Email table that share the same value
     * in the fromEmailAddress column and returns the data as a list of
     * EmailBean objects
     *
     * @param from email address.
     * @return The arraylist of Email objects
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    @Override
    public List<EmailBean> findFrom(String from) throws SQLException, IOException {
        List<EmailBean> rows = new ArrayList<>();
        String selectQuery = "SELECT EMAILID,SUBJECT,TEXT,HTMLTEXT,"
                + "USERNAME,FROMEMAILADDRESS,DATE,FOLDERID FROM EMAIL WHERE FROMEMAILADDRESS = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery);) {
            selectStatement.setString(1, from);
            rows.addAll(find(selectStatement));
        }
        LOG.info("# of records found with from email address :  " + from + " is : " + rows.size());
        return rows;
    }

    /**
     * Retrieves all the records from the EMAIL table that share the same value
     * in the SUBJECT column and returns the data as an list of EmailBean
     * objects
     *
     * @param subject
     * @return The arraylist of Email objects
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    @Override
    public List<EmailBean> findSubject(String subject) throws SQLException, IOException {
        List<EmailBean> rows = new ArrayList<>();
        String selectQuery = "SELECT EMAILID,SUBJECT,TEXT,HTMLTEXT,"
                + "USERNAME,FROMEMAILADDRESS,DATE,FOLDERID,USERNAME FROM EMAIL WHERE SUBJECT = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery);) {
            selectStatement.setString(1, subject);
            rows.addAll(find(selectStatement));
        }
        LOG.info("# of records found with subject : " + subject + " is :" + rows.size());
        return rows;
    }

    /**
     * Finds all emails in the database with a specific email address in a
     * specific recipient field.
     *
     * @param address (email address to look for)
     * @param recipientField (CC,BCC,TO)
     * @return (List of emailBeans found)
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public List<EmailBean> findWith(String address, String recipientField)
            throws SQLException, IOException {
        List<EmailBean> rows = new ArrayList<>();
        String selectQuery = "SELECT EMAILID,SUBJECT,TEXT,"
                + "HTMLTEXT,FROMEMAILADDRESS,DATE,FOLDERID,USERNAME "
                + "FROM EMAIL INNER JOIN EMAILTOADDRESS USING(EMAILID) "
                + "INNER JOIN ADDRESSES USING(ADDRESSID) WHERE EMAILADDRESS = ? AND EMAILADDRESSTYPE = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery);) {
            selectStatement.setString(1, address);
            selectStatement.setString(2, recipientField);
            rows.addAll(find(selectStatement));
        }
        LOG.info("# of records found with : "
                + address + " in the " + recipientField + " field is : " + rows.size());
        return rows;
    }

    /**
     * Helper method that executes the select statement in the input
     * PreparedStatement. Returns the rows found as a list of EmailBean objects.
     *
     * @param selectStatement (select statement that selects records based on
     * specific criterias.)
     * @return a List of EmailBean objects found based on the input
     * PreparedStatement.
     * @throws SQLException
     * @throws IOException
     */
    private List<EmailBean> find(PreparedStatement selectStatement) throws SQLException, IOException {
        List<EmailBean> rows = new ArrayList<>();
        try ( ResultSet resultSet = selectStatement.executeQuery();) {
            while (resultSet.next()) {
                rows.add(createEmailData(resultSet));
            }
        }
        return rows;
    }

    /**
     * This method adds an Email object as a record to the database. The id
     * column is not included in the insert statement, because it is an
     * auto-increment field. It inserts the email's content, recipients and
     * attachments. It also assigns a folder to the email created. By default, a
     * ReceivedEmail object is assigned the "INBOX" folder and an Email object
     * is asssigned the "DRAFT" folder(before it gets sent).
     *
     * @param emailBean
     * @return the created email's id.
     * @throws SQLException
     * @throws java.io.IOException
     */
    @Override
    public int create(EmailBean emailBean) throws SQLException, IOException, EmailLimitExceededException {
        int recordNum;
        int result;
        String createQuery = "INSERT INTO EMAIL "
                + "(SUBJECT,TEXT,HTMLTEXT,USERNAME,FROMEMAILADDRESS,DATE,FOLDERID) "
                + "VALUES (?,?,?,?,?,?,?)";
        try ( Connection connection = getConnection();  PreparedStatement insertStatement
                = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForEmail(insertStatement, emailBean, "INSERT");
            result = insertStatement.executeUpdate();
            // Retrieve generated primary key value and assign it to an EmailBean
            try ( ResultSet insertResult = insertStatement.getGeneratedKeys();) {
                recordNum = -1;
                //ResultSet should only have one result.
                if (insertResult.next()) {
                    recordNum = insertResult.getInt(1);
                }
                emailBean.setDate(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES));
                emailBean.setId(recordNum);
                LOG.debug("New record ID is " + recordNum);
            }
            setRecipients(emailBean);
            setAttachments(emailBean);
        }
        LOG.info("# of records created : " + result);
        LOG.info("The id of the new record added to the Email table is : " + recordNum);
        return recordNum;
    }

    /**
     * Sets an email's folder for the first time. The folder assigned for a new
     * email received is INBOX, The folder assigned for an email saved, but not
     * sent is DRAFT. Therefore, when an email is sent, its folder must be
     * updated to SENT.
     *
     * @param insertStatement in which the folder is inserted.
     * @param emailBean
     * @throws SQLException
     */
    private void setFolder(PreparedStatement insertStatement, EmailBean emailBean) throws SQLException {
        String folder = "";
        if (emailBean.email instanceof ReceivedEmail) {
            folder = "INBOX";
        } else if (emailBean.email instanceof Email) {
            folder = "DRAFT";
        }
        int folderId = -1;
        String selectQuery = "SELECT FOLDERID FROM FOLDERS WHERE FOLDER = ?";
        if (checkExists(selectQuery, folder) != -1) {
            folderId = checkExists(selectQuery, folder);
            insertStatement.setInt(7, folderId);
            emailBean.setFolderKey(folderId);
        }
    }

    /**
     * Removes a folder from the database.It also deletes all the email records
     * associated with this folder.
     *
     * @param folder
     * @throws SQLException
     * @throws IOException
     */
    @Override
    public void removeFolder(String folder) throws SQLException, IOException {
        int deleted = 0;
        String selectQuery = "SELECT FOLDERID FROM FOLDERS WHERE FOLDER = ?";
        // Check that the folder exists in the database before removing it.
        if (checkExists(selectQuery, folder) != -1) {
            String deleteQuery = "DELETE FROM FOLDERS WHERE FOLDER = ?";
            List<EmailBean> emailBeans = this.findAll();
            for (EmailBean emailBean : emailBeans) {
                if (getFolder(emailBean).equals(folder)) {
                    this.delete(emailBean.getId());
                    deleted++;
                }
            }
            try ( Connection connection = getConnection();  PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery)) {
                deleteStatement.setString(1, folder);
            }
        } else {
            LOG.info("Folder : " + folder + "was not found in the mail database.");
        }
        LOG.info("Folder : " + folder + " and the  " + deleted + " emails it contained are deleted.");
    }

    /**
     * Adds the fields from an Email object to the PreparedStatement on behalf
     * of the create and update methods. If the operation input is "INSERT" The
     * prepared statement given as input must insert all the fields of an Email
     * record. If the operation input is "UPDATE", The prepared statement must
     * have all the fields of an Email record, except the folderid and the
     * emailid fields.
     *
     * @param statement
     * @param EmailBean emailBean object
     * @throws SQLException
     */
    private void fillPreparedStatementForEmail(PreparedStatement statement, EmailBean emailBean, String operation)
            throws SQLException, IOException, EmailLimitExceededException {
        String text = retrieveEmailMessage(emailBean.email)[0];
        String html = retrieveEmailMessage(emailBean.email)[1];
        // Check if email subject does not exceed the character limit.
        handleTextLimits(statement, emailBean.email.subject(), text);
        statement.setString(3, html);
        String fromAddress = emailBean.email.from().getEmail();
        // Add address to the address table if it does not exist yet.
        String selectQuery = "SELECT ADDRESSID FROM ADDRESSES WHERE EMAILADDRESS = ?";
        if (checkExists(selectQuery, fromAddress) == -1) {
            addAddress(fromAddress);
        }
        statement.setString(4, emailBean.getUsername());
        statement.setString(5, emailBean.email.from().getEmail());
        LocalDateTime date = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(date);
        statement.setTimestamp(6, timestamp);
        // Based on the preparedStatement's operation, we either set the folder or update it.
        if (operation.equals("INSERT")) {
            setFolder(statement, emailBean);
        } else if (operation.equals("UPDATE")) {
            updateFolder(getFolderFromKey(emailBean.getFolderKey()), emailBean);
        }
    }

    /**
     * Handles limits on an email's subject field or text field.
     *
     * @param statement (PreparedStatement that inserts/updates an email record)
     * @param subject (subject field of an email)
     * @param text (plain text field of an email)
     * @throws SQLException
     * @throws EmailLimitExceededException
     */
    private void handleTextLimits(PreparedStatement statement, String subject, String text) throws SQLException, EmailLimitExceededException {
        if (subject.length() < 256) {
            statement.setString(1, subject);
        } else {
            throw new EmailLimitExceededException("Email's subject limit was exceeded : " + subject);
        }
        if (text == null) {
            statement.setString(2, text);
        }
        if (text != null) {
            if (text.length() < 5001) {
                statement.setString(2, text);
            } else {
                throw new EmailLimitExceededException("Email's text message limit was exceeded : " + text);
            }
        }
    }

    /**
     * Retrieves a folder's name based on its primary key.
     *
     * @param key (a folder's primary key)
     * @return (Folder name)
     * @throws SQLException
     */
    private String getFolderFromKey(int key) throws SQLException {
        String folder = null;
        String selectQuery = "SELECT FOLDER FROM FOLDERS WHERE FOLDERID = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            selectStatement.setInt(1, key);
            ResultSet selectResult = selectStatement.executeQuery();
            while (selectResult.next()) {
                folder = selectResult.getString("FOLDER");
            }
        }
        return folder;
    }

    /**
     * Retrieves a folder's primary key based on its name.
     *
     * @param folder
     * @return (Folder name)
     * @throws SQLException
     */
    public int getKeyFromFolder(String folder) throws SQLException {
        int key = -1;
        String selectQuery = "SELECT FOLDERID FROM FOLDERS WHERE FOLDER = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            selectStatement.setString(1, folder);
            ResultSet selectResult = selectStatement.executeQuery();
            if (selectResult.next()) {
                key = selectResult.getInt("FOLDERID");
            }
        }
        return key;
    }

    /**
     * Retrieves all folders in the database.
     *
     * @return (folders)
     * @throws SQLException
     */
    public List<String> getFolders() throws SQLException {
        List<String> folders = new ArrayList<>();
        String selectQuery = "SELECT FOLDER FROM FOLDERS";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            ResultSet selectResult = selectStatement.executeQuery();
            while (selectResult.next()) {
                String folder = selectResult.getString("FOLDER");
                folders.add(folder);
            }
        }
        return folders;
    }

    /**
     * Helper method that retrieves an email's html text and plain text. Returns
     * an array containing two strings, the first string is the email's plain
     * text and the second string is the email's html text.
     *
     * @param email (CommonEmail object)
     * @return String [] containing the email's html and plain text.
     */
    private String[] retrieveEmailMessage(CommonEmail email) {
        String[] messages = new String[2];
        if (email.messages() != null) {
            for (Object emessage : email.messages()) {
                EmailMessage message = ((EmailMessage) (emessage));
                if (message.getMimeType().equals(MimeTypes.MIME_TEXT_PLAIN)) {
                    //Store the email's plain text.
                    messages[0] = message.getContent();
                }
                if (message.getMimeType().equals(MimeTypes.MIME_TEXT_HTML)) {
                    //Store the email's html text.
                    messages[1] = message.getContent();
                }
            }
        }
        return messages;
    }

    /**
     * Helper method to convert an array of EmailAddress objects to a String
     * array.
     *
     * @param emails (Array of EmailAddress objects)
     * @return String array converted from an array of EmailAddress
     */
    private String[] convertEmailAddressesToStrings(EmailAddress[] emails) {
        if (emails != null) {
            String[] stringEmails = new String[emails.length];
            for (int i = 0; i < emails.length; i++) {
                stringEmails[i] = emails[i].getEmail();
            }
            return stringEmails;
        }
        return null;
    }

    /**
     * Inserts all the recipients of an email into the database.
     *
     * @param emailBean that has an id and an email with recipients to set.
     * @throws SQLException
     */
    private void setRecipients(EmailBean emailBean) throws SQLException {
        CommonEmail commonEmail = emailBean.email;
        List<String[]> recipients = new ArrayList<>();
        recipients.add(convertEmailAddressesToStrings(commonEmail.to()));
        recipients.add(convertEmailAddressesToStrings(commonEmail.cc()));
        // Check type of commonEmail, since only Email objects have a bcc field.
        if (commonEmail instanceof Email) {
            recipients.add(convertEmailAddressesToStrings(((Email) commonEmail).bcc()));
            setRecipient(recipients.get(2), "BCC", emailBean.getId());
        }
        setRecipient(recipients.get(0), "TO", emailBean.getId());
        setRecipient(recipients.get(1), "CC", emailBean.getId());
    }

    /**
     * Adds an email address to the mail database in the ADDRESSES table. The
     * email address should be unique to be inserted in the Addresses table in
     * the database. Return its id.
     *
     * @param (address to add to the Addresses table)
     * @return id (id of the inserted address)
     */
    private int addAddress(String address) throws SQLException {
        int result = -1;
        String createQuery = "INSERT INTO ADDRESSES (EMAILADDRESS) VALUES (?)";
        try ( Connection connection = getConnection();  PreparedStatement insertStatement
                = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS)) {
            insertStatement.setString(1, address);
            insertStatement.executeUpdate();
            ResultSet keys = insertStatement.getGeneratedKeys();
            if (keys.next()) {
                result = keys.getInt(1);
            }
        }
        return result;
    }

    /**
     * Inserts an email's recipients for a specific recipient type to the
     * database.
     *
     * @param recipients (array of String email addresses
     * @param recipientType (CC,BCC,TO)
     * @param emailId (the id of the email that needs its recipients to be set.)
     * @throws SQLException
     */
    private void setRecipient(String[] recipients, String recipientType, int emailId) throws SQLException {
        int count = 0;
        String insertEmailToAddress = "INSERT INTO EMAILTOADDRESS "
                + "(EMAILID,ADDRESSID,EMAILADDRESSTYPE) VALUES (?,?,?)";
        int result = -1;
        try ( Connection connection = getConnection();  PreparedStatement insertBridging
                = connection.prepareStatement(insertEmailToAddress)) {
            if (recipients != null) {
                for (String address : recipients) {
                    // Check if recipient email address already exists in the database (in the ADDRESSES table).
                    String selectQuery = "SELECT ADDRESSID FROM ADDRESSES WHERE EMAILADDRESS = ?";
                    if (checkExists(selectQuery, address) == -1) {
                        result = addAddress(address);
                    } else {
                        PreparedStatement selectStatement = connection.prepareStatement("SELECT ADDRESSID "
                                + "FROM ADDRESSES WHERE EMAILADDRESS = ?");
                        selectStatement.setString(1, address);
                        ResultSet resultIds = selectStatement.executeQuery();
                        if (resultIds.next()) {
                            result = resultIds.getInt(1);
                        }
                    }
                    insertBridging.setInt(1, emailId);
                    insertBridging.setInt(2, result);
                    insertBridging.setString(3, recipientType);
                    count += insertBridging.executeUpdate();
                }
            }
        }
        LOG.info("# of recipients set for the  " + recipientType + " field : " + count);
    }

    /**
     * Inserts an email's attachments to the database.
     *
     * @param emailBean that contains an id and attachments to set.
     * @throws SQLException
     * @throws IOException
     */
    private void setAttachments(EmailBean emailBean) throws SQLException, IOException {
        String createQuery = "INSERT INTO ATTACHMENTS (FILENAME,ATTACHMENT,EMBEDDED,CONTENTID,EMAILID) VALUES (?,?,?,?,?)";
        List<EmailAttachment> attachments = emailBean.email.attachments();
        for (EmailAttachment<? extends DataSource> file : attachments) {
            try ( Connection connection = getConnection();  PreparedStatement insertStatement = connection.prepareStatement(createQuery)) {
                insertStatement.setString(1, file.getName());
                insertStatement.setBlob(2, file.getDataSource().getInputStream());
                insertStatement.setBoolean(3, file.isEmbedded());
                if (file.isEmbedded()) {
                    insertStatement.setString(4, file.getName());
                } else {
                    insertStatement.setString(4, null);
                }
                insertStatement.setInt(5, emailBean.getId());
                insertStatement.executeUpdate();
            }
        }
    }

    /**
     * Helper method that creates an object of type Email from the current
     * record in the ResultSet
     *
     * @param resultSet (resultset that contains email records.)
     * @return emailBean returns the data found from the resultSet as an
     * EmailBean object.
     * @throws SQLException
     */
    private EmailBean createEmailData(ResultSet resultSet) throws SQLException, IOException {
        CommonEmail email = Email.create();
        EmailBean emailBean = new EmailBean();
        emailBean.setId(resultSet.getInt("EMAILID"));
        emailBean.setFolderKey(resultSet.getInt("FOLDERID"));
        String folder = getFolder(emailBean);
        if (folder.equals("INBOX")) {
            email = ReceivedEmail.create()
                    .receivedDate(resultSet.getTimestamp("DATE"));
        } else if (folder.equals("SENT")) {
            email = Email.create()
                    .sentDate(resultSet.getTimestamp("DATE"));
        } else if (folder.equals("DRAFT")) {
            email = Email.create();
        }
        email.from(resultSet.getString("FROMEMAILADDRESS"));
        // Only insert the following fields if they are not null.
        if (resultSet.getString("SUBJECT") != null) {
            email.subject(resultSet.getString("SUBJECT"));
        }
        if (resultSet.getString("TEXT") != null) {
            email.textMessage(resultSet.getString("TEXT"));
        }
        if (resultSet.getString("HTMLTEXT") != null) {
            email.htmlMessage(resultSet.getString("HTMLTEXT"));
        }
        emailBean.setUsername(resultSet.getString("USERNAME"));
        emailBean.setDate(resultSet.getTimestamp("DATE").toLocalDateTime().truncatedTo(ChronoUnit.MINUTES));
        emailBean.setEmail(email);
        readBlobData(resultSet.getInt("EMAILID"), email);
        readRecipients(resultSet, email);
        return emailBean;

    }

    /**
     * Returns an emailBean's folder name.
     *
     * @param emailBean
     * @return folder
     * @throws SQLException
     */
    public String getFolder(EmailBean emailBean) throws SQLException {
        String folder = null;
        String selectQuery = "SELECT FOLDER FROM FOLDERS "
                + "INNER JOIN EMAIL USING (FOLDERID) WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            selectStatement.setInt(1, emailBean.getId());
            ResultSet selectResult = selectStatement.executeQuery();
            while (selectResult.next()) {
                folder = selectResult.getString("FOLDER");
            }
        }
        return folder;
    }

    /**
     * Reads an email's recipients from the database into the given CommonEmail
     * object.
     *
     * @param resultSet
     * @param email
     * @throws SQLException
     */
    private void readRecipients(ResultSet resultSet, CommonEmail email) throws SQLException {
        int emailId = resultSet.getInt("emailId");
        String select = "SELECT EMAILADDRESS,EMAILADDRESSTYPE,ADDRESSID "
                + "FROM ADDRESSES INNER JOIN EMAILTOADDRESS USING (ADDRESSID) WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(select)) {
            selectStatement.setInt(1, emailId);
            ResultSet selectResult = selectStatement.executeQuery();
            while (selectResult.next()) {
                String addressType = selectResult.getString("EMAILADDRESSTYPE");
                String address = selectResult.getString("EMAILADDRESS");
                if (addressType.equals("CC")) {
                    email.cc(address);
                }
                if (addressType.equals("BCC") && email instanceof Email) {
                    ((Email) (email)).bcc(address);
                }
                if (addressType.equals("TO")) {
                    email.to(address);
                }
            }
        }
    }

    /**
     * Reads an email's attachments from the database into a CommonEmail object.
     *
     * @param emailId
     * @param email
     * @return list of files/attachments.
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws IOException
     */
    private List<File> readBlobData(int emailId, CommonEmail email)
            throws SQLException, FileNotFoundException, IOException {
        List<File> files = new ArrayList<>();
        String select = "SELECT FILENAME, ATTACHMENT,EMBEDDED,CONTENTID FROM ATTACHMENTS WHERE EMAILID = ?";
        try ( Connection connection = getConnection();  PreparedStatement selectStatement = connection.prepareStatement(select);) {
            selectStatement.setInt(1, emailId);
            ResultSet selectResult = selectStatement.executeQuery();
            while (selectResult.next()) {
                String name = selectResult.getString("FILENAME");
                boolean embedded = selectResult.getBoolean("EMBEDDED");
                if (selectResult.getBinaryStream("ATTACHMENT") != null) {
                    File file = new File(name);
                    files.add(file);
                    try ( FileOutputStream fos = new FileOutputStream(file)) {
                        byte[] buffer = new byte[1024];
                        // Get the binary stream of our BLOB data
                        InputStream is = selectResult.getBinaryStream("ATTACHMENT");
                        while (is.read(buffer) > 0) {
                            fos.write(buffer);
                        }
                        if (embedded) {
                            String contentID = selectResult.getString("CONTENTID");
                            email.embeddedAttachment(EmailAttachment.with().
                                    content(file).contentId(contentID));
                        } else {
                            email.attachment(EmailAttachment.with().content(file));
                        }
                    }
                }
            }
        }
        return files;
    }

}
