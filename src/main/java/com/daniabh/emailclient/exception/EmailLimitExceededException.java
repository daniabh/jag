package com.daniabh.emailclient.exception;

/**
 *
 * This exception is thrown when an email's subject or text exceeds the limit set in the database.
 * @author Dania Bouhmidi
 */
public class EmailLimitExceededException extends Exception {
    /**
     * Constructor.
     * @param message 
     */
    public EmailLimitExceededException(String message) {
        super(message);
    }
    
 
    
}
