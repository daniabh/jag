package com.daniabh.emailclient.exception;

/**
 * Exception thrown when there is an invalid to field email address.
 *
 * @author Dania Bouhmidi
 */
public class InvalidTOException extends Exception {

    private String message;
    private String invalidRecipient;

    /**
     * Constructor : call the base constructor with input message. The input
     * message should be the specific invalid email address.
     *
     * @param invalidRecipient
     */
    public InvalidTOException(String invalidRecipient) {
        super(invalidRecipient);
        this.invalidRecipient = invalidRecipient;
        this.message = "There is an invalid TO recipient email address.";
    }

    /**
     * Returns the message.
     */
    @Override
    public String getMessage() {
        return this.message + " " + this.invalidRecipient;
    }

    /**
     * Returns the invalid recipient
     *
     * @return invalidRecipient
     */
    public String getInvalidRecipient() {
        return invalidRecipient;
    }

}
