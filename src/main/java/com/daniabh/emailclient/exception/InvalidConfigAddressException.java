package com.daniabh.emailclient.exception;

/**
 * Exception that is thrown when a user attempts to send or receive an email with a invalid email address.
 * @author Dania Bouhmidi
 */
public class InvalidConfigAddressException extends Exception {
    /**
     * Constructor : calls base class constructor with input message.
     * @param message 
     */
    public InvalidConfigAddressException (String message) {
        super(message);
    }
}
