package com.daniabh.emailclient.exception;

/**
 * Exception thrown when there is an invalid cc field email address.
 *
 * @author Dania Bouhmidi
 */
public class InvalidCCException extends Exception {

    private String message;
    private String invalidRecipient;

    /**
     * Constructor : calls the base class constructor with the input message.
     * The input message should be the specific invalid email address.
     *
     * @param invalidRecipient
     */
    public InvalidCCException(String invalidRecipient) {
        super(invalidRecipient);
        this.invalidRecipient = invalidRecipient;
        this.message = "There is an invalid CC recipient email address.";
    }

    /**
     * Returns the message.
     */
    @Override
    public String getMessage() {
        return this.message + " " + this.invalidRecipient;
    }

    /**
     * Returns the invalid recipient
     *
     * @return invalidRecipient
     */
    public String getInvalidRecipient() {
        return invalidRecipient;
    }

}
