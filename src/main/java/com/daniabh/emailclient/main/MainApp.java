package com.daniabh.emailclient.main;

import com.daniabh.emailclient.controller.PropertiesFormController;
import com.daniabh.emailclient.controller.RootLayoutController;
import com.daniabh.emailclient.properties.PropertiesManager;
import com.daniabh.emailclient.javafxbean.PropertyBean;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;

/**
 * This class launches the JavaFX application by using the FXML/Controller
 * architecture.
 *
 * @author Dania Bouhmidi
 */
public class MainApp extends Application {

    // slf4j log4j logger
    private final Logger LOG = LoggerFactory.getLogger(this.getClass()
            .getName());
    
    private Stage primaryStage;
    private Parent rootPane;
    private PropertyBean propertyBean;
    private PropertiesManager propertiesManager;
    private RootLayoutController rootController;
    private Scene formScene;
    private final Locale currentLocale;

    /**
     * Default constructor Instantiates the current locale.
     */
    public MainApp() {
        currentLocale = Locale.getDefault();
        LOG.debug("Locale = " + currentLocale);
    }

    /**
     * Overrides the start method from the Application class. Receives the stage
     * object, creates a scene and displays it.
     *
     * @param primaryStage
     * @throws java.io.IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException, Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));
        rootController = new RootLayoutController();
        
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/email.png")));
        rootController.setStage(primaryStage);
        
        formScene = createPropertiesForm();
        if (!checkForProperties()) {
            this.primaryStage.setScene(formScene);
        } else {
            this.primaryStage.setScene(initRootLayout());
        }
        
        primaryStage.show();
        
    }

    /**
     * Creates the properties form to be filled by the user.
     *
     * @return scene
     * @throws Exception
     */
    private Scene createPropertiesForm() throws Exception {
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();
        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource("/fxml/PropertiesForm.fxml"));
        
        loader.setResources(ResourceBundle.getBundle("MessagesBundle",currentLocale));
        
        Parent root = (GridPane) loader.load();
        
        PropertiesFormController controller = loader.getController();
        propertyBean = new PropertyBean();
        propertiesManager = new PropertiesManager();
        controller.setupProperties(propertiesManager, propertyBean);
        controller.setStageController(primaryStage, rootController);
        
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Check if a Properties file exists and can be loaded into a bean. Does not
     * verify that the contents of the bean fields are valid.
     *
     * @return boolean indicating whether a properties file was found or not.
     */
    private boolean checkForProperties() {
        boolean found = false;
        try {
            if (propertiesManager.loadTextProperties(propertyBean, "", "MailConfig")) {
                found = true;
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
            errorAlert("checkForProperties error");
        }
        return found;
    }

    /**
     * The stop method is called before the stage is closed.
     */
    @Override
    public void stop() {
        LOG.info("Stage is closing");
    }

    /**
     * Load the root layout of the FXML application.
     *
     * @return Scene
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    public Scene initRootLayout() throws IOException, SQLException {
        
        FXMLLoader loader = new FXMLLoader();
        // Configure the FXMLLoader with the i18n locale resource bundles
        loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));

        // Connect the FXMLLoader to the fxml file that is stored in the jar
        loader.setLocation(MainApp.class
                .getResource("/fxml/RootLayout.fxml"));

        // The load command returns a reference to the root pane of the fxml file
        rootPane = (Parent) loader.load();
        rootController = loader.getController();
        rootController.setPropertyBean(propertyBean);
        rootController.getTreeController().setEmailDAO(rootController.getEmailDAO());
        rootController.getTableController().setEmailDAO(rootController.getEmailDAO());
        rootController.getHtmlController().setEmailDAO(rootController.getEmailDAO());
        rootController.getHtmlController().setConfigBean(rootController.getConfigBean());
        rootController.getTableController().setTreeController(rootController.getTreeController());
        rootController.getTableController().setPropertyBean(propertyBean);
        rootController.getHtmlController().setTableController(rootController.getTableController());
        
        rootController.getTreeController().displayTree();
        Scene scene = new Scene(rootPane);
        return scene;
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(msg);
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("errorTitle"));
        dialog.show();
    }

    /**
     * Main method
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
