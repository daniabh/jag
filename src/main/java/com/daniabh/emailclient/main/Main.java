package com.daniabh.emailclient.main;

/**
 * Main Class Calls the JavaFX MainApp's main method.
 *
 * @author Dania Bouhmidi
 */
public class Main {

    /**
     * Main method that calls the (JavaFx) MainApp's main method.
     *
     * @param args
     */
    public static void main(String[] args) {
        MainApp.main(args);
    }
}
