package com.daniabh.emailclient.controller;

import com.daniabh.emailclient.business.Mailer;
import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.exception.EmailLimitExceededException;
import com.daniabh.emailclient.exception.InvalidBCCException;
import com.daniabh.emailclient.exception.InvalidCCException;
import com.daniabh.emailclient.exception.InvalidConfigAddressException;
import com.daniabh.emailclient.exception.InvalidTOException;
import com.daniabh.emailclient.javafxbean.EmailData;
import com.daniabh.emailclient.javafxbean.EmailFXBean;
import com.daniabh.emailclient.javafxbean.FormData;
import com.daniabh.emailclient.javafxbean.MessageBean;
import com.daniabh.emailclient.persistence.EmailDAOImpl;
import com.daniabh.emailclient.properties.MailConfigBean;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jodd.mail.CommonEmail;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.ReceivedEmail;

/**
 * FXML Controller class for the Email Composition Module. Allows the user to
 * enter cc,bcc,to and subject fields for the email they want to compose. Also
 * displays an email in the html editor when it is clicked on.
 *
 * @author Dania Bouhmidi
 */
public class MailDisplayController {

    @FXML // fx:id="fromField"
    private TextField fromField; // Value injected by FXMLLoader
    @FXML // fx:id="toField"
    private TextField toField; // Value injected by FXMLLoader

    @FXML // fx:id="ccField"
    private TextField ccField; // Value injected by FXMLLoader

    @FXML // fx:id="bccField"
    private TextField bccField; // Value injected by FXMLLoader

    @FXML // fx:id="subjectField"
    private TextField subjectField; // Value injected by FXMLLoader

    @FXML // fx:id="regularAttachmentButton"
    private Button regularAttachmentButton; // Value injected by FXMLLoader

    @FXML // fx:id="embeddedAttachmentButton"
    private Button embeddedAttachmentButton; // Value injected by FXMLLoader

    @FXML // fx:id="saveButton"
    private Button saveButton; // Value injected by FXMLLoader

    @FXML // fx:id="plainText"
    private TextArea plainText; // Value injected by FXMLLoader

    @FXML // fx:id="sendReceiveButton"
    private Button sendReceiveButton; // Value injected by FXMLLoader

    @FXML // fx:id="cancelButton"
    private Button cancelButton; // Value injected by FXMLLoader

    @FXML // fx:id="emailHTMLLayout"
    private AnchorPane emailHTMLLayout; // Value injected by FXMLLoader

    @FXML // fx:id="form"
    private GridPane form; // Value injected by FXMLLoader

    private EmailDAOImpl emailDAO;

    @FXML
    private HTMLEditor htmlEditor;

    // Resource bundle is injected when controller is loaded
    @FXML
    private ResourceBundle resources;

    @FXML // fx:id="mailDisplay"
    private GridPane mailDisplay; // Value injected by FXMLLoader

    @FXML // fx:id="removeAttachmentsButton"
    private Button removeAttachmentsButton; // Value injected by FXMLLoader
    private Mailer mailer;
    private MailConfigBean configBean;
    private List<File> regularAttachments = new ArrayList<>();
    private List<File> embeddedAttachments = new ArrayList<>();
    //Display mode or Composition Mode
    private String mode;
    private EmailTableController tableController;
    private FormData formData = new FormData();
    private MessageBean messageBean = new MessageBean();

    private String currentEmailStatus;
    private final static Logger LOG = LoggerFactory.getLogger(MailDisplayController.class);

    private EmailBean currentEmail;

    /**
     * Initializes the controller class.
     *
     */
    @FXML
    void initialize() {
        assert emailHTMLLayout != null : "fx:id=\"emailHTMLLayout\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert form != null : "fx:id=\"form\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert toField != null : "fx:id=\"toField\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert ccField != null : "fx:id=\"ccField\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert bccField != null : "fx:id=\"bccField\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert subjectField != null : "fx:id=\"subjectField\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert regularAttachmentButton != null : "fx:id=\"regularAttachmentButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert embeddedAttachmentButton != null : "fx:id=\"embeddedAttachmentButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert removeAttachmentsButton != null : "fx:id=\"removeAttachmentsButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert sendReceiveButton != null : "fx:id=\"sendReceiveButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert saveButton != null : "fx:id=\"saveButton\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert fromField != null : "fx:id=\"fromField\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert plainText != null : "fx:id=\"plainText\" was not injected: check your FXML file 'htmlView.fxml'.";
        assert htmlEditor != null : "fx:id=\"htmlEditor\" was not injected: check your FXML file 'htmlView.fxml'.";
        LOG.info("Initializing the MailDisplayController");
        this.ccField.setText("");
        this.bccField.setText("");
        this.toField.setText("");
        this.fromField.setText("");
        this.subjectField.setText("");
        this.plainText.setText("");
        this.htmlEditor.autosize();
        this.currentEmailStatus = "toSend";
        this.mode = "COMPOSITION";
        this.bindForm();

    }

    /**
     * Sets the MailConfigBean
     *
     * @param configBean
     */
    public void setConfigBean(MailConfigBean configBean) {
        this.configBean = configBean;
        this.mailer = new Mailer(configBean);
        this.fromField.setText(this.configBean.getUserEmailAddress());
        this.fromField.setDisable(true);
    }

    /**
     * Sets the tableController that will be used to keep tracks of the table
     * records, and add new emails.
     *
     * @param tableController
     */
    public void setTableController(EmailTableController tableController) {
        this.tableController = tableController;
    }

    @FXML
    /**
     * Cancels an email so that it is not sent Assigns the email the outbox
     * folder.
     */
    void cancelEmail(ActionEvent event) {
        this.currentEmailStatus = "Cancelled";
        this.cancelButton.setDisable(true);
    }

    /**
     * Saves the current email data from the cc,bcc,to,from, fields as well as
     * the htmlEditor text as draft email into the database.
     */
    @FXML
    void onSave() throws SQLException, IOException, EmailLimitExceededException {
        String[] to = null, cc = null, bcc = null;
        if (!this.formData.getTo().isEmpty()) {
            to = this.formData.getTo().split(",");
        }
        if (!this.formData.getCc().isEmpty()) {
            cc = this.formData.getCc().split(",");
        }
        if (!this.formData.getBcc().isEmpty()) {
            bcc = this.formData.getBcc().split(",");
        }

        String subject = this.formData.getSubject();
        Email email = Email.create().subject(subject).from(this.configBean.getUserEmailAddress()).to(to).cc(cc).bcc(bcc)
                .textMessage(messageBean.getTextMessage()).htmlMessage(this.htmlEditor.getHtmlText());
        for (File file : this.regularAttachments) {
            email.attachment(EmailAttachment.with().content(file));
        }
        for (File file : this.embeddedAttachments) {
            email.embeddedAttachment(EmailAttachment.with().content(file).contentId(file.getName()));
        }
        //Only create a new emailBean object if the current email is not already saved in the database.
        if (this.currentEmail == null) {
            EmailBean bean = new EmailBean(email);
            this.emailDAO.create(bean);
        } else { //Otherwise, update it
            LOG.info("Updating the email.");
            this.currentEmail.setEmail(email);
            this.emailDAO.update(this.currentEmail);
        }
        this.tableController.getEmailDataTable().setItems(this.tableController.retrieveEmailData());

    }

    /**
     * Binds the form
     */
    private void bindForm() {
        formData.getSubjectProperty().bindBidirectional(this.subjectField.textProperty());
        formData.getCcProperty().bindBidirectional(this.ccField.textProperty());
        formData.getBccProperty().bindBidirectional(this.bccField.textProperty());
        formData.getToProperty().bindBidirectional(this.toField.textProperty());
        messageBean.getMessageProperty().bindBidirectional(this.plainText.textProperty());
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * emailDAO object.
     *
     * @param emailDAO
     */
    public void setEmailDAO(EmailDAOImpl emailDAO) {
        this.emailDAO = emailDAO;
    }

    @FXML
    /**
     * Attach file event handler.
     *
     * @param event
     */
    public void attachRegularFile(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        List<File> files = fileChooser.showOpenMultipleDialog(stage);
        fileChooser.setTitle("Open Multiple Files");
        if (files != null) {
            for (File file : files) {
                LOG.info("Absolute Path: " + file.getAbsolutePath());
                this.regularAttachments.add(file);
            }
        }
    }

    @FXML
    /**
     * Event handler for the button to attach an embedded file. Adds the files
     * to the list of embedded files, and updates the html text of the html
     * editor accordingly.
     *
     * @param event
     */
    public void attachEmbeddedFile(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        List<File> files = fileChooser.showOpenMultipleDialog(stage);
        fileChooser.setTitle("Open Multiple Files");
        if (files != null) {
            for (File file : files) {
                LOG.info("Absolute Path: " + file.getAbsolutePath());
                this.embeddedAttachments.add(file);
                String htmlToEdit = htmlEditor.getHtmlText();
                StringBuilder sb = new StringBuilder(htmlToEdit);
                int lastBodyTagPos = sb.lastIndexOf("</body>");
                String toEmbed = "<embed src=" + file.getName() + ">";
                toEmbed = this.editHtml(toEmbed);
                sb.insert(lastBodyTagPos, toEmbed);
                EmailAttachment attachment = EmailAttachment.with().contentId(file.getName()).content(file).buildFileDataSource(file.getName(), file);
                this.saveAttachment(attachment);
                this.htmlEditor.setHtmlText(sb.toString());
            }
        }
        LOG.info("Added embedded attachments : " + this.htmlEditor.getHtmlText());
    }

    /**
     * Displays the form containing the cc,bcc,to and subject fields of an
     * email.
     *
     * @param bean
     */
    public void displayForm(EmailFXBean bean) {
        fromField.setText(bean.getEmail().from().getEmail());
        toField.setText(getEmailAddressesAsStrings(bean.getEmail().to()));
        ccField.setText(getEmailAddressesAsStrings(bean.getEmail().cc()));
        //Only display the bcc field if the email is an email that the config user has sent.
        if (bean.getEmail() instanceof Email) {
            bccField.setText(getEmailAddressesAsStrings(((Email) bean.getEmail()).bcc()));
        }
        subjectField.setText(bean.getEmail().subject());
    }

    /**
     * Returns a String representation of an array of EmailAddress objects.
     *
     * @param emailArray
     * @return String
     */
    public String getEmailAddressesAsStrings(EmailAddress[] emailArray) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < emailArray.length; i++) {
            EmailAddress address = emailArray[i];
            sb.append(address.getEmail());
            if (i != emailArray.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * Displays the message bean and the email data that represent a specific
     * email in the email tableView in the EmailTableController
     *
     * @param emailData
     * @param messageBean
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public void displayEmail(EmailData emailData, MessageBean messageBean) throws SQLException, IOException {
        this.currentEmail = this.emailDAO.findID(emailData.getId());
        this.subjectField.setText(emailData.getSubject());
        this.plainText.setText(messageBean.getTextMessage());
        EmailFXBean emailfxBean = new EmailFXBean();
        EmailBean emailBean = emailDAO.findID(emailData.getId());
        CommonEmail email = emailBean.email;
        emailfxBean.setEmail(email);
        this.saveEmbeddedAttachments(emailfxBean);
        this.htmlEditor.setHtmlText((messageBean.getHtmlMessage()));
        this.mode = "DISPLAY";
        //Disable email fields
        this.disableEmailComponents(true);
        if (emailDAO.getFolder(emailBean).equals("DRAFT")) {
            this.retrieveDraftAttachments();
            this.mode = "COMPOSITION";
            this.removeAttachmentsButton.setDisable(false);
            this.regularAttachmentButton.setDisable(false);
            this.embeddedAttachmentButton.setDisable(false);
            this.disableEmailComponents(false);
        }
    }

    /**
     * Retrieves the attachments of a draft email.
     *
     * @throws IOException
     */
    private void retrieveDraftAttachments() throws IOException {
        Email email = (Email) this.currentEmail.email;
        for (EmailAttachment attachment : email.attachments()) {
            byte[] bytes = attachment.getDataSource().getInputStream().readAllBytes();
            File file = new File("C:/Temp/" + attachment.getName());
            OutputStream outputStream
                    = new FileOutputStream(file);
            // Starts writing the bytes in it 
            outputStream.write(bytes);
            if (attachment.isEmbedded()) {
                this.embeddedAttachments.add(file);
            } else {
                this.regularAttachments.add(file);
            }

        }

    }

    /**
     * Enables/disables the email fields/buttons based on the boolean parameter
     * that is passed to it.
     *
     * @param enable
     */
    public void disableEmailComponents(boolean enable) {
        this.fromField.setDisable(true);
        this.bccField.setDisable(enable);
        this.ccField.setDisable(enable);
        this.toField.setDisable(enable);
        this.subjectField.setDisable(enable);
        this.saveButton.setDisable(enable);
        this.regularAttachmentButton.setDisable(enable);
        this.embeddedAttachmentButton.setDisable(enable);
        this.removeAttachmentsButton.setDisable(enable);
        this.plainText.setDisable(enable);
        this.cancelButton.setDisable(enable);
        this.disableHtmlEditor(enable);
    }

    /**
     * Disables/Enables the html editor based on the boolean parameter passed to
     * it. Replaces the html editor with a WebView(for received emails and sent
     * emails) so that the content is not editable, but still scrollable. When
     * it comes to draft emails, the webview gets replaced by the htmlEditor so
     * that the content is editable.
     */
    private void disableHtmlEditor(boolean enable) {

        if (enable) {
            String htmlString = htmlEditor.getHtmlText();
            WebView browser = new WebView();

            if (htmlString.contains("contenteditable=\"true\"")) {
                htmlString = htmlString.replace("contenteditable=\"true\"", "contenteditable=\"false\"");
            }
            browser.getEngine().loadContent(htmlString);

            mailDisplay.getChildren().clear();
            mailDisplay.getChildren().add(browser);
        } else {

            mailDisplay.getChildren().clear();

            mailDisplay.getChildren().add(this.htmlEditor);
        }
    }

    /**
     * Clears the html editor text and the email fields.
     */
    public void clear() {
        this.currentEmail = null;
        this.htmlEditor.setHtmlText("");
        this.mode = "COMPOSITION";
        toField.clear();
        ccField.clear();
        bccField.clear();
        subjectField.clear();
        plainText.clear();
        fromField.setText(this.configBean.getUserEmailAddress());
        this.regularAttachments = new ArrayList<>();
        this.embeddedAttachments = new ArrayList<>();
    }

    /**
     * Enables email composition in the html editor, enables the text fields.
     *
     * @param event
     */
    @FXML
    public void enableEmailComposition(ActionEvent event) {
        //Clear email table selection
        this.tableController.getEmailDataTable().getSelectionModel().clearSelection();
        this.clear();
        this.fromField.setText(this.configBean.getUserEmailAddress());
        this.fromField.setDisable(true);
        this.disableEmailComponents(false);
        this.mode = "COMPOSITION";
        this.cancelButton.setDisable(false);
    }

    @FXML
    /**
     * Send and receive button event handler
     *
     * @param event
     */
    public void sendAndReceive(ActionEvent event) throws InvalidConfigAddressException, SQLException, IOException, EmailLimitExceededException, InvalidCCException, InvalidBCCException, InvalidTOException {
        LOG.info("Send and receive button clicked.");
        boolean draft = false;
        if (this.tableController.getEmailDataTable().getSelectionModel().getSelectedItem() != null) {
            int id = this.tableController.getEmailDataTable().getSelectionModel().getSelectedItem().getId();
            draft = this.emailDAO.getFolder(this.emailDAO.findID(id)).equals("DRAFT");
        }
        this.sendEmail(draft);
        this.receiveEmails();
        this.tableController.getEmailDataTable().refresh();
        this.clear();
    }

    /**
     * Sends the current (draft) email If the current email status is
     * "Cancelled" it does not get sent and it gets assigned the "Outbox"
     * folder.
     *
     * @param draft
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws java.sql.SQLException
     * @throws com.daniabh.emailclient.exception.EmailLimitExceededException
     * @throws com.daniabh.emailclient.exception.InvalidCCException
     * @throws com.daniabh.emailclient.exception.InvalidBCCException
     * @throws com.daniabh.emailclient.exception.InvalidTOException
     */
    public void sendEmail(boolean draft) throws IOException, InvalidConfigAddressException, SQLException, EmailLimitExceededException, InvalidCCException, InvalidBCCException, InvalidTOException {
        //Send email only if the current mode is Composition, and not Display
        if (this.mode.equals("COMPOSITION")) {
            List<String> to = null, cc = null, bcc = null;
            //Send the composed email
            MessageBean messageBean = new MessageBean();
            messageBean.setTextMessage(htmlEditor.getHtmlText());
            if (!this.toField.getText().isEmpty()) {
                to = Arrays.asList(this.toField.getText().split(","));
            }
            if (!this.ccField.getText().isEmpty()) {
                cc = Arrays.asList(this.ccField.getText().split(","));
            }
            if (!this.bccField.getText().isEmpty()) {
                bcc = Arrays.asList(this.bccField.getText().split(","));
            }
            String subject = this.subjectField.getText();
            Email email;
            if (!this.currentEmailStatus.equals("Cancelled")) {
                LOG.info("Sending the email.");
                if (draft) {
                    int id = this.tableController.getEmailDataTable().getSelectionModel().getSelectedItem().getId();
                    EmailBean emailBean = this.emailDAO.findID(id);
                    emailBean.email.from(this.configBean.getUserEmailAddress());
                    Email update = mailer.sendEmail(this.regularAttachments, this.embeddedAttachments, subject, plainText.getText(), htmlEditor.getHtmlText(), to, cc, bcc);
                    emailBean.setDate(LocalDateTime.now());
                    emailBean.setEmail(update);
                    this.emailDAO.update(emailBean);
                    this.emailDAO.updateFolder("SENT", emailBean);
                } else {
                    try {
                        email = mailer.sendEmail(this.regularAttachments, this.embeddedAttachments, subject, plainText.getText(), htmlEditor.getHtmlText(), to, cc, bcc);
                        EmailBean bean = new EmailBean(email);
                        this.emailDAO.create(bean);
                        //Update the folder to sent
                        this.emailDAO.updateFolder("SENT", bean);
                    } catch (InvalidCCException | InvalidBCCException | EmailLimitExceededException | IllegalArgumentException | InvalidTOException ex) {

                        this.errorAlert(ex.getMessage());
                    }
                }
            } else {
                EmailBean emailBean;
                email = this.retrieveEmail();
                if (draft) {
                    int id = this.tableController.getEmailDataTable().getSelectionModel().getSelectedItem().getId();
                    emailBean = this.emailDAO.findID(id);
                    emailBean.setEmail(email);
                    emailBean.setDate(LocalDateTime.now());
                    this.emailDAO.update(emailBean);
                } else {

                    emailBean = new EmailBean(email);
                    this.emailDAO.create(emailBean);
                }
                //Update the folder to OUTBOX
                this.emailDAO.updateFolder("OUTBOX", emailBean);
            }
            this.tableController.getTreeController().retrieveFolders();
            this.tableController.getEmailDataTable().setItems(this.tableController.retrieveEmailData());
        }
    }

    /**
     * Retrieves an email from the text fields and html editor.
     *
     * @return email
     */
    private Email retrieveEmail() {
        Email email = Email.create().from(this.configBean.getUserEmailAddress()).subject(this.subjectField.getText()).textMessage(plainText.getText()).
                htmlMessage(htmlEditor.getHtmlText()).to(this.toField.getText().split(","))
                .cc(this.ccField.getText().split(",")).bcc(this.bccField.getText().split(","));
        this.regularAttachments.forEach(attachment -> {
            email.attachment(EmailAttachment.with().content(attachment));
        });
        this.embeddedAttachments.forEach(attachment -> {
            email.embeddedAttachment(EmailAttachment.with().content(attachment).contentId(attachment.getName()));
        });
        return email;
    }

    /**
     * Receives any new email the config user has.Adds them to the database.
     *
     * @throws com.daniabh.emailclient.exception.InvalidConfigAddressException
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     * @throws com.daniabh.emailclient.exception.EmailLimitExceededException
     */
    public void receiveEmails() throws InvalidConfigAddressException, SQLException, IOException, EmailLimitExceededException {
        //Receive emails if there are any
        ReceivedEmail[] receivedEmails = mailer.receiveEmail(configBean);
        for (ReceivedEmail remail : receivedEmails) {
            EmailBean receivedBean = new EmailBean(remail);
            this.emailDAO.create(receivedBean);
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Mail Composition Error ");
        dialog.setHeaderText(msg);
        dialog.show();
    }

    /**
     * Removes all the attachments from the current (draft) email.
     *
     * @throws java.io.IOException
     */
    @FXML
    public void removeAttachments() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource("/fxml/AttachmentList.fxml"));

        AnchorPane root = (AnchorPane) loader.load();
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setGraphic(root);
        dialog.setTitle("Choose files to delete. ");
        dialog.setHeaderText("Remove files.");
        dialog.show();
        dialog.setOnCloseRequest(e -> dialog.hide());

        AttachmentController controller = loader.getController();

        controller.setHtmlController(this);

    }

    /**
     * Gets the regular attachments
     *
     * @return regularAttachments
     */
    public List<File> getRegularAttachments() {
        return regularAttachments;
    }

    /**
     * Gets the embedded attachments
     *
     * @return embeddedAttachments
     */
    public List<File> getEmbeddedAttachments() {
        return embeddedAttachments;
    }

    /**
     * Displays a Reply-To form, so that the user can reply to a specific email.
     *
     * @param fxbean
     * @param messageBean
     */
    public void displayReplyForm(EmailFXBean fxbean, MessageBean messageBean) {
        this.clear();
        CommonEmail emailToReply = fxbean.getEmail();
        fromField.setText(this.configBean.getUserEmailAddress());
        toField.setText(emailToReply.from().getEmail());
        subjectField.setText(emailToReply.subject());
        plainText.setText(messageBean.getTextMessage());
        this.htmlEditor.setHtmlText(messageBean.getHtmlMessage() + "<hr>");
        this.disableEmailComponents(false);
        this.mode = "COMPOSITION";
    }

    /**
     * Displays a Reply All form, so that the user can reply to all the cc, and
     * to recipients of a specific email.
     *
     * @param fxbean
     * @param messageBean
     */
    public void displayReplyAllForm(EmailFXBean fxbean, MessageBean messageBean) {
        this.clear();
        CommonEmail emailToReply = fxbean.getEmail();
        fromField.setText(this.configBean.getUserEmailAddress());
        toField.setText(emailToReply.from().getEmail());
        ccField.setText(getEmailAddressesAsStrings(fxbean.getEmail().cc()));
        subjectField.setText(emailToReply.subject());
        plainText.setText(messageBean.getTextMessage());
        this.htmlEditor.setHtmlText(messageBean.getHtmlMessage() + "<hr>");
        this.disableEmailComponents(false);
        this.mode = "COMPOSITION";
    }

    /**
     * Displays a forward email form, so that the user can forward a specific
     * email.
     *
     * @param fxbean
     * @param messageBean
     */
    public void displayForwardForm(EmailFXBean fxbean, MessageBean messageBean) {
        //Display email body
        this.clear();
        fromField.setText(this.configBean.getUserEmailAddress());
        plainText.setText(messageBean.getTextMessage());
        this.htmlEditor.setHtmlText(messageBean.getHtmlMessage() + "<hr>");
        this.disableEmailComponents(false);
        this.mode = "COMPOSITION";
    }

    /**
     * Helper method to modify an HTML string to be able to show embedded
     * attachments.
     *
     * @param emailBean
     * @return html
     */
    private String editHtml(String html) {
        int index = html.indexOf("src=");
        if (index >= 0) {
            StringBuilder sb = new StringBuilder(html);
            sb.replace(index, index + 4, "src=file:/C:/Temp/");
            html = sb.toString();
        }
        return html;
    }

    /**
     * Helper method to save embedded attachments to a temporary location so
     * that they can be displayed.
     *
     * @param emailBean bean that contains an email with embedded attachments
     */
    private void saveEmbeddedAttachments(EmailFXBean emailBean) {
        //Save all the embedded attachments to disk in the Temp folder
        for (Object file : emailBean.getEmail().attachments()) {
            EmailAttachment attachment = ((EmailAttachment) file);
            if (attachment.isEmbedded()) {
                this.saveAttachment(attachment);
            }
        }
    }

    /**
     * Saves an email's embdedded attachments in a temporary location (to the
     * C:\\Temp folder), so that they can be viewed and displayed in the
     * HtmlEditor.
     *
     * @param attachment (to save)
     */
    private void saveAttachment(EmailAttachment attachment) {
        File directory = new File("C:\\Temp");
        if (!directory.exists()) {
            directory.mkdir();
        }
        File file = new File("C:\\Temp", attachment.getName());
        if (!file.exists()) {
            try {

                byte[] bytes = attachment.getDataSource().getInputStream().readAllBytes();
                // Starts writing the bytes in it
                try ( OutputStream outputStream = new FileOutputStream(file)) {
                    // Starts writing the bytes in it
                    outputStream.write(bytes);
                }

            } catch (IOException e) {
                LOG.error("Saving the file to temp has caused an error", e);
                Platform.exit();
            }
        }
    }

    /**
     * Gets the htmlEditor.
     *
     * @return htmlEditor
     */
    public HTMLEditor getHtmlEditor() {
        return htmlEditor;
    }
}
