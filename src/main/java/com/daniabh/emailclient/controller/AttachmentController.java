/**
 * Sample Skeleton for 'AttachmentList.fxml' Controller Class
 */
package com.daniabh.emailclient.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controlle for the removal embedded attachment and regular attachments.
 * Interacts with the HtmlViewController to allow the user to remove specific
 * attachments from their draft email.
 *
 * @author Dania Bouhmidi
 */
public class AttachmentController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="removeSpecificAttachments"
    private Button removeSpecificAttachments; // Value injected by FXMLLoader

    @FXML // fx:id="embeddedAttachmentsList"
    private ListView<File> embeddedAttachmentsList; // Value injected by FXMLLoader

    private MailDisplayController mailDisplayController;

    @FXML // fx:id="regularAttachmentsList"
    private ListView<File> regularAttachmentsList; // Value injected by FXMLLoader

    private final static Logger LOG = LoggerFactory.getLogger(AttachmentController.class);

    /**
     * Sets the MailDisplayController Sets up the regular attachments and
     * embedded attachments from this controller.
     *
     * @param mailDisplayController
     */
    public void setHtmlController(MailDisplayController mailDisplayController) {
        this.mailDisplayController = mailDisplayController;
        ObservableList<File> regular = FXCollections.observableList(mailDisplayController.getRegularAttachments());
        ObservableList<File> embedded = FXCollections.observableList(mailDisplayController.getEmbeddedAttachments());
        this.regularAttachmentsList.setItems(regular);
        this.embeddedAttachmentsList.setItems(embedded);
    }

    @FXML
    /**
     * Event handler for the remove button, removes the attachments the user has
     * selected, from the list view.
     */
    void removeSpecificAttachments(ActionEvent event) {

        if (this.regularAttachmentsList.getSelectionModel().getSelectedIndices() != null) {
            ObservableList<Integer> selectedIndicesRegular
                    = this.regularAttachmentsList.getSelectionModel().getSelectedIndices();
            for (int i = 0; i < selectedIndicesRegular.size(); i++) {
                int selected = selectedIndicesRegular.get(i);
                this.regularAttachmentsList.getItems().remove(selected);
            }
        }
        if (this.embeddedAttachmentsList.getSelectionModel().getSelectedIndices() != null) {
            ObservableList<Integer> selectedIndicesEmbedded
                    = this.embeddedAttachmentsList.getSelectionModel().getSelectedIndices();
            for (int i = 0; i < selectedIndicesEmbedded.size(); i++) {
                int selected = selectedIndicesEmbedded.get(i);
                File selectedFile = this.embeddedAttachmentsList.getItems().get(selected);
                this.embeddedAttachmentsList.getItems().remove(selected);
                String currentHtml = this.mailDisplayController.getHtmlEditor().getHtmlText();
                //Remove embedded attachment from the html editor.
                String filePath = '"' + "file:/C:/Temp/" + selectedFile.getName() + '"';
                String toReplace = "<embed src=" + filePath + ">";

                String modifiedHtml = currentHtml.replace(toReplace, "");
                this.mailDisplayController.getHtmlEditor().setHtmlText(modifiedHtml);
            }
        }

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert removeSpecificAttachments != null : "fx:id=\"removeSpecificAttachments\" was not injected: check your FXML file 'AttachmentList.fxml'.";
        assert regularAttachmentsList != null : "fx:id=\"regularAttachmentsList\" was not injected: check your FXML file 'AttachmentList.fxml'.";
        assert embeddedAttachmentsList != null : "fx:id=\"embeddedAttachmentsList\" was not injected: check your FXML file 'AttachmentList.fxml'.";

    }

}
