package com.daniabh.emailclient.controller;

import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.javafxbean.FolderFXBean;
import com.daniabh.emailclient.persistence.EmailDAOImpl;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class for the folder tree view.
 *
 * @author Dania Bouhmidi
 */
public class FolderTreeController {

    private final static Logger LOG = LoggerFactory.getLogger(FolderTreeController.class);

    private EmailDAOImpl emailDAO;
    private EmailTableController emailTableController;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="folderTreeView"
    private TreeView<FolderFXBean> folderTreeView; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert folderTreeView != null : "fx:id=\"folderTreeView\" was not injected: check your FXML file 'FolderTree.fxml'.";

    }

    /**
     * Gets the folder tree view.
     *
     * @return folderTreeView
     */
    public TreeView<FolderFXBean> getFolderTree() {
        return this.folderTreeView;
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * EmailDAO object.
     *
     * @param emailDAO
     */
    public void setEmailDAO(EmailDAOImpl emailDAO) {
        this.emailDAO = emailDAO;
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * EmailTableController from which it can request a reference to the
     * TreeView.With the TreeView reference it can change the selection in the
     * TableView.
     *
     * @param tableController
     */
    public void setTableController(EmailTableController tableController) {
        this.emailTableController = tableController;
    }

    /**
     * Retrieves folders from the database.
     *
     * @return hashet of folders
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public List<FolderFXBean> retrieveFolders() throws SQLException, IOException {
        List<String> allFolders = this.emailDAO.getFolders();
        List<FolderFXBean> folders = new ArrayList<>();
        for (String folder : allFolders) {
            FolderFXBean folderBean = new FolderFXBean(this.emailDAO.getKeyFromFolder(folder), folder);
            folders.add(folderBean);
        }

        FolderFXBean root = new FolderFXBean();
        root.setFolder(resources.getString("Folders"));
        folderTreeView.setRoot(new TreeItem<>(root));
        folders.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
            item.setGraphic(new ImageView(getClass().getResource("/images/folder.jpg").toExternalForm()));
            return item;
        }).forEachOrdered((item) -> {
            folderTreeView.getRoot().getChildren().add(item);
        });
        folderTreeView.getRoot().setExpanded(true);
        return folders;
    }

    /**
     * Build the tree from fake folder data.
     *
     * @throws SQLException
     * @throws java.io.IOException
     */
    public void displayTree() throws SQLException, IOException {
        this.retrieveFolders();
        this.setUpTree();

        // Open the tree
        folderTreeView.getRoot().setExpanded(true);

        folderTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue != null) {
                try {
                    if (newValue.getValue() != null) {
                        showFolder(newValue.getValue());
                    } else {
                        this.emailTableController.clearTable();
                    }
                } catch (SQLException | IOException ex) {
                    LOG.error("An SQL Error occured or an IOException occured while adding the listener for the selected item.");
                }

            }
        });
    }
    /**
     * Sets up the folder tree, the context menu items for each folder in the folder tree
     * and associates drag and drop events to the folders. 
     */
    private void setUpTree(){
         folderTreeView.setCellFactory((e) -> new TreeCell<FolderFXBean>() {
            @Override
            protected void updateItem(FolderFXBean item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getFolder());
                    setGraphic(getTreeItem().getGraphic());
                    MenuItem deleteItem = new MenuItem("Delete Folder");
                    deleteItem.setId(item.getFolder());
                    deleteItem.setOnAction((event) -> {
                        try {
                            deleteFolderHandler(deleteItem.getId());
                        } catch (SQLException | IOException ex) {
                            java.util.logging.Logger.getLogger(FolderTreeController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                    setContextMenu(new ContextMenu(deleteItem));
                    setOnDragEntered((DragEvent event) -> {
                        {
                            setStyle("-fx-background-color: green;");
                        }

                        event.consume();
                    });
                    setOnDragExited((DragEvent event) -> {
                        setStyle(null);
                        event.consume();
                    });
                    setOnDragDropped((DragEvent event) -> {
                        dragDropped(event);
                    });

                    setOnDragOver((DragEvent event) -> {
                        try {
                            onDragOver(event);
                        } catch (SQLException | IOException ex) {
                            java.util.logging.Logger.getLogger(FolderTreeController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });
    }

    /**
     * Shows the emails a specific folder contains
     *
     * @param folderBean
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    private void showFolder(FolderFXBean folderBean) throws SQLException, IOException {
        LOG.info("Displaying the " + folderBean.getFolder() + " folder content");
        this.emailTableController.displayTheTable(folderBean.getFolder());
    }

    /**
     * Handles a delete on folder.
     *
     * @param folder
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    private void deleteFolderHandler(String folder) throws SQLException, IOException {
        LOG.info("Handling delete on folder");
        Alert confirmation = new Alert(AlertType.CONFIRMATION);
        confirmation.setTitle(null);
        confirmation.setHeaderText(null);
        confirmation.setContentText(resources.getString("DeleteFolder") + " '" + folder + "'?");
        Optional<ButtonType> choice = confirmation.showAndWait();
        if (choice.get() == ButtonType.OK) {
            //Interact with the database and delete the folder and all of its content.
            this.emailDAO.removeFolder(folder);
            //Remove the deleted folder from the tree view
            int itemToRemove = folderTreeView.getSelectionModel().getSelectedIndex() - 1;
            folderTreeView.getRoot().getChildren().remove(itemToRemove);
            //Update the folder list in the table
            emailTableController.clearTable();
        }

    }

    /**
     * On Drag Over handler Retrieves the dragged email, and updates its folder
     * to the target folder.
     *
     * @param event
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    private void onDragOver(DragEvent event) throws SQLException, IOException {
        LOG.info("onDragOver");
        if (event.getGestureSource() != this.folderTreeView && event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            TreeCell<FolderFXBean> targetFolder = (TreeCell<FolderFXBean>) event.getTarget();
            String emailid = event.getDragboard().getString();
            int id = Integer.parseInt(emailid);
            String folder = targetFolder.getTreeItem().getValue().getFolder();

            LOG.info("The target folder is : " + folder);
            EmailBean toMove = this.emailDAO.findID(id);

            if (folder.equals("SENT") && this.emailDAO.getFolder(toMove).equals("DRAFT") || this.emailDAO.getFolder(toMove).equals("DRAFT")) {
                Alert warning = new Alert(AlertType.WARNING);
                warning.setTitle("Cannot move email from the draft folder unless it is sent.");
                warning.setHeaderText("The email must be sent first, so that its folder can be updated.");
                warning.show();
            } else {
                this.emailDAO.updateFolder(folder, toMove);
                this.emailTableController.getEmailDataTable().setItems(this.emailTableController.retrieveEmailData());
                this.emailTableController.getEmailDataTable().refresh();
            }
        }
        event.consume();

    }

    /**
     * Drag dropped event handler
     *
     * @param event
     */
    private void dragDropped(DragEvent event) {
        LOG.debug("Tree Drag Drop");
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (event.getDragboard().hasString()) {
            String text = db.getString();
            LOG.debug("Table row ID: " + text);
            success = true;
        }
        event.setDropCompleted(success);
        event.consume();
    }

}
