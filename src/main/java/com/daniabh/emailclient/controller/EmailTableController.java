/**
 * Sample Skeleton for 'emailFXML.fxml' Controller Class
 */
package com.daniabh.emailclient.controller;

import com.daniabh.emailclient.emailbean.EmailBean;
import com.daniabh.emailclient.javafxbean.EmailData;
import com.daniabh.emailclient.javafxbean.EmailFXBean;
import com.daniabh.emailclient.javafxbean.MessageBean;
import com.daniabh.emailclient.javafxbean.PropertyBean;
import com.daniabh.emailclient.persistence.EmailDAOImpl;
import com.daniabh.emailclient.properties.MailConfigBean;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for the email table view. Displays email data based on the current
 * folder
 *
 * @author Dania Bouhmidi
 */
public class EmailTableController {

    private EmailDAOImpl emailDAO;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="emailTable"
    private TableView<EmailData> emailTable; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<EmailData, String> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<EmailData, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<EmailData, LocalDateTime> dateColumn; // Value injected by FXMLLoader

    @FXML // fx:id="searchCriteria"
    private ComboBox<String> searchCriteria; // Value injected by FXMLLoader

    @FXML // fx:id="searchBar"
    private TextField searchBar; // Value injected by FXMLLoader

    private MailDisplayController htmlController;
    private FolderTreeController treeController;
    private String currentFolder;
    private FilteredList<EmailData> filteredData;
    private MailConfigBean configBean;
    @FXML // fx:id="searchButton"
    private Button searchButton; // Value injected by FXMLLoader

    private final static Logger LOG = LoggerFactory.getLogger(EmailTableController.class);

    /**
     * Sets the folder tree controller
     *
     * @param treeController
     */
    public void setTreeController(FolderTreeController treeController) {
        this.treeController = treeController;
    }

    /**
     *
     * Returns the email data table view.
     *
     * @return tableview of EmailData
     */
    public TableView<EmailData> getEmailDataTable() {
        return emailTable;
    }

    /**
     * Sets the html controller that will be used to display the selected email
     * in the tableview.
     *
     * @param controller
     */
    public void setHtmlController(MailDisplayController controller) {
        this.htmlController = controller;
    }

    /**
     * Sets the emailDAO.
     *
     * @param emailDAO
     */
    public void setEmailDAO(EmailDAOImpl emailDAO) {
        this.emailDAO = emailDAO;
    }

    /**
     * Sets up the property bean
     *
     * @param bean
     * @throws SQLException
     * @throws IOException
     */
    public void setPropertyBean(PropertyBean bean) throws SQLException, IOException {
        System.out.println(bean.toString());
        configBean.setDatabaseName(bean.mysqlDatabaseProperty().get());
        configBean.setDatabaseURL(bean.mysqlURLProperty().get());
        configBean.setDatabaseUserName(bean.mysqlUserProperty().get());
        configBean.setDatabasePassword(bean.mysqlPasswordProperty().get());
        configBean.setDatabasePortNumber(Integer.parseInt(bean.getMysqlPort()));
        configBean.setUserName(bean.userNameProperty().get());
        configBean.setUserEmailAddress(bean.emailAddressProperty().get());
        configBean.setEmailPassword(bean.mailPasswordProperty().get());
        configBean.setSmtpUrl(bean.smtpURLProperty().get());
        configBean.setImapUrl(bean.imapURLProperty().get());

    }

    @FXML
    /**
     * Initializes the emailTable. Connects properties in the emailData object
     * to the table columns. Sets up change listeners for the email table
     * selection and the searchBar textfield.
     */
    public void initialize() throws SQLException, IOException {
        configBean = new MailConfigBean();
        setUpSearchCriterias();
        // Connects the property in the EmailData object to the column in the
        // table
        fromColumn.setCellValueFactory(cellData -> cellData.getValue().getFromProperty());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().getSubjectProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
        setTableContextMenu();
        adjustColumnWidths();

        // Listen for selection changes and show the EmailData details when
        // changed.
        emailTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            if (newValue != null) {
                                try {
                                    showEmailDetails(newValue);
                                } catch (SQLException | IOException ex) {
                                    java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        });

    }

    /**
     * Drag Detected event for an email row, Determines what will be dragged
     * when it detects the drag event.
     *
     * @param event
     */
    @FXML
    void dragDetected(MouseEvent event) {
        LOG.debug("onDragDetected");
        Dragboard db = emailTable.startDragAndDrop(TransferMode.ANY);
        ClipboardContent content = new ClipboardContent();
        if (emailTable.getSelectionModel().getSelectedItem() != null) {
            String id = Integer.toString((emailTable.getSelectionModel().getSelectedItem().getId()));
            content.putString(id);
            db.setContent(content);
        }

        event.consume();
    }

    /**
     * Sets the context menu for each row in the table of EmailData
     *
     * @throws java.sql.SQLException
     */
    public void setTableContextMenu() throws SQLException {
        emailTable.setRowFactory(new Callback<TableView<EmailData>, TableRow<EmailData>>() {
            @Override
            public TableRow<EmailData> call(TableView<EmailData> tableView) {
                final TableRow<EmailData> row = new TableRow<>();

                // Set context menu on row,  make it only show for non-empty rows:  
                row.emptyProperty().addListener((obs, wasEmpty, isEmpty) -> {
                    if (isEmpty) {
                        row.setContextMenu(null);
                    } else {
                        try {
                            row.setContextMenu(getContextMenu(row));
                        } catch (SQLException | IOException ex) {
                            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                return row;
            }
        });
    }

    /**
     * Returns the context menu for each row of the email table.
     *
     * @param row
     * @return ContextMenu
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public ContextMenu getContextMenu(TableRow<EmailData> row) throws SQLException, IOException {
        int id = row.getItem().getId();
        final EmailBean emailBean = emailDAO.findID(id);
        EmailFXBean fxbean = new EmailFXBean(emailBean.getId(), emailBean.getFolderKey(), emailBean.getDate(), emailBean.getUsername(),
                emailBean.email);
        ContextMenu emailContextMenu = new ContextMenu();
        MenuItem removeMenuItem = new MenuItem("Remove");
        MenuItem saveAttachmentMenuItem = new MenuItem("Save Attachment");
        MenuItem addAttachmentMenuItem = new MenuItem("Add Attachment");
        MenuItem replyMenuItem = new MenuItem("Reply");
        MenuItem replyAllMenuItem = new MenuItem("Reply All");
        MenuItem forwardMenuItem = new MenuItem("Forward");
        removeMenuItem.setOnAction((e) -> {
            //Remove email from the database
            LOG.info("Remove Menu Item was clicked");
            try {
                deleteEmail();
            } catch (SQLException | IOException ex) {
                java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        );
        saveAttachmentMenuItem.setOnAction((e) -> {
            LOG.info("Save Attachment Menu Item was clicked");
            FileChooser fileChooser = new FileChooser();

            List<File> attachmentsToSave = new ArrayList<>();
            try {

                List<EmailAttachment> attachments = emailBean.email.attachments();
                for (EmailAttachment<? extends DataSource> file : attachments) {
                    String fileName = file.getName();
                    byte[] bytes = file.getDataSource().getInputStream().readAllBytes();
                    URI uri = EmailTableController.class.getResource("/attachments/").toURI();
                    String mainPath = Paths.get(uri).toString();
                    Path path = Paths.get(mainPath, fileName);
                    Files.write(path, bytes);
                    attachmentsToSave.add(path.toFile());
                }
            } catch (IOException | URISyntaxException ex) {
                java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (File file : attachmentsToSave) {
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().addAll( //add filter
                        new ExtensionFilter("Files", getFileExtension(file)));
                fileChooser.setInitialFileName(file.getName());
                File destination = fileChooser.showSaveDialog(emailTable.getScene().getWindow());
                if (destination != null) {

                    try {
                        Files.copy(file.toPath(), destination.toPath());
                    } catch (IOException ex) {
                        // handle exception...
                        LOG.error(ex.getMessage());
                    }
                }
            }
        }
        );
        addAttachmentMenuItem.setOnAction((e) -> {
            LOG.info("Add Attachment Menu Item was clicked");

            FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            List<File> files = fileChooser.showOpenMultipleDialog(stage);
            fileChooser.setTitle("Open Multiple Files");
            if (files != null) {
                for (File file : files) {
                    LOG.info("Absolute Path: " + file.getAbsolutePath());
                    emailBean.email.attachment(EmailAttachment.with().content(file));
                    htmlController.getRegularAttachments().add(file);
                }
            }
        }
        );
        replyMenuItem.setOnAction((e) -> {
            LOG.info("Reply Menu Item was clicked");
            MessageBean messageBean = this.createMessageBean(fxbean);
            htmlController.displayReplyForm(fxbean, messageBean);
        }
        );
        replyAllMenuItem.setOnAction((e) -> {
            LOG.info("Reply All Menu Item was clicked");
            MessageBean messageBean = this.createMessageBean(fxbean);
            htmlController.displayReplyAllForm(fxbean, messageBean);
        }
        );
        forwardMenuItem.setOnAction((e) -> {
            LOG.info("Forward Menu Item was clicked");

            MessageBean messageBean = this.createMessageBean(fxbean);
            htmlController.displayForwardForm(fxbean, messageBean);
        }
        );

        // Set context menu items based on the email type and folder.
        if (emailBean.email instanceof Email && !emailDAO.getFolder(emailBean).equals("DRAFT")) {
            emailContextMenu.getItems().addAll(removeMenuItem, replyMenuItem);
            if (emailBean.email.cc().length > 0 || emailBean.email.to().length > 1) {
                emailContextMenu.getItems().add(replyAllMenuItem);
            }
        }
        if (emailBean.email instanceof ReceivedEmail) {
            emailContextMenu.getItems().addAll(removeMenuItem, replyMenuItem);
            if (emailBean.email.cc().length > 0 || emailBean.email.to().length > 1) {
                emailContextMenu.getItems().add(replyAllMenuItem);
            }
        }
        if (emailDAO.getFolder(emailBean).equals("DRAFT")) {
            emailContextMenu.getItems().addAll(removeMenuItem, addAttachmentMenuItem);
        }
        if (emailBean.email.attachments().size() > 0) {

            emailContextMenu.getItems().add(saveAttachmentMenuItem);
        }
        emailContextMenu.getItems().add(forwardMenuItem);
        return emailContextMenu;

    }

    /**
     * Helper method to get a file's extension.
     *
     * @param file
     * @return file extension
     */
    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".") + 1;
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    /**
     * Sets the width of the columns based on a percentage of the overall width
     * of the emailTable. This width has been set in the fxml file.
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = this.emailTable.getPrefWidth();
        // Set width of each column
        fromColumn.setPrefWidth(width * .30);
        dateColumn.setPrefWidth(width * .30);
        subjectColumn.setPrefWidth(width * .40);
    }

    /**
     * Displays the email data table based on the input folder.
     *
     * @param folder
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public void displayTheTable(String folder) throws SQLException, IOException {
        //Keep track of the current folder.
        this.currentFolder = folder;
        ObservableList<EmailData> data = this.retrieveEmailData();
        filteredData = new FilteredList<>(data, p -> true);
        emailTable.setItems(filteredData);
    }

    /**
     * Retrieves email data from the database into an ObservableList of
     * <EmailData> objects
     *
     * @return ObservableList of EmailData objects
     * @throws SQLException
     * @throws IOException
     */
    public ObservableList<EmailData> retrieveEmailData() throws SQLException, IOException {
        // Add observable list data to the table
        List<EmailBean> emails = this.emailDAO.findAll();

        List<EmailData> emailData = new ArrayList<>();
        for (EmailBean emailBean : emails) {
            EmailFXBean fxbean = new EmailFXBean(emailBean.getId(), emailBean.getFolderKey(), emailBean.getDate(), emailBean.getUsername(),
                    emailBean.email);

            EmailData data = new EmailData(fxbean.getId(), fxbean.getEmail().from().getEmail(), fxbean.getEmail().subject(), fxbean.getDate());
            String emailFolder = this.emailDAO.getFolder(emailBean);
            if (emailFolder.equals(this.currentFolder)) {
                emailData.add(data);
            }
        }
        ObservableList<EmailData> data = FXCollections.observableArrayList(emailData);
        return data;
    }

    /**
     * Displays the EmailData object that corresponds to the selected row in the
     * table.
     *
     * @param EmailData emailData bean
     */
    private void showEmailDetails(EmailData bean) throws SQLException, IOException {

        EmailBean emailBean = this.emailDAO.findID(bean.getId());
        EmailFXBean fxbean = new EmailFXBean(emailBean.getId(), emailBean.getFolderKey(), emailBean.getDate(), emailBean.getUsername(),
                emailBean.email);

        MessageBean messageBean = this.createMessageBean(fxbean);
        this.htmlController.displayEmail(bean, messageBean);

        this.htmlController.displayForm(fxbean);
    }

    /**
     * Creates a message bean that contains an email's plain text and html text.
     *
     * @param fxbean
     * @return messageBean
     */
    public MessageBean createMessageBean(EmailFXBean fxbean) {
        String htmlFound = "", plainText = "";
        for (Object emessage : fxbean.getEmail().messages()) {
            EmailMessage message = ((EmailMessage) (emessage));
            if (message.getMimeType().equals(MimeTypes.MIME_TEXT_HTML)) {
                htmlFound = message.getContent();
            }
            if (message.getMimeType().equals(MimeTypes.MIME_TEXT_PLAIN)) {
                plainText = message.getContent();
            }
        }
        MessageBean messageBean = new MessageBean(plainText, htmlFound);
        return messageBean;
    }

    /**
     * Clear the email tableView items.
     */
    public void clearTable() {
        this.htmlController.clear();
        this.emailTable.refresh();
    }

    /**
     * Display the emails of the currently opened folder that match the input
     * String in the search bar that corresponds to the criteria in the
     * searchCriteria ComboBox.
     *
     */
    @FXML
    void filterEmails(ActionEvent event) {
        LOG.info("Filtering emails.");
        final String criteria = this.searchCriteria.getSelectionModel().getSelectedItem();
        String matchCriteria = this.searchBar.getText();

        filteredData.setPredicate(email -> {
            List<EmailBean> filteredEmails = new ArrayList<>();

            if (criteria != null) {
                switch (criteria) {
                    case "From","De" -> {
                        {
                            try {
                                filteredEmails = this.emailDAO.findFrom(matchCriteria);
                            } catch (SQLException | IOException ex) {
                                java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        for (EmailBean bean : filteredEmails) {
                            if (bean.getId() == email.getId()) {
                                return true;
                            }
                        }
                    }
                    case "BCC", "CC", "To","À" -> {
                        try {
                            if (criteria.equals("À")) {
                                filteredEmails = this.emailDAO.findWith(matchCriteria, "To");
                            } else {
                                filteredEmails = this.emailDAO.findWith(matchCriteria, criteria);
                            }
                            for (EmailBean bean : filteredEmails) {
                                if (bean.getId() == email.getId()) {
                                    return true;
                                }
                            }

                        } catch (SQLException | IOException ex) {
                            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    case "Subject","Sujet" -> {
                        try {
                            filteredEmails = this.emailDAO.findSubject(matchCriteria);
                            for (EmailBean bean : filteredEmails) {
                                if (bean.getId() == email.getId()) {
                                    return true;
                                }
                            }
                        } catch (SQLException | IOException ex) {
                            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    case "All","Tout" -> {
                        //Display all the emails
                        return true;

                    }

                }
            }
            return false;
        }
        );

    }

    /**
     * Event handler for the Remove context menu item in the email table.
     * Deletes the email currently selected.
     */
    private void deleteEmail() throws SQLException, IOException {
        EmailData emailToDelete = this.emailTable.getSelectionModel().getSelectedItem();
        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle(resources.getString("DeleteEmail") + " '" + emailToDelete.getId() + "'?");
        confirmation.setContentText(resources.getString("DeleteEmail") + " '" + emailToDelete.getId() + "'?");
        Optional<ButtonType> choice = confirmation.showAndWait();
        if (choice.get() == ButtonType.OK) {
            //Interact with the database and delete the email by retrieving its id.
            int emailID = this.emailTable.getSelectionModel().getSelectedItem().getId();
            this.emailTable.getSelectionModel().clearSelection();
            this.htmlController.clear();
            // Remove email from the table and the database
            this.emailDAO.delete(emailID);
            ObservableList<EmailData> refreshedData = this.retrieveEmailData();
            this.emailTable.setItems(refreshedData);

        }
    }

    /**
     * Sets up the searchCriteria ComboBox. It will allow the user to filter
     * emails based on a specific field email address or based on a specific
     * subject.
     */
    private void setUpSearchCriterias() {
        ObservableList<String> criterias
                = FXCollections.observableArrayList(
                        resources.getString("From"),
                        resources.getString("Subject"),
                        resources.getString("CC"),
                        resources.getString("BCC"),
                        resources.getString("To"),
                        resources.getString("All")
                );

        this.searchCriteria.setItems(criterias);
    }

    /**
     * Returns the tree controller.
     *
     * @return treeController
     */
    public FolderTreeController getTreeController() {
        return treeController;
    }

    /**
     * Returns the current folder.
     *
     * @return currentFolder
     */
    public String getCurrentFolder() {
        return currentFolder;
    }

}
