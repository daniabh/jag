package com.daniabh.emailclient.controller;

import com.daniabh.emailclient.properties.PropertiesManager;
import com.daniabh.emailclient.javafxbean.PropertyBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for the Properties Form, Manages and validates the properties form
 * for the MailConfigBean. Validates that the textfields for the configuration
 * parameters are filled.
 *
 * @author Dania Bouhmidi
 */
public class PropertiesFormController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    private PropertyBean propertyBean;
    private PropertiesManager propertiesManager;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="userNameField"
    private TextField userNameField; // Value injected by FXMLLoader

    @FXML // fx:id="emailAddressField"
    private TextField emailAddressField; // Value injected by FXMLLoader

    @FXML // fx:id="mailPasswordField"
    private TextField mailPasswordField; // Value injected by FXMLLoader

    @FXML // fx:id="imapURLField"
    private TextField imapURLField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpURLField"
    private TextField smtpURLField; // Value injected by FXMLLoader

    @FXML // fx:id="imapPortField"
    private TextField imapPortField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpPortField"
    private TextField smtpPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlURLField"
    private TextField mysqlURLField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabaseField"
    private TextField mysqlDatabaseField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPortField"
    private TextField mysqlPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlUserField"
    private TextField mysqlUserField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPasswordField"
    private TextField mysqlPasswordField; // Value injected by FXMLLoader

    @FXML //fx:id="configForm"
    private HBox configForm; // Value injected by FXMLLoader
    private RootLayoutController rootController;
    private Stage stage;

    @FXML
    /**
     * Cancel button event handler
     *
     * @param event
     */
    void pressCancel(ActionEvent event) {
        LOG.info("Cancelling");
    }

    @FXML
    /**
     * Save button event handler
     *
     * @param event
     */
    void pressSave(ActionEvent event) throws IOException {
        if (validateFields()) {
            propertiesManager = new PropertiesManager();
            propertiesManager.writeTextProperties("", "MailConfig", propertyBean);
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(this.getClass()
                    .getResource("/fxml/RootLayout.fxml"));
            Scene newScene = new Scene(loader.load());
            stage.setScene(newScene);
        } else {
            validationAlert("Invalid Mail Configuration");
        }
    }

    /**
     * Pops up a confirmation dialog to tell the user that they did not fill in
     * all required fields.
     *
     * @param msg
     */
    private void validationAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle(resources.getString("InvalidConfig"));
        dialog.setHeaderText(resources.getString("InvalidConfig"));
        dialog.setContentText(msg);
        dialog.showAndWait();
    }

    /**
     * Validates that the textFields were filled.
     *
     * @return boolean indicating whether the textFields were filled.
     */
    public boolean validateFields() {
        ObservableList<Node> form = this.configForm.getChildren();
        for (Node node : form) {
            if (node instanceof TextField) {
                if (((TextField) node).getText().isBlank() || ((TextField) node).getText().isEmpty()) {
                    return false;
                }
                if (((TextField) node).getText() == null) {
                    return false;
                }
            }
        }
        return true;

    }

    /**
     *
     * Sets the stage and rootController.
     *
     * @param stage
     * @param rootController
     */
    public void setStageController(Stage stage, RootLayoutController rootController) {
        this.stage = stage;
        this.rootController = rootController;
    }

    /**
     * Sets up the properties and the binding.
     *
     * @param pm
     * @param pb
     */
    public void setupProperties(PropertiesManager pm, PropertyBean pb) {
        this.propertiesManager = pm;
        this.propertyBean = pb;
        doBindings();
    }

    @FXML
    /**
     * This method is called by the FXMLLoader when initialization is complete
     * Initializes the property bean and the stage.
     */
    void initialize() {
        assert userNameField != null : "fx:id=\"userNameField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert emailAddressField != null : "fx:id=\"emailAddressField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mailPasswordField != null : "fx:id=\"mailPasswordField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert imapURLField != null : "fx:id=\"imapURLField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert smtpURLField != null : "fx:id=\"smtpURLField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert imapPortField != null : "fx:id=\"imapPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert smtpPortField != null : "fx:id=\"smtpPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlURLField != null : "fx:id=\"mysqlURLField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlDatabaseField != null : "fx:id=\"mysqlDatabaseField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlPortField != null : "fx:id=\"mysqlPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlUserField != null : "fx:id=\"mysqlUserField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlPasswordField != null : "fx:id=\"mysqlPasswordField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        propertyBean = new PropertyBean();
        stage = new Stage();
    }

    /**
     * Sets up the binding of the text fields with the property bean fields.
     */
    private void doBindings() {
        LOG.debug("PropertyBean :" + (propertyBean == null));
        Bindings.bindBidirectional(userNameField.textProperty(), propertyBean.userNameProperty());
        Bindings.bindBidirectional(emailAddressField.textProperty(), propertyBean.emailAddressProperty());
        Bindings.bindBidirectional(mailPasswordField.textProperty(), propertyBean.mailPasswordProperty());
        Bindings.bindBidirectional(imapURLField.textProperty(), propertyBean.imapURLProperty());
        Bindings.bindBidirectional(smtpURLField.textProperty(), propertyBean.smtpURLProperty());
        Bindings.bindBidirectional(imapPortField.textProperty(), propertyBean.imapPortProperty());
        Bindings.bindBidirectional(smtpPortField.textProperty(), propertyBean.smtpPortProperty());
        Bindings.bindBidirectional(mysqlURLField.textProperty(), propertyBean.mysqlURLProperty());
        Bindings.bindBidirectional(mysqlDatabaseField.textProperty(), propertyBean.mysqlDatabaseProperty());
        Bindings.bindBidirectional(mysqlPortField.textProperty(), propertyBean.mysqlPortProperty());
        Bindings.bindBidirectional(mysqlUserField.textProperty(), propertyBean.mysqlUserProperty());
        Bindings.bindBidirectional(mysqlPasswordField.textProperty(), propertyBean.mysqlPasswordProperty());

    }

    /**
     * Returns the property bean
     *
     * @return propertyBean
     */
    public PropertyBean getPropertyBean() {
        return propertyBean;
    }
}
