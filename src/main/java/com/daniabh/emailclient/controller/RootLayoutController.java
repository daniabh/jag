package com.daniabh.emailclient.controller;

import com.daniabh.emailclient.persistence.EmailDAOImpl;
import com.daniabh.emailclient.properties.PropertiesManager;
import com.daniabh.emailclient.javafxbean.FolderFXBean;
import com.daniabh.emailclient.javafxbean.PropertyBean;
import com.daniabh.emailclient.properties.MailConfigBean;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Window;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import javafx.stage.Stage;

/**
 * FXML Controller class Initializes the three components of the SplitPane.
 * Handles menu items events, such as displaying an About page, when the About
 * Menu Item is selected, such as displaying a form for the user to fill when
 * the Configure Mail Properties Menu Item is selected, and validating them.
 *
 * @author Dania Bouhmidi
 */
public class RootLayoutController {

    private Stage stage;

    @FXML
    private BorderPane rootPane;
    @FXML
    private BorderPane upperRightSplit;

    @FXML
    private BorderPane leftSplit;

    @FXML
    private BorderPane lowerRightSplit;

    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutController.class);

    @FXML
    private ResourceBundle resources;

    private EmailDAOImpl emailDAO;
    private FolderTreeController treeController;
    private EmailTableController tableController;
    private MailDisplayController htmlController;
    private MailConfigBean configBean;
    private PropertiesFormController propertyController;

    /**
     * Default constructor Sets the emailDAO object
     */
    public RootLayoutController() {
        configBean = new MailConfigBean();
    }

    /**
     * Sets the stage
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Sets up the property bean
     *
     * @param bean
     * @throws SQLException
     * @throws IOException
     */
    public void setPropertyBean(PropertyBean bean) throws SQLException, IOException {
        System.out.println(bean.toString());
        configBean.setDatabaseName(bean.mysqlDatabaseProperty().get());
        configBean.setDatabaseURL(bean.mysqlURLProperty().get());
        configBean.setDatabaseUserName(bean.mysqlUserProperty().get());
        configBean.setDatabasePassword(bean.mysqlPasswordProperty().get());
        configBean.setDatabasePortNumber(Integer.parseInt(bean.getMysqlPort()));
        configBean.setUserName(bean.userNameProperty().get());
        configBean.setUserEmailAddress(bean.emailAddressProperty().get());
        configBean.setEmailPassword(bean.mailPasswordProperty().get());
        configBean.setSmtpUrl(bean.smtpURLProperty().get());
        configBean.setImapUrl(bean.imapURLProperty().get());

        emailDAO = new EmailDAOImpl(configBean);

    }

    /**
     * Initializes and loads the containers. Sends/Sets the appropriate
     * information to each of them.
     */
    @FXML
    void initialize() throws IOException, SQLException {
        initLeftLayout();
        initUpperRightLayout();
        initLowerRightLayout();
        tableController.setHtmlController(htmlController);
        // Tell the tree about the table
        setTableControllerToTree();
    }

    @FXML
    /**
     * Event handler when the Configure Mail Properties choice is selected in
     * the Mail Menu.
     *
     * @throws IOException
     */
    void configureMailProperties() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/PropertiesForm.fxml"));
        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
        loader.setResources(resources);

        Parent form = loader.load();
        propertyController = loader.getController();
        PropertyBean pb = new PropertyBean();
        PropertiesManager pm = new PropertiesManager();
        propertyController.setupProperties(pm, pb);
        Dialog dialog = new Dialog();
        dialog.setGraphic(form);
        dialog.initModality(Modality.NONE);
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> window.hide());
        dialog.setTitle(resources.getString("SettingsTitle"));
        dialog.setHeaderText(resources.getString("SettingsTitle"));
        dialog.show();
    }

    /**
     * Send the reference from the treeController to the tableController
     */
    private void setTableControllerToTree() {
        treeController.setTableController(tableController);
    }

    /**
     * Initializes the Folder TreeView Layout
     */
    private void initLeftLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);

        loader.setLocation(RootLayoutController.class
                .getResource("/fxml/FolderTree.fxml"));
        AnchorPane treeView = (AnchorPane) loader.load();
        treeController = loader.getController();
        // Give the controller the data object.
        treeView.autosize();
        leftSplit.setCenter(treeView);
    }

    /**
     * Initializes the TableView Layout that displays emails.
     *
     * @throws IOException
     */
    private void initUpperRightLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);
        loader.setLocation(RootLayoutController.class
                .getResource("/fxml/EmailTable.fxml"));
        ScrollPane tableView = (ScrollPane) loader.load();
        tableController = loader.getController();
        upperRightSplit.setCenter(tableView);
        tableView.autosize();
        tableView.prefWidthProperty().bind(upperRightSplit.prefWidthProperty());
        tableView.prefHeightProperty().bind(upperRightSplit.prefHeightProperty());
        upperRightSplit.getStylesheets().add(getClass().getResource("/styles/emailfxml.css").toExternalForm());
    }

    /**
     * Initializes the lower right layout, the HTMLEditor Layout and its text
     * fields and buttons that are necessary for email composition or email
     * display.
     */
    private void initLowerRightLayout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);

        loader.setLocation(RootLayoutController.class
                .getResource("/fxml/htmlView.fxml"));
        AnchorPane htmlView = (AnchorPane) loader.load();
        htmlController = loader.getController();
        htmlView.autosize();
        htmlView.prefWidthProperty().bind(lowerRightSplit.prefWidthProperty());
        htmlView.prefHeightProperty().bind(lowerRightSplit.prefHeightProperty());
        lowerRightSplit.setCenter(htmlView);
    }

    /**
     * Pops up an error message dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("errorTitle"));
        dialog.setHeaderText(resources.getString("errorTitle"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Event handle when the About choice in the Help menu is selected. Pops up
     * a dialog window explaining how the application works.
     *
     * @param event
     */
    @FXML
    void handleAbout(ActionEvent event) {
        final String html = "help.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
        LOG.info("uri= " + uri.toString());
        // Create WebView with specified local content
        WebView webView = new WebView();
        webView.getEngine().load(uri.toString());
        Dialog dialog = new Dialog();
        dialog.initModality(Modality.NONE);
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> window.hide());
        dialog.setTitle(resources.getString("AboutTitle"));
        dialog.setHeaderText(resources.getString("AboutTitle"));
        dialog.setGraphic(webView);
        dialog.show();
    }

    @FXML
    /**
     * Event handler when the Creating a New Folder choice in the File Menu is
     * selected. Displays a form for the user to enter the folder name of the
     * folder they want to create. Validates that the folder does not already
     * exist, and that it is not blank,empty or null.
     */
    void createFolder() {
        TextField folderName = new TextField();
        folderName.setPromptText("Enter the new folder name.");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Button createFolderButton = new Button(resources.getString("NewFolderForm"));
        grid.add(folderName, 1, 1);
        grid.add(createFolderButton, 1, 2);
        createFolderButton.setOnAction(e -> validateNewFolder(folderName.getText()));
        LOG.info("Creating a new folder.");
        Dialog dialog = new Dialog();
        dialog.initModality(Modality.NONE);
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> window.hide());
        dialog.setTitle(resources.getString("NewFolderForm"));
        dialog.setHeaderText(resources.getString("NewFolderForm"));
        dialog.setGraphic(grid);
        dialog.show();
    }

    /**
     * Validates the newly created folder. If the folder name's already exists
     * in the folder table, it is invalid. If it the folder's name is
     * empty/blank, it is also invalid.
     *
     * @param newFolder
     */
    private void validateNewFolder(String newFolder) {
        boolean foundExists = false;
        boolean emptyBlankOrNull = false;
        if (newFolder == null || newFolder.isEmpty() || newFolder.equals("")) {
            this.errorAlert("Invalid folder name, folder name is empty or blank");
            emptyBlankOrNull = true;
        }

        for (TreeItem<FolderFXBean> item : this.treeController.getFolderTree().getRoot().getChildren()) {
            if (item.getValue().getFolder().equalsIgnoreCase(newFolder)) {
                this.errorAlert("Invalid folder name, folder name already exists.");
                foundExists = true;
            }
        }
        if (!emptyBlankOrNull && !foundExists) {
            //Setting the folder id to -1, because we do not have access to the database
            //Later, when we can access the database, we can set a real folder id.
            FolderFXBean folderBean = new FolderFXBean(-1, newFolder);
            TreeItem<FolderFXBean> newTreeFolder = new TreeItem(folderBean);
            newTreeFolder.setGraphic(new ImageView(getClass().getResource("/images/folder.jpg").toExternalForm()));
            this.treeController.getFolderTree().getRoot().getChildren().add(newTreeFolder);
            this.treeController.getFolderTree().refresh();
        }

    }

    /**
     * Gets the treeController
     *
     * @return treeController
     */
    public FolderTreeController getTreeController() {
        return treeController;
    }

    /**
     * Gets the tableController
     *
     * @return tableController
     */
    public EmailTableController getTableController() {
        return tableController;
    }

    /**
     * Gets the emailDAO
     *
     * @return emaiLDAO
     */
    public EmailDAOImpl getEmailDAO() {
        return emailDAO;
    }

    /**
     * Gets the htmlController
     *
     * @return htmlController
     */
    public MailDisplayController getHtmlController() {
        return htmlController;
    }

    /**
     * Gets the configBean
     *
     * @return configBean
     */
    public MailConfigBean getConfigBean() {
        return configBean;
    }

}
